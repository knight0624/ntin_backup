/**
 * [確認傳遞參數]
 * @return {[type]} [description]
 */
function checkArguments(){
	for (var i = 0; i < arguments.length; i++) {
		if(arguments[i].trim() == ""){
			return false;
		}
	};
	return true;
}

/**
 * [初始化傳遞參數]
 */
function initArguments(){
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].value = "";
	};
}

/**
 * [清除多餘HTML標籤]
 */
function clearTag(text){
	if(typeof(text)==="string"){
		return text.replace(/(<([^>]+)>)/ig,"");
	}
}