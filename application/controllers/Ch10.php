<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch10 extends MY_Authentication {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('ch10_model','',TRUE);
		$this->load->helper('form');
		$this->load->model("common_model","",TRUE);
	}

	public function index() 
	{
		if ($this->getLoginStatus() === 1){
			$this->write($_SESSION['hw_id'], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function write ($hw_id = "", $team_id = "") 
	{
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);

		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			// 學生有main_id
			$data = $this->pageInit("家庭資源");
			$data["permission"] = 1;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch10_model->getFamilyResources_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch10_view" , $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			// 老師有main_id
			$data = $this->pageInit("家庭資源");
			$data["permission"] = 2;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch10_model->getFamilyResources_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch10_view" , $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			// admin
			$data = $this->pageInit("家庭資源");
			$data["permission"] = 3;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch10_model->getFamilyResources_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch10_view" , $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ) {
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ) {
			redirect("mark");  
		}
		else {
			// other
			$data = $this->pageInit("家庭資源");
			$data["permission"] = 6;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch10_model->getFamilyResources_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch10_view" , $data);
		}
	}

	/**
	 * 家庭內在資源 (FAMLIS)
	 */
	public function FAMLIS()
	{
		if(!empty($this->input->post('FAMLIS_result'))){
			$FAMLIS_array = $this->security->xss_clean($this->input->post('FAMLIS_result'));
			$insert_status = $this->ch10_model->editFAMLIS_model($_SESSION["team_id"], $_SESSION["hw_id"], $FAMLIS_array); //寫入
			echo $insert_status;
		} else {
			echo false;
		}
	}

	/**
	 * 家庭外在資源 (SCREEEM)
	 */
	public function SCREEEM()
	{
		if(!empty($this->input->post('SCREEEM_result'))){
			$SCREEEM_array = $this->security->xss_clean($this->input->post('SCREEEM_result'));
			$insert_status = $this->ch10_model->editSCREEEM_model($_SESSION["team_id"], $_SESSION["hw_id"], $SCREEEM_array); //寫入
			echo $insert_status;
		} else {
			echo false;
		}
	}
	
	/**
	 * [圖檔/上傳檔案]
	 * @return [type] [description]
	 */
	public function Eco_Map_img_upload() 
	{
		if (empty($_FILES['userfile_img']['name'])) {
			redirect('Ch10');
		} else {
			$config['upload_path']   = './uploads/img';
			$config['file_name']     = "Eco-Map圖片_".$_SESSION["main_id"]."_".date("Ymdhi");
			$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
			$config['max_size']      = 8192;
			$config['overwrite']     = TRUE;

			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('userfile_img')) {
				$error = array(
					"error" => $this->upload->display_errors(),
					"title" => "失敗"
					);
				$this->load->view("upload_error_view", $error);
			} else {
				$data = $this->upload->data();
				$old_file_name = $this->ch10_model->getSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'fr_Eco_Map_img'); // 先撈之前上傳的檔名
				$path = 'uploads/img/'.$old_file_name['fr_Eco_Map_img'];
				if ($data["file_name"] != $old_file_name['fr_Eco_Map_img']) {
					if ( !empty($old_file_name) && is_readable($path) ) { // 若有值且在資料夾有東西
						unlink($path); // 刪除檔案
					}	        		
				}
				$insert_status = $this->ch10_model->editSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'fr_Eco_Map_img', $this->upload->data('file_name')); //寫入
				redirect('Ch10');
			}
		}
	}

	/**
	 * [PPT檔/上傳檔案]
	 * @return [type] [description]
	 */
	public function Eco_Map_ppt_upload() 
	{
		if (empty($_FILES['userfile_ppt']['name'])) {
			redirect('Ch10');
		} else {
			$config['upload_path']   = './uploads/ppt';
			$config['file_name']     = "Eco-MapPPT_".$_SESSION["main_id"]."_".date("Ymdhi");
			$config['allowed_types'] = 'ppt|pptx';
			$config['max_size']      = 8192;
			$config['overwrite']     = TRUE;

			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('userfile_ppt')) {
				$error = array(
					"error" => $this->upload->display_errors(),
					"title" => "失敗"
					);
				$this->load->view("upload_error_view", $error);
			} else {
				$data = $this->upload->data();
				$old_file_name = $this->ch10_model->getSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'fr_Eco_Map_ppt'); // 先撈之前上傳的檔名
				$path = 'uploads/ppt/'.$old_file_name['fr_Eco_Map_ppt'];
				if ($data["file_name"] != $old_file_name['fr_Eco_Map_ppt']) {
					if ( !empty($old_file_name) && is_readable($path) ) { // 若有值且在資料夾有東西
						unlink($path); // 刪除檔案
					}
				}
				$data = array( 'upload_data' => $this->upload->data());
				$insert_status = $this->ch10_model->editSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'fr_Eco_Map_ppt', $this->upload->data('file_name')); //寫入
				redirect('Ch10');
			}
		}
	}

	/**
	 * 刪除文件
	 */
	public function Delete_File()
	{
		$this->load->helper("file");
		if (empty($this->input->post('file_name'))) {
			echo false;
		} else{
			$file_name = $this->input->post('file_name');
			$file_arr = explode(".",$file_name);
			if ($file_arr[1]=='jpg' || $file_arr[1]=='png' || $file_arr[1]=='gif' || $file_arr[1]=='bmp' || $file_arr[1]=='jpeg') {
				$path = 'uploads/img/'.$file_name;
				// echo $path;
				if (is_readable($path) && unlink($path)) {
					$delete_status = $this->ch10_model->editSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'fr_Eco_Map_img', ''); //清空
					echo $delete_status;
				} else {
					echo false;
				}
			} else if ($file_arr[1]=='ppt' || $file_arr[1]=='pptx') {
				$path = 'uploads/ppt/'.$file_name;
				// echo $path;
				if (is_readable($path) && unlink($path)) {
					$delete_status = $this->ch10_model->editSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'fr_Eco_Map_ppt', ''); //清空
					echo $delete_status;
				} else {
					echo false;
				}
			}
		}
	}

	/**
	 * 補充
	 */
	public function Save_Supplement()
	{
		$supplement_value = $this->input->post('supplement');
		!empty($comment) ? $this->security->xss_clean($supplement) : "";
		$insert_status = $this->ch10_model->editSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'fr_supplement', $supplement_value); //寫入
		echo $insert_status;
	}

	/**
	 * 老師評語
	 */
	public function addComment()
	{
		$comment = $this->input->post('comment_value');
		!empty($comment) ? $this->security->xss_clean($comment) : "";
		$insert_status = $this->ch10_model->editSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'comment', $comment); //寫入
		echo $insert_status;
	}
	
}
