<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch13 extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model('ch13_model','',TRUE);
		$this->load->helper('form');
		$this->load->model("common_model","",TRUE);
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$this->write($_SESSION["hw_id"], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function write($hw_id = "", $team_id = ""){
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = $result->hw_end_time;

		$comment ="";
		$reference_text = "";
		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			$permission = '1';
			$data = $this->pageInit("參考文獻");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$reference = $this->ch13_model->getreference_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$score = $this->ch13_model->getScore_model($_SESSION["team_id"], $_SESSION["hw_id"])->row()->score;
			$reference_content = $reference['reference_content'];
			$comment = $reference['comment'];

			$data["permission"] = $permission;
			$data["reference_content"] = $reference_content;
			$data["comment"] = $comment;
			$data["score"] = $score;
			$data["upload"] = $this->ch13_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch13_view" , $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			$permission = '2';
			$data = $this->pageInit("參考文獻");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$reference = $this->ch13_model->getreference_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$score = $this->ch13_model->getScore_model($_SESSION["team_id"], $_SESSION["hw_id"])->row()->score;
			$reference_content = $reference['reference_content'];
			$comment = $reference['comment'];

			$data["permission"] = $permission;
			$data["reference_content"] = $reference_content;
			$data["comment"] = $comment;
			$data["score"] = $score;
			$data["upload"] = $this->ch13_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch13_view" , $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			$permission = '3';
			$data = $this->pageInit("參考文獻");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$reference = $this->ch13_model->getreference_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$score = $this->ch13_model->getScore_model($_SESSION["team_id"], $_SESSION["hw_id"])->row()->score;
			$reference_content = $reference['reference_content'];
			$comment = $reference['comment'];

			$data["permission"] = $permission;
			$data["reference_content"] = $reference_content;
			$data["comment"] = $comment;
			$data["score"] = $score;
			$data["upload"] = $this->ch13_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch13_view" , $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ){
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ){
			redirect("mark");
		}
		else{
			$permission = '6';
			$data = $this->pageInit("參考文獻");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$reference = $this->ch13_model->getreference_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$score = $this->ch13_model->getScore_model($_SESSION["team_id"], $_SESSION["hw_id"])->row()->score;
			$reference_content = $reference['reference_content'];
			$comment = $reference['comment'];

			$data["permission"] = $permission;
			$data["reference_content"] = $reference_content;
			$data["comment"] = $comment;
			$data["score"] = $score;
			$data["upload"] = $this->ch13_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch13_view" , $data);
		}
	}

	public function editReference(){
		$reference = !empty($_POST["reference"]) ? $this->security->xss_clean($_POST["reference"]) : "";
		$this->ch13_model->editReference_model($reference, $_SESSION["team_id"],  $_SESSION["hw_id"]);
		echo true;
	}

	public function addComment() {
		$comment = !empty($_POST["comment"]) ? $this->security->xss_clean($_POST["comment"]) : "";
		$score = !empty($_POST["score"]) ? $this->security->xss_clean($_POST["score"]) : 0;
		$this->ch13_model->addComment_model($comment, $_SESSION["team_id"], $_SESSION["hw_id"]);
		$this->ch13_model->editScore_model($score, $_SESSION["team_id"], $_SESSION["hw_id"] );
		echo true;
	}

	public function do_upload_file(){
		for($i = 0; $i < count($_FILES["userfile"]["name"]); $i++){
			$_FILES["tempFile"]["name"] = $_SESSION["main_id"]."_".date("Y/m/d/h/i")."_附件".($i+1)."_".$_FILES["userfile"]["name"][$i];
			$_FILES["tempFile"]["type"] = $_FILES["userfile"]["type"][$i];
			$_FILES["tempFile"]["tmp_name"] = $_FILES["userfile"]["tmp_name"][$i];
			$_FILES["tempFile"]["error"] = $_FILES["userfile"]["error"][$i];
			$_FILES["tempFile"]["size"] = $_FILES["userfile"]["size"][$i];
			$config["file_name"] = $_FILES["tempFile"]["name"];
			$config["max_size"]  = 20480;
			$config["detect_mime"] = true;
			$config["upload_path"] = "./uploads/reference/";
			$config["allowed_types"] = "bmp|jpg|png|gif|pdf|txt|csv|doc|docx|xls|xlsx|ppt|pptx";
			$this->load->library("upload", $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload("tempFile")){
				$old_name = $this->ch13_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
				$file_name = $this->upload->data("file_name");
				$this->ch13_model->addPttName_model($_SESSION["team_id"], $_SESSION["hw_id"], $file_name);
			} else {
				$this->load->view("upload_error_view");
			}
		}
		header("Refresh: 3; url=".base_url()."Ch13");
	}

	public function delete_file() {
		$file_name = $_POST["file_name"];

		if (is_readable("uploads/reference/".$file_name)) {
			$this->ch13_model->deleteFile_model($_SESSION["team_id"], $_SESSION["hw_id"], $file_name);
			unlink("uploads/reference/".$file_name);
			echo true;
		}else {
			echo false;
		}
	}
}