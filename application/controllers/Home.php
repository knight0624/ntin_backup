<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Home extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model("home_model");
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("課程列表");
			$this->load->model("common_model");
			$_SESSION["permission"] = $this->getPermission();
			$this->load->view("home_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	function getCourse(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		$this->load->library("MY_Datatables_Lib");
		$this->my_datatables_lib
		->select("SHA1(CONCAT('ntin_',course.course_id)) as course_id , course.course_name , course.academic_year, user.name , IF(course.semester = 1 , '上學期' , '下學期') as semester")
		->from("course")
		->join("user", "course.instructor = user.account");
		// 如果有選擇年份就進行過濾
		if ( $this->getPermission() == $min ) {
			$this->my_datatables_lib->add_column("edit_btn",
			"<div class='text-center'>
				<a class='btn btn-primary btn-xs' href='".base_url()."home/getItem/$1'><i class='fa fa-search'></i></a>
			</div>",
			"course_id");
		} elseif ( $this->getPermission() == $second ) {
			$this->my_datatables_lib->where("course_status" , 1);
			$this->my_datatables_lib->where("instructor" , $_SESSION["username"]);
			$this->my_datatables_lib->add_column("edit_btn",
			"<div class='text-center'>
				<a class='btn btn-primary btn-xs' href='".base_url()."home/getItem/$1'><i class='fa fa-search'></i></a>
			</div>",
			"course_id");
		} else {
			$this->my_datatables_lib->join("(select * from course_student where account ='".$_SESSION["username"]."') as course_student" , "course.course_id = course_student.course_id");
			$this->my_datatables_lib->add_column("edit_btn",
			"<div class='text-center'>
				<a class='btn btn-primary btn-xs' href='".base_url()."home/getItem/$1'><i class='fa fa-search'></i></a>
			</div>",
			"course_id");
		}
		echo $this->my_datatables_lib->generate();
	}

	function getItem($course_id){
		if ($this->getLoginStatus() === 1 && !empty($course_id)){
			$this->load->model("common_model");
			$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
			$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
			$permission = $this->getPermission();

			$this->load->model("course_model");
			$result = $this->course_model->getCourseDetial_model($this->security->xss_clean($course_id) , $permission , $second)->row();

			if(count($result)){
				$data = $this->pageInit("作業列表");
				$data["writable"] = false;
				if($min == $permission || $second == $permission){
					$data["writable"] = true;
				}
				$_SESSION["course_name"] = $result->course_name;
				$_SESSION["course_id"] = $result->course_id;
				$this->load->view("home_item_view" , $data);
			} else {
				echo "請透過正確管道瀏覽";
				header("Refresh: 3; url=".base_url()."home");
			}
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	function getHomework(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		$this->load->library("MY_Datatables_Lib");
		$this->my_datatables_lib
		->select("SHA1(CONCAT('ntin_',homework.hw_id)) as hw_id , homework.hw_name , homework.hw_end_time , homework.create_time, user_history.team_id")
		->from("homework")
		->join("course" , "homework.course_id = course.course_id")
		->join("(SELECT DISTINCT team_id , hw_id FROM user_history where account= '".$_SESSION["username"]."') as user_history" , "SHA1(CONCAT('ntin_', homework.hw_id)) = user_history.hw_id" , "LEFT")
		->where("course.course_status" , 1)
		->where("homework.course_id", $_SESSION["course_id"]);

		if ( $this->getPermission() == $min ) {
			$this->my_datatables_lib->add_column("edit_btn",
			"<div class='text-center'>
				<a href='".base_url()."team/setting/".SHA1('ntin_'.$_SESSION["course_id"])."/$1' data-hw='$1' class='btn btn-primary btn-xs hwbtn'>分組清單</a>
				<button data-edit='$1' class='btn btn-warning btn-xs editbtn'>編輯</button>
			</div>",
			"hw_id");
		}
		/*老師*/
		elseif ( $this->getPermission() == $second ) {
			$this->my_datatables_lib->where("course.instructor" , $_SESSION["username"]);
			$this->my_datatables_lib->add_column("edit_btn",
			"<div class='text-center'>
				<a href='".base_url()."team/setting/".SHA1('ntin_'.$_SESSION["course_id"])."/$1' class='btn btn-primary btn-xs hwbtn'>分組清單</a>
				<button data-edit='$1' class='btn btn-warning btn-xs editbtn'>編輯</button>
			</div>",
			"hw_id");
		}
		/*學生*/
		else {
			$this->my_datatables_lib->join("(select * from course_student where account ='".$_SESSION["username"]."') as course_student" , "course.course_id = course_student.course_id");
			$this->my_datatables_lib->add_column("edit_btn",
			"<div class='text-center'>
				<a class='btn btn-primary btn-xs' href='".base_url()."ch1/write/$1/$2'><i class='fa fa-search'></i></a>
			</div>",
			"hw_id,team_id");
		}
		echo $this->my_datatables_lib->generate();
	}

	function getHomeworkDetial(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if( ($min == $permission || $second == $permission) && !empty($_POST["edit"]) ){
			$result = $this->home_model->getHomeworkDetial_model($this->security->xss_clean($_POST["edit"]))->row();
			echo json_encode($result);
		}
	}

	function setHomework(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if($min == $permission || $second == $permission){
			$data = $this->security->xss_clean($_POST);
			foreach ($data as $key => $value) {
				if(empty($value)){
					exit();
				}
			}
			$this->home_model->setHomework_model($data);
		}
	}

	function updateHomework(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if($min == $permission || $second == $permission){
			$data = $this->security->xss_clean($_POST);
			foreach ($data as $key => $value) {
				if(empty($value)){
					exit();
				}
			}
			$this->home_model->updateHomework_model($data);
		}
	}
}
?>