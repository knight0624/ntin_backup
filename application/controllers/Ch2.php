<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch2 extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model("ch2_model","",TRUE);
		$this->load->model("common_model","",TRUE);
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$this->write($_SESSION["hw_id"], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	function write($hw_id = "", $team_id = ""){
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);

		$ch2_comment ="";
		$ch2_text = "";
		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			$data = $this->pageInit("文獻查證");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$literature_data = $this->ch2_model->getLiterature_model($_SESSION["team_id"], $_SESSION["hw_id"]);

			$ch2_text .=
						'<div class="ibox-content">' .
									'<div class="form-horizontal">' .
										'<div class="form-group">' .
											'<label class="col-sm-12">文獻查證：</label>' .
											'<div class="col-sm-12"><textarea class="form-control" rows="20" placeholder="文獻" id="Literature" style="resize: none;">' . $literature_data['literature_text'] . '</textarea></div>' .
											'<div class="col-sm-12"><br><button class="btn btn-primary" id="btn_lit" >儲存</button></div>' .
										'</div>' .
									'</div>' .
								'</div>' ;


			$ch2_comment .=
						'<div class="ibox-content">' .
							'<div class="form-horizontal">' .
								'<p class="text-danger form-control">'.$literature_data['comment'].'</p>' .
							'</div>' .
						'</div>';

			$data["comment"] = $ch2_comment;
			$data["ch2_text"] = $ch2_text;
			$this->load->view("ch2_view" , $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			$data = $this->pageInit("文獻查證");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$literature_data = $this->ch2_model->getLiterature_model($_SESSION["team_id"], $_SESSION["hw_id"]);

			$ch2_text .=
						'<div class="ibox-content">' .
							'<div class="form-horizontal">' .
								'<div class="form-group">' .
									'<label class="col-sm-12">文獻查證：</label>' .
									'<div class="col-sm-12"><p class="form-control" rows="10" placeholder="文獻" id="Literature" style="resize: none;">' . $literature_data['literature_text'] . '</p></div>' .
								'</div>' .
							'</div>' .
						'</div>' ;


			$ch2_comment .=
						'<div class="ibox-content">' .
							'<div class="form-horizontal">' .
								'<textarea class="form-control" name="comment" rows="20" placeholder="老師評語">'.$literature_data['comment'].'</textarea>' .
								'<div class="form-group">' .
									'<div class="col-sm-10"><br>' .
										'<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>' .
									'</div>' .
								'</div>' .
							'</div>' .
						'</div>' ;
			$data["comment"] = $ch2_comment;
			$data["ch2_text"] = $ch2_text;
			$this->load->view("ch2_view" , $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			$data = $this->pageInit("文獻查證");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$literature_data = $this->ch2_model->getLiterature_model($_SESSION["team_id"], $_SESSION["hw_id"]);

			$ch2_text .=
						'<div class="ibox-content">' .
							'<div class="form-horizontal">' .
								'<div class="form-group">' .
									'<label class="col-sm-12">文獻查證：</label>' .
									'<div class="col-sm-12"><textarea class="form-control" rows="20" placeholder="文獻" id="Literature" style="resize: none;">' . $literature_data['literature_text'] . '</textarea></div>' .
									'<div class="col-sm-12"><br><button class="btn btn-primary" id="btn_lit" >儲存</button></div>' .
								'</div>' .
							'</div>' .
						'</div>' ;


			$ch2_comment .=
						'<div class="ibox-content">' .
							'<div class="form-horizontal">' .
								'<textarea class="form-control" name="comment" rows="20" placeholder="老師評語">'.$literature_data['comment'].'</textarea>' .
								'<div class="form-group">' .
									'<div class="col-sm-10"><br>' .
										'<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>' .
									'</div>' .
								'</div>' .
							'</div>' .
						'</div>' ;
			$data["comment"] = $ch2_comment;
			$data["ch2_text"] = $ch2_text;
			$this->load->view("ch2_view" , $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ){
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ){
			redirect("mark");
		}
		else{
			$data = $this->pageInit("文獻查證");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$literature_data = $this->ch2_model->getLiterature_model($_SESSION["team_id"], $_SESSION["hw_id"]);

			$ch2_text .=
						'<div class="ibox-content">' .
							'<div class="form-horizontal">' .
								'<div class="form-group">' .
									'<label class="col-sm-12">文獻查證：</label>' .
									'<div class="col-sm-12"><p class="form-control">' . $literature_data['literature_text'] . '</p></div>' .
								'</div>' .
							'</div>' .
						'</div>' ;


			$ch2_comment .=
						'<div class="ibox-content">' .
							'<div class="form-horizontal">' .
								'<p class="text-danger form-control" name="comment" rows="4" placeholder="老師評語">'.$literature_data['comment'].'</p>' .
							'</div>' .
						'</div>' ;
			$data["comment"] = $ch2_comment;
			$data["ch2_text"] = $ch2_text;
			$this->load->view("ch2_view" , $data);
		}
	}

	public function editLiterature() {
		$this->ch2_model->editLiterature_model($this->security->xss_clean($_POST), $_SESSION["team_id"],  $_SESSION["hw_id"]);
		echo true;
	}

	public function editComment() {
		$comment_value = !empty($_POST["comment_value"]) ? $this->security->xss_clean($_POST["comment_value"]) : "";
		$this->ch2_model->editComment_model($comment_value, $_SESSION["team_id"], $_SESSION["hw_id"]);
		echo true;
	}

}
