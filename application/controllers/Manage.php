<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Manage extends MY_Authentication {

	private $defalut_value;

	public function __construct() {
		parent::__construct();
		$this->load->model("manage_model");
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("帳號管理");
			$this->load->view("manage_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * [getUser 取得所有使用者資訊]
	 * @return [String] [所有成員資訊與編輯按鈕]
	 */
	function getUser(){
		$permission = $this->getPermission();
		$this->load->library("MY_Datatables_Lib");
		$this->my_datatables_lib
		->select("SHA1(CONCAT('ntin_',account)) as acc, account, name, email, opt_permission.opt_permission_name, opt_user_status.status_name, entry, class")
		->from("user")
		->join("opt_permission" , "user.permission_id = opt_permission.opt_permission_id")
		->join("opt_user_status" , "user.status = opt_user_status.status_id")
		->where("permission_id >", $permission)
		->where("permission_id !=", $permission)
		->add_column("edit_btn",
		"<div class='text-center'>
			<a class='btn btn-primary btn-xs' href='".base_url()."manage/userInfo/$1'>編輯</a>
			<a class='btn btn-warning btn-xs chg_status' data-acc='$1'>$2</a>
		</div>" , "acc,status_name");
		echo $this->my_datatables_lib->generate();
	}

	/**
	 * [insert 新增使用者]
	 * @return [type] [null]
	 */
	public function insert(){
		if ($this->getLoginStatus() === 1){
			$this->load->model("common_model");
			$this->load->library("form_validation");
			$data = $this->pageInit("新增使用者");
			$data["permission_opt"] = $this->getOptPermission();
			if (empty($_SESSION["insert_token"])) {
				$_SESSION["insert_token"] = sha1(time());
			}
			$this->load->view("mag_insert_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * [驗證表單並新增使用者基本資料]
	 * @return [type] [description]
	 */
	function insertUser(){
		$this->load->library("form_validation");
		$this->form_validation->set_rules("account", "學號(帳號)", "trim|required",
			array("required" => "%s不能為空")
		);
		$this->form_validation->set_rules("user_name", "姓名", "trim|required",
			array("required" => "%s不能為空")
		);
		$this->form_validation->set_rules("email", "信箱", "trim|required|valid_email",
			array("required" => "%s不能為空" , "valid_email" => "%s格式錯誤" )
		);
		$this->form_validation->set_rules("entry", "入學年", "trim|numeric|required",
			array("required" => "%s不能為空" , "numeric" => "%s格式需為數字")
		);
		$this->form_validation->set_rules("class", "班級", "trim|numeric|required",
			array("required" => "%s不能為空" , "numeric" => "%s格式需為數字")
		);
		if($this->form_validation->run() == FALSE){
			$this->insert();
		} else {
			if(!empty($_POST["token"]) && $_POST["token"] === $_SESSION["insert_token"]){
				if($this->manage_model->checkAc_model($this->security->xss_clean($_POST))){
					$account = $this->manage_model->insertUser_model($this->security->xss_clean($_POST))->row()->account;
					$_SESSION["insert_token"] = sha1($_SESSION["insert_token"]);
					header("Location: " . base_url() . "manage");
				} else {
					echo "帳號重複存在";
					header("Refresh: 3; url=" . base_url() . "manage");
				}
				unset($_POST);
			} else {
				echo "請勿重複儲存";
				header("Refresh: 3; url=" . base_url() . "manage");
			}
		}
	}

	/**
	 * [userInfo 使用者基本資料]
	 * @param  [String] $account [使用者的帳號]
	 * @return [type]          [資本資料表單]
	 */
	public function userInfo($account = ""){
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("使用者帳號");
			if(!empty($account)){
				$account = $this->security->xss_clean($account);
				$data["info"] = $this->getUserInfo($account);
				$_SESSION["account_index"] = $account;
				$this->load->view("userInfo_view" , $data);
			} else {
				echo "請由正常管道選擇帳號";
				header("Refresh: 3; url=".base_url()."manage");
			}
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * [getUserInfo 取得使用者基本資料]
	 * @param  [String] $account [使用者帳號]
	 * @return [String]          [description]
	 */
	private function getUserInfo($account){
		$result = $this->manage_model->getUserInfo_model($account)->row();
		if (count($result) > 0) {
			$infoHTML = "";
			$col = array("姓名","信箱","身份","狀態","入學年","班級");
			$i = 0;

			foreach ($result as $key => $value) {
				$infoHTML .= "<tr><td>".$col[$i]."</td><td>".$value."</td></tr>";
				$i++;
			}
			return $infoHTML;
		}
		exit();
	}

	/**
	 * [設定基本資料]
	 * @return [type] [description]
	 */
	function userSetting(){
		if ($this->getLoginStatus() === 1){
			if(!empty($_SESSION["account_index"])){
				$this->load->model("common_model");
				$this->load->library("form_validation");
				$this->defalut_value = $this->manage_model->getDefault_model()->row();
				if(count($this->defalut_value) > 0){
					$data = $this->pageInit("帳號設定");
					$data["permission_opt"] = $this->getOptPermission($this->defalut_value->permission_id);
					$data["object"] = $this;
					$this->load->view("userSetting_view" , $data);
				}
			} else {
				echo "請由正常管道選擇帳號";
				header("Refresh: 3; url=".base_url()."manage");
			}
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * [驗證表單並更新基本資料]
	 * @return [type] [description]
	 */
	function updateUserInfo(){
		if ($this->getLoginStatus() === 1){
			$this->load->library("form_validation");
			if($this->getLoginStatus() === 1 && !empty($_SESSION["account_index"])){
				$this->form_validation->set_rules("pw", "密碼", "trim|max_length[15]",
					array("trim" => "請勿輸入含有空白" , "max_length" => "密碼長度不超過15字")
				);
				$this->form_validation->set_rules("user_name", "姓名", "trim|required",
					array("required" => "%s不能為空")
				);
				$this->form_validation->set_rules("email", "信箱", "trim|required|valid_email",
					array("required" => "%s不能為空" , "valid_email" => "%s格式錯誤" )
				);
				$this->form_validation->set_rules("entry", "入學年", "trim|numeric|required",
					array("required" => "%s不能為空" , "numeric" => "%s格式需為數字")
				);
				$this->form_validation->set_rules("class", "班級", "trim|numeric|required",
					array("required" => "%s不能為空" , "numeric" => "%s格式需為數字")
				);
				if ($this->form_validation->run() == FALSE){
					$this->userSetting();
				} else {
					$this->manage_model->updateUserInfo_model($_POST);
					unset($_SESSION["account_index"]);
					header("Location: " . base_url() . "manage");
				}
			}
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * [取得權限項目]
	 * @return [type] [description]
	 */
	private function getOptPermission($permission_id = ""){
		$result = $this->manage_model->getPermission_model($this->getPermission())->result_array();
		$optHTML = "";
		for ($i = 0; $i < count($result); $i++) {
			$optHTML .= "<option value=".$result[$i]["opt_permission_id"];
			if(!empty($permission_id) && $result[$i]["opt_permission_id"] == $permission_id){
				$optHTML .= " selected";
			}
			$optHTML .= ">".$result[$i]["opt_permission_name"]."</option>";
		}

		return $optHTML;
	}

	/**
	 * [初始給予預設舊資料]
	 * @param  [type] $index [description]
	 * @return [type]        [description]
	 */
	function getDefault($index){
		if($_SERVER["REQUEST_METHOD"] != "POST"){
			return $this->defalut_value->$index;
		}
	}

	/**
	 * [切換帳號使用狀態]
	 * @return [type] [description]
	 */
	function updateStatus(){
		if ($this->getLoginStatus() === 1 && $_POST["acc"]){
			$this->load->model("common_model");
			$permission = $this->getPermission();
			$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
			$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
			if($permission == $min || $permission == $second){
				$account = $this->security->xss_clean($_POST["acc"]);
				$this->manage_model->updateStatus_model($account);
			}
		}
	}
}
