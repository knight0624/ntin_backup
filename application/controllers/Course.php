<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Course extends MY_Authentication {

	function __construct(){
		parent::__construct();
		$this->load->model("course_model");
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("編輯課程");
			$data["year_opt"] = $this->getYear();
			$data["instructor_opt"] = $this->getInstructor();
			$data["writable"] = false;
			$this->load->model("common_model");
			$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
			$permission = $this->getPermission();
			if($permission == $min) {
				$data["writable"] = true;
			}
			$this->load->view("course_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * [getUser 取得所有課程資訊]
	 * @return [String] [所有成員資訊與編輯按鈕]
	 */
	function getCourse(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if($this->getPermission() == $min || $this->getPermission() == $second ){
			$this->load->library("MY_Datatables_Lib");
			$this->my_datatables_lib
			->select("SHA1(CONCAT('ntin_',course.course_id)) as course_id , course.course_name , course.academic_year, user.name , IF(course.semester = 1 , '上學期' , '下學期') as semester , IF(course.course_status = 1 , '啟用' , '隱藏') as course_status")
			->from("course")
			->join("user", "course.instructor = user.account");
			// 如果有選擇年份就進行過濾
			if( !empty($_POST["select_year"]) ){
				$select_year = $this->security->xss_clean($_POST["select_year"]);
				$this->my_datatables_lib->where("course.academic_year" , $select_year);
			}
			if ( $this->getPermission() == $min ) {
				$this->my_datatables_lib->add_column("edit_btn",
				"<div class='text-center'>
					<a class='btn btn-primary btn-xs' href='".base_url()."course/detail/$1'><i class='fa fa-search'></i></a>
					<button data-edit='$1' class='btn btn-warning btn-xs editbtn'><i class='fa fa-pencil-square-o'></i></button>
					<button data-lock='$1' class='btn btn-danger btn-xs lockbtn'><i class='fa fa-trash-o'></i></button>
				</div>",
				"course_id");
			}
			elseif ( $this->getPermission() == $second ) {
				$this->my_datatables_lib->where("course_status" , 1);
				$this->my_datatables_lib->where("instructor" , $_SESSION["username"]);
				$this->my_datatables_lib->add_column("edit_btn",
				"<div class='text-center'>
					<a class='btn btn-primary btn-xs' href='".base_url()."course/detail/$1'><i class='fa fa-search'></i></a>
				</div>",
				"course_id");
			}

			echo $this->my_datatables_lib->generate();
		}
	}

	/**
	 * [getYear 取得year]
	 * @return [String] [year選單]
	 */
	private function getYear(){
		$this->load->model("common_model");
		$result = $this->common_model->getCourseYear_model()->result_array();
		$optHTML = "";
		for ($i = 0; $i < count($result); $i++) {
			$optHTML .= "<option value=".$result[$i]["academic_year"].">".$result[$i]["academic_year"]."</option>";
		}
		return $optHTML;
	}

	private function getStudentEntry(){
		$this->load->model("common_model");
		$result = $this->common_model->getCourseEntry_model()->result_array();
		$optHTML = "";
		for ($i = 0; $i < count($result); $i++) {
			$optHTML .= "<option value=".$result[$i]["entry"].">".$result[$i]["entry"]."</option>";
		}
		return $optHTML;
	}

	private function getStudentClass(){
		$this->load->model("common_model");
		$result = $this->common_model->getStudentClass_model()->result_array();
		$optHTML = "";
		for ($i = 0; $i < count($result); $i++) {
			$optHTML .= "<option value=".$result[$i]["class"].">".$result[$i]["class"]."</option>";
		}
		return $optHTML;		
	}

	/**
	 * [getInstructor 取得授課老師]
	 */
	private function getInstructor(){
		$this->load->model("common_model");
		$result = $this->common_model->getInstructor_model()->result_array();
		$optHTML = "";
		for ($i = 0; $i < count($result); $i++) {
			$optHTML .= "<option value=".$result[$i]["account"].">".$result[$i]["name"]."</option>";
		}
		return $optHTML;
	}

	function getCourseDetial(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if( ($min == $permission || $second == $permission) && !empty($_POST["edit"]) ){
			$result = $this->course_model->getCourseDetial_model($this->security->xss_clean($_POST["edit"]) , $permission , $second)->row();
			echo json_encode($result);
		}
	}

	function setCourse(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if($min == $permission || $second == $permission){
			$data = $this->security->xss_clean($_POST);
			foreach ($data as $key => $value) {
				if(empty($value)){
					exit();
				}
			}
			$this->course_model->setCourse_model($data);
		}
	}

	function updateCourse(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if($min == $permission || $second == $permission){
			$data = $this->security->xss_clean($_POST);
			foreach ($data as $key => $value) {
				if(empty($value)){
					exit();
				}
			}
			$this->course_model->updateCourse_model($data);
		}
	}

	function lockCourse(){
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if( ($min == $permission || $second == $permission) && !empty($_POST["course_id"]) ){
			$data = $this->security->xss_clean($_POST);
			$this->course_model->lockCourse_model($data);
		}
	}

	function detail($course_id = ""){
		if ($this->getLoginStatus() === 1 && !empty($course_id) ){
			$this->load->model("common_model");
			$permission = $this->getPermission();
			$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
			$result = $this->course_model->getCourseDetial_model($this->security->xss_clean($course_id) , $permission , $second)->row();
			if(count($result)){
				$_SESSION["course_id"] = $result->course_id;
				$data = $this->pageInit("修課學生管理");
				$data["course_name"] = $result->academic_year."學年-".($result->semester == 1 ? "上學期-" : "下學期-").$result->course_name;
				$data["num"] = $this->getNum();
				$data["entry_opt"] = $this->getStudentEntry();
				$data["class_opt"] = $this->getStudentClass();
				$data["student"] = $this->getCourseStudent();
				$this->load->view("course_det_view" , $data);
			} else {
				echo "請透過正確管道瀏覽";
				header("Refresh: 3; url=".base_url()."course");
			}
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	function getNum(){
		if(!empty($_POST["ajax"]) && $this->security->xss_clean($_POST["ajax"])){
			echo $this->course_model->getNum_model();
		} else {
			return $this->course_model->getNum_model();
		}
	}

	function getCourseStudent(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if($min == $permission || $second == $permission){
			$year = date("Y")-1911;
			$class = NULL;
			$year = !empty($_POST["year"]) ? $this->security->xss_clean($_POST["year"]) : $year;
			$class = !empty($_POST["cls"]) ? $this->security->xss_clean($_POST["cls"]) : $class;
			$optHTML = "";
			$result = $this->course_model->getCourseStudent_model($_SESSION["course_id"] , $class , $year)->result_array();
			for ($i=0; $i < count($result); $i++) {
				$optHTML .= "<tr><td><input type='checkbox' class='student' value='".$result[$i]["account"]."'";
				if(!is_null($result[$i]["course_id"])){
					$optHTML .= " checked ";
				}
				$optHTML .= "></td><td>".$result[$i]["account"]."</td><td>".$result[$i]["name"]."</td><td>".$result[$i]["entry"]."</td><td>".$result[$i]["class"]."</td></tr>";
			}

			if(!empty($_POST)){
				echo $optHTML;
			} else {
				return $optHTML;
			}
		}
	}

	function setStudent(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if($min == $permission || $second == $permission){
			$this->course_model->setStudent_model($this->security->xss_clean($_POST["acc"]));
		}
	}

	function deleteStudent(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if($min == $permission || $second == $permission){
			$this->course_model->deleteStudent_model($this->security->xss_clean($_POST["acc"]));
		}
	}

	function batch(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if($min == $permission || $second == $permission){
			$data = $this->security->xss_clean($_POST);
			$this->course_model->batch_model(json_decode($data["data"]) , $data["fun"]);
		}
	}
}
?>