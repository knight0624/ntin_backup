<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch4 extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model("ch4_model","",TRUE);
		$this->load->helper(array('form', 'url'));
		$this->load->model("common_model","",TRUE);

	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$this->write($_SESSION["hw_id"], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	function write($hw_id = "", $team_id = ""){
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);

		$comment ="";
		$ch4_text = "";
		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			$data = $this->pageInit("個案家庭成員");
			$data["permission"] = 1;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data['member'] = $this->ch4_model->listMember_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$data['show_fm_other'] = $this->ch4_model->getFm_other_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$data['img_file_name'] = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"img");
			$data['ppt_file_name'] = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"ppt");
			$data['comment_data'] = $this->ch4_model->getComment_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$member_list = $this->input->post();

			if(!empty($member_list)){
				$this->ch4_model->addMember_model($member_list,$_SESSION["team_id"],$_SESSION["hw_id"]);
				redirect("Ch4", $data);
			}
			else{
				$member_list = $this->input->post();
			}

			$this->load->view("ch4_view" , $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			$data = $this->pageInit("個案家庭成員");
			$data["permission"] = 2;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data['member'] = $this->ch4_model->listMember_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$data['show_fm_other'] = $this->ch4_model->getFm_other_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$data['img_file_name'] = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"img");
			$data['ppt_file_name'] = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"ppt");
			$data['comment_data'] = $this->ch4_model->getComment_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$member_list = $this->input->post();

			if(!empty($member_list)){
				$this->ch4_model->addMember_model($member_list,$_SESSION["team_id"],$_SESSION["hw_id"]);
				redirect("Ch4", $data);
			}
			else{
				$member_list = $this->input->post();
			}

			$this->load->view("ch4_view" , $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			$data = $this->pageInit("個案家庭成員");
			$data["permission"] = 3;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data['member'] = $this->ch4_model->listMember_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$data['show_fm_other'] = $this->ch4_model->getFm_other_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$data['img_file_name'] = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"img");
			$data['ppt_file_name'] = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"ppt");
			$data['comment_data'] = $this->ch4_model->getComment_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$member_list = $this->input->post();

			if(!empty($member_list)){
				$this->ch4_model->addMember_model($member_list,$_SESSION["team_id"],$_SESSION["hw_id"]);
				redirect("Ch4", $data);
			}
			else{
				$member_list = $this->input->post();
			}

			$this->load->view("ch4_view" , $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ){
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ){
			redirect("mark");
		}
		else {
			$data = $this->pageInit("個案家庭成員");
			$data["permission"] = 6;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data['member'] = $this->ch4_model->listMember_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$data['show_fm_other'] = $this->ch4_model->getFm_other_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$data['img_file_name'] = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"img");
			$data['ppt_file_name'] = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"ppt");
			$data['comment_data'] = $this->ch4_model->getComment_model($_SESSION["team_id"],$_SESSION["hw_id"]);
			$member_list = $this->input->post();

			if(!empty($member_list)){
				$this->ch4_model->addMember_model($member_list,$_SESSION["team_id"],$_SESSION["hw_id"]);
				redirect("Ch4", $data);
			}
			else{
				$member_list = $this->input->post();
			}

			$this->load->view("ch4_view" , $data);
		}
	}


	/**
	 * 刪除家庭成員
	 * @return [type] [description]
	 */
	public function delete() {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("個案家庭成員");

			$data['member'] = $this->ch4_model->listMember_model();

			$delete_member = $this->input->post();
			$this->ch4_model->deleteMember_model($delete_member);
			redirect("Ch4", $data);
			$this->load->view("ch4_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}


	/**
	 * 新增補充說明[addFm_other description]
	 */
	public function addFm_other() {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("個案家庭成員");

			$data['member'] = $this->ch4_model->listMember_model();

			$fm_other = $this->input->post();

			if(!empty($fm_other['fm_other'])){
				$this->ch4_model->addFm_other_model($fm_other,$_SESSION["team_id"],$_SESSION["hw_id"]);
				echo true;
			}else{
				echo false;
			}

			redirect("Ch4", $data);
			$this->load->view("ch4_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function editMember($edit_type) {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("個案家庭成員");

			$edit_data = $this->input->post();

			if(!empty($edit_data['edit_data'])){
				$this->ch4_model->editMember_model($edit_data, $edit_type, $_SESSION["team_id"],$_SESSION["hw_id"]);
				echo true;
			}else{
				echo false;
			}

			redirect("Ch4", $data);
			$this->load->view("ch4_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
     * [上傳檔案]
     * @return [type] [description]
     */
    public function do_upload() {
    	$newfilename = "家系圖圖片_".$_SESSION["main_id"]."_".date("Ymdhi");
        $config['upload_path'] = './uploads/img';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['max_size'] = 8192;
        $config['file_name'] = $newfilename;
		$config['overwrite'] = true;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('upload_error_view', $error);
        }
        else {
        	$data = $this->upload->data();
        	$old_name = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"img");
        	if ($data["file_name"] != $old_name) {
        		if (! empty($old_name) && is_readable("uploads/img/".$old_name)) {
        			unlink("uploads/img/".$old_name);
        		}
        	}
			$this->ch4_model->setFile_path_model($data,$_SESSION["hw_id"],$_SESSION["team_id"],"img");
            redirect("Ch4", $data);
			$this->load->view("ch4_view" , $data);
        }
    }

    public function do_upload_ppt() {
    	$newfilename = "家系圖PPT_".$_SESSION["main_id"]."_".date("Ymdhi");
        $config['upload_path'] = './uploads/ppt';
        $config['allowed_types'] = 'ppt|pptx';
        $config['max_size'] = 8192;
        $config['file_name'] = $newfilename;
		$config['overwrite'] = true;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('upload_error_view', $error);
        }
        else {
        	$data = $this->upload->data();
        	$old_name = $this->ch4_model->getFile_name_model($_SESSION["hw_id"],$_SESSION["team_id"],"ppt");
        	if ($data["file_name"] != $old_name) {
        		if (! empty($old_name) && is_readable("uploads/ppt/".$old_name)) {
        			unlink("uploads/ppt/".$old_name);
        		}
        	}
			$this->ch4_model->setFile_path_model($data,$_SESSION["hw_id"],$_SESSION["team_id"],"ppt");
            redirect("Ch4", $data);
			$this->load->view("ch4_view" , $data);
        }
    }

    public function deleteFile_img() {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("個案家庭成員");

			$data['member'] = $this->ch4_model->listMember_model();

			$delete_file = $this->input->post();
			$this->ch4_model->deleteFile_img_model($delete_file);
			redirect("Ch4", $data);
			$this->load->view("ch4_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function deleteFile_ppt() {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("個案家庭成員");

			$data['member'] = $this->ch4_model->listMember_model();

			$delete_file = $this->input->post();
			$this->ch4_model->deleteFile_ppt_model($delete_file);
			redirect("Ch4", $data);
			$this->load->view("ch4_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function addComment() {
		$comment = !empty($_POST["comment_value"]) ? $this->security->xss_clean($_POST["comment_value"]) : "";
		$this->ch4_model->addComment_model($comment, $_SESSION["team_id"], $_SESSION["hw_id"]);
		echo true;
	}
}
