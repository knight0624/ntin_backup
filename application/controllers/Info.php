<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Info extends MY_Authentication {

	private $defalut_value;

	public function __construct() {
		parent::__construct();
		$this->load->model("info_model");
		$this->defalut_value = $this->info_model->getDefault_model()->row();
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("我的帳號");
			$data["info"] = $this->getInfo();
			$this->load->view("info_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * [取得基本資料]
	 * @return [type] [description]
	 */
	private function getInfo(){
		$result = $this->info_model->getInfo_model()->row();

		$infoHTML = "";
		$col = array("姓名","信箱","身份","狀態","入學年","班級");
		$i = 0;

		foreach ($result as $key => $value) {
			$infoHTML .= "<tr><td>".$col[$i]."</td><td>".$value."</td></tr>";
			$i++;
		}
		return $infoHTML;
	}

	/**
	 * [設定基本資料]
	 * @return [type] [description]
	 */
	function setting(){
		if ($this->getLoginStatus() === 1){
			$this->load->library("form_validation");
			$data = $this->pageInit("帳號設定");
			$data["object"] = $this;
			$this->load->view("setting_view" , $data);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * [驗證表單並更新基本資料]
	 * @return [type] [description]
	 */
	function updateInfo(){

		$this->load->library("form_validation");

		$this->form_validation->set_rules("old_pw", "密碼", "required|callback_checkPw|trim|max_length[15]",
			array("required" => "修改資料需先提供舊密碼" , "trim" => "請勿輸入含有空白" , "max_length" => "密碼長度不超過15字")
		);
		$this->form_validation->set_rules("pw", "密碼", "matches[c_pw]|trim|max_length[15]",
			array("matches" => "密碼輸入不一致" , "trim" => "請勿輸入含有空白" , "max_length" => "密碼長度不超過15字")
		);
		$this->form_validation->set_rules("c_pw", "密碼", "matches[pw]|trim|max_length[15]",
			array("matches" => "密碼輸入不一致" , "trim" => "請勿輸入含有空白" , "max_length" => "密碼長度不超過15字")
		);
		$this->form_validation->set_rules("user_name", "姓名", "required|trim",
			array("required" => "%s不能為空" , "trim" => "請勿輸入含有空白")
		);
		$this->form_validation->set_rules("email", "信箱", "required|valid_email|trim",
			array("required" => "%s不能為空" , "valid_email"=>"%s格式錯誤"  , "trim" => "請勿輸入含有空白")
		);
		$this->form_validation->set_rules("entry", "入學年", "trim|numeric|required",
			array("required" => "%s不能為空" , "numeric" => "%s格式需為數字")
		);
		$this->form_validation->set_rules("class", "班級", "trim|numeric|required",
			array("required" => "%s不能為空" , "numeric" => "%s格式需為數字")
		);
		if ($this->form_validation->run() == FALSE){
			$this->setting();
		} else {
			$this->info_model->updateInfo_model($_POST);
			if(!empty($_POST["pw"])){
				$data = $this->pageInit("進行登出");
				$this->load->view("chgpw_view" , $data);
			}
			$this->index();
		}
	}

	/**
	 * [確認密碼輸入是否正確]
	 * @param  [type] $pw [description]
	 * @return [type]     [description]
	 */
	function checkPw($pw){
		if(!$this->info_model->checkPw_model($pw)){
			$this->form_validation->set_message("checkPw", "舊密碼錯誤");
			return false;
		}
		return true;
	}

	/**
	 * [初始給予預設舊資料]
	 * @param  [type] $index [description]
	 * @return [type]        [description]
	 */
	function getDefault($index){
		if($_SERVER["REQUEST_METHOD"] != "POST"){
			return $this->defalut_value->$index;
		}
	}
}
