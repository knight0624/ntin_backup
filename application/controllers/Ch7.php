<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch7 extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model('ch7_model','',TRUE);
		$this->load->model("common_model","",TRUE);
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$this->write($_SESSION["hw_id"], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function write($hw_id = "", $team_id = "") {
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);
		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			// 學生有main_id
			$data = $this->pageInit("家庭發展");
			$data["permission"] = 1;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch7_model->getTeam_All_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch7_view", $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			// 老師有main_id
			$data = $this->pageInit("家庭發展");
			$data["permission"] = 2;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch7_model->getTeam_All_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch7_view", $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			// admin
			$data = $this->pageInit("家庭發展");
			$data["permission"] = 3;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch7_model->getTeam_All_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch7_view", $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ) {
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ) {
			redirect("mark");
		}
		else{
			// other
			$data = $this->pageInit("家庭發展");
			$data["permission"] = 6;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch7_model->getTeam_All_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch7_view", $data);
		}
	}

	/**
	 * [表單]
	 * @return [type]
	 */
	public function fdTable() {
		$check = json_decode($this->input->post('table_value') );
		if (count($check) >= 3) {
		//確認表單至少勾選三項
			for ($i = 0; $i < count($check) - 2; $i++) 
				$table_tasks[$i] = $check[$i+2];
				//把tasks欄位加入table_tasks
			$tasks = implode(",",$table_tasks);
			//用","把table_tasks陣列轉成tasks字串
			$table_value = array(
				'fd_periods' => $check[0],
				'fd_stages' => $check[1],
				'fd_tasks' => $tasks
				);
			$this->ch7_model->tableUpdate_model($_SESSION["team_id"] , $_SESSION["hw_id"],  $table_value);
			echo true;
		} else 
			echo false;
	}

	/**
	 * [補充]
	 * @return [type]
	 */
	public function fdSuppleMent() {
		$fdt_supple_ment = $this->input->post('supple_ment_value');
		! empty($fdt_supple_ment) ? $this->security->xss_clean($fdt_supple_ment) : "";
		$status = $this->ch7_model->addSuppleMent_model($_SESSION["team_id"], $_SESSION["hw_id"], $fdt_supple_ment);
		echo $status;
	}

	public function addComment() {
		$comment = $this->input->post('comment_value');
		! empty($comment) ? $this->security->xss_clean($comment) : "";
		$this->ch7_model->addComment_model($_SESSION["team_id"] ,$_SESSION["hw_id"],  $comment);
		echo true;
	}
}