<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Team extends MY_Authentication {

	private $defalut_value;

	public function __construct() {
		parent::__construct();
		$this->load->model("team_model");
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$data = $this->pageInit("分組管理");
			$this->load->model("common_model");
			$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
			$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
			$permission = $this->getPermission();
			if($min == $permission || $second == $permission){
				$type = "";
				if($min == $permission){
					$type = "1";
				} elseif($second == $permission){
					$type = "2";
				}
				$optHTML = "<option value=''>選擇課程</option>";
				$result = $this->team_model->getCourseOpt_model($type)->result_array();
				for ($i=0; $i < count($result); $i++) {
					$optHTML .= "<option value='".$result[$i]["course_id"]."'>".$result[$i]["item_name"]."</option>";
				}
				$data["opt"] = $optHTML;
				$this->load->view("team_view" , $data);
			}
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function setting($course_id , $hw_id) {
		if ($this->getLoginStatus() === 1 && !empty($course_id)){
			$this->load->model("common_model");
			$this->load->model("course_model");
			$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
			$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
			$permission = $this->getPermission();
			$name = $this->course_model->getCourseDetial_model($this->security->xss_clean($course_id) , $permission , $second)->row();
			if($min == $permission || $second == $permission){
				$result = $this->team_model->getCourseInfo_model($this->security->xss_clean($course_id))->row();
				if(count($result)){
					$_SESSION["course_id"] = $result->course_id;
					$_SESSION["hw_id"] = $hw_id;
					$data = $this->pageInit("分組管理");
					$data["entry_opt"] = $this->getStudentEntry();
					$data["class_opt"] = $this->getStudentClass();
					$data["team_opt"] = $this->getTeamOpt();
					$data["course_name"] = $name->academic_year."學年-".($name->semester == 1 ? "上學期-" : "下學期-").$name->course_name;
					$data["NotHwStudent"] = $this->getNotHwStudent();
					$data["HwStudent"] = $this->getHwStudent();
					$this->load->view("team_setting_view" , $data);
				} else {
					echo "請透過正確管道瀏覽";
					header("Refresh: 3; url=".base_url()."team");
				}
			}
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * [未分組名單]
	 * @return [type] [description]
	 */
	function getNotHwStudent(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if(($min == $permission || $second == $permission) && !empty($_SESSION["course_id"]) ){
			$year = "all";
			$class = "all";
			$year = !empty($_POST["year"]) ? $this->security->xss_clean($_POST["year"]) : $year;
			$class = !empty($_POST["cls"]) ? $this->security->xss_clean($_POST["cls"]) : $class;

			$optHTML = "";
			$result = $this->team_model->getNotHwStudent_model($year , $class)->result_array();
			for ($i=0; $i < count($result); $i++) {
				$optHTML .= "<tr>
								<td><input type='checkbox' class='i_student' value='".$result[$i]["account"]."'></td>
								<td>".$result[$i]["account"]."</td>
								<td>".$result[$i]["name"]."</td>
								<td>".$result[$i]["entry"]."</td>
								<td>".$result[$i]["class"]."</td>
							</tr>";
			}
			if(!empty($_POST)){
				echo $optHTML;
			} else {
				return $optHTML;
			}
		}
	}

	/**
	 * [已分組名單]
	 * @return [type] [description]
	 */
	public function getHwStudent(){
		$this->load->model("common_model");
		$min = $this->common_model->getNumofPermission_model("min")->row()->permission_id;
		$second = $this->common_model->getNumofPermission_model(2)->row()->permission_id;
		$permission = $this->getPermission();
		if(($min == $permission || $second == $permission) && !empty($_SESSION["course_id"]) ){
			$team_id = NULL;
			$team_id = !empty($_POST["team_id"]) ? $this->security->xss_clean($_POST["team_id"]) : $team_id;
			$optHTML = "";
			$result = $this->team_model->getHwStudent_model($team_id)->result_array();
			for ($i=0; $i < count($result); $i++) {
				$optHTML .= "<tr>
								<td><input type='checkbox' class='d_student' value='".$result[$i]["account"]."'></td>
								<td>".$result[$i]["account"]."</td>
								<td>".$result[$i]["name"]."</td>
								<td>".$result[$i]["entry"]."</td>
								<td>".$result[$i]["class"]."</td>
							</tr>";
			}
			if(!empty($_POST)){
				echo $optHTML;
			} else {
				return $optHTML;
			}
		}
	}

	public function getTeamOpt(){
		if(!empty($_SESSION["course_id"])){
			$result = $this->team_model->getTeamOpt_model()->result_array();
			$optHTML = "<option value=''>篩選分組</option>";
			for ($i = 0; $i < count($result); $i++) {
				$optHTML .= "<option value=".$result[$i]["team_id"].">".$result[$i]["name"]."</option>";
			}
			if(!empty($_POST["ajax"])){
				echo $optHTML;
			}else{
				return $optHTML;
			}
		}
	}

	private function getStudentEntry(){
		$this->load->model("common_model");
		$result = $this->common_model->getCourseEntry_model()->result_array();
		$optHTML = "";
		for ($i = 0; $i < count($result); $i++) {
			$optHTML .= "<option value=".$result[$i]["entry"].">".$result[$i]["entry"]."</option>";
		}
		return $optHTML;
	}

	private function getStudentClass(){
		$this->load->model("common_model");
		$result = $this->common_model->getStudentClass_model()->result_array();
		$optHTML = "";
		for ($i = 0; $i < count($result); $i++) {
			$optHTML .= "<option value=".$result[$i]["class"].">".$result[$i]["class"]."</option>";
		}
		return $optHTML;
	}

	function setTeaminfo(){
		$data = $this->security->xss_clean(json_decode($_POST["acc"]));
		$team_name = $this->security->xss_clean($_POST["team_name"]);
		if(count($data) && !empty($team_name)){
			$this->team_model->setTeaminfo_model($data , $team_name);
		}
	}

	function deleteTeaminfo(){
		$u_acc = !empty($_POST["u_acc"]) ? $this->security->xss_clean(json_decode($_POST["u_acc"])) : "";
		$i_acc = !empty($_POST["i_acc"]) ? $this->security->xss_clean(json_decode($_POST["i_acc"])) : "";
		$this->team_model->deleteTeaminfo_model($u_acc , $i_acc , $this->security->xss_clean($_POST["team"]), $this->security->xss_clean($_POST["team_name"]) );
	}

	function getHw(){
		$course_id = $this->security->xss_clean($_POST["course_id"]);
		$result = $this->team_model->getHw_model($course_id)->result_array();
		$optHTML = "";
		for ($i = 0; $i < count($result); $i++) {
			$optHTML .= "<option value=".$result[$i]["hw_id"].">".$result[$i]["hw_name"]."</option>";
		}
		echo $optHTML;
	}
}
?>