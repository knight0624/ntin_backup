<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Handle_404 extends MY_Authentication {

	function index() {
		$data["title"] = "錯誤頁面";
		$this->load->view("404" , $data);
	}

}
?>