<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch0 extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model("ch0_model","",TRUE);
		$this->load->model("common_model","",TRUE);
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$this->load->library("form_validation");
			if (!isset($_SESSION["temp_chapter"]))
				$_SESSION["temp_chapter"] = "ch0";
			if (empty($_SESSION["insert_token"])) {
				$_SESSION["insert_token"] = sha1(time());
			}
			$this->write($_SESSION["hw_id"], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function write($hw_id = "", $team_id = "") {
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);

		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			// 學生有main_id
			if (!empty($_SESSION["temp_chapter"]))
				unset($_SESSION["temp_chapter"]);
			$data = $this->pageInit("家庭功能");
			$data["permission"] = 1;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["record"] = $this->ch0_model->getrecord($_SESSION["team_id"], $_SESSION["hw_id"]);
			// print_r($data["record"]);
			$this->load->view("ch0_view" , $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			//老師有main_id
			if (!empty($_SESSION["temp_chapter"]))
				unset($_SESSION["temp_chapter"]);
			$data = $this->pageInit("家庭功能");
			$data["permission"] = 2;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["record"] = $this->ch0_model->getrecord($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch0_view" , $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			// admin
			if (!empty($_SESSION["temp_chapter"]))
				unset($_SESSION["temp_chapter"]);
			$data = $this->pageInit("家庭功能");
			$data["permission"] = 3;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["record"] = $this->ch0_model->getrecord($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch0_view" , $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ) {
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ) {
			redirect("mark");
		}
		else {
			// other
			if (!empty($_SESSION["temp_chapter"]))
				unset($_SESSION["temp_chapter"]);
			$data = $this->pageInit("家庭功能");
			$data["permission"] = 6;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["record"] = $this->ch0_model->getrecord($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch0_view" , $data);
		}
	}

	public function editForeword(){
		$foreword_text = !empty($_POST["foreword_text"]) ? $this->security->xss_clean($_POST["foreword_text"]) : "";
		$this->ch0_model->editForeword_model($foreword_text, $_SESSION["team_id"],  $_SESSION["hw_id"]);
		echo true;
	}

	public function addComment() {
		$comment_value = !empty($_POST["comment_value"]) ? $this->security->xss_clean($_POST["comment_value"]) : "";
		$this->ch0_model->addComment_model($comment_value, $_SESSION["team_id"], $_SESSION["hw_id"]);
		echo true;
	}

	public function Addrecord(){
		$status = !empty($_POST["status"]) ? $this->security->xss_clean($_POST["status"]) : "";
		$date = !empty($_POST["date"]) ? $this->security->xss_clean($_POST["date"]) : "";
		$this->ch0_model->addrecord($status , $date);
		echo true;
		
	}

	public function editstatus(){
		$record_id = !empty($_POST["record_id"]) ? $this->security->xss_clean($_POST["record_id"]) : "";
		$status = !empty($_POST["status"]) ? $this->security->xss_clean($_POST["status"]) : "";
		$this->ch0_model->editstatus($record_id ,$status);
		echo true; 
	}
	public function deletestatus(){
		$record_id = !empty($_POST["record_id"]) ? $this->security->xss_clean($_POST["record_id"]) : "";
		$status = !empty($_POST["status"]) ? $this->security->xss_clean($_POST["status"]) : "";
		$this->ch0_model->deletestatus($record_id);
		echo true;
	}

	/**
	 * [驗證表單並新增使用者基本資料]
	 * @return [type] [description]
	 */
	function insertRespondent(){
		$this->load->library("form_validation");
		$this->form_validation->set_rules("account", "帳號", "trim|required",
			array("required" => "%s不能為空")
		);
		$this->form_validation->set_rules("user_name", "姓名", "trim|required",
			array("required" => "%s不能為空")
		);
		if($this->form_validation->run() == FALSE){
			$this->insert();
		} else {
			if(!empty($_POST["token"]) && $_POST["token"] === $_SESSION["insert_token"]){
				if($this->ch0_model->checkAc_model($this->security->xss_clean($_POST))){
					$entryclass = $this->ch0_model->getEntryClass()->row();
					$entry = $entryclass->entry;
					$class = $entryclass->class;
					$account = $this->ch0_model->insertRespondent_model($this->security->xss_clean($_POST), $entry, $class)->row()->account;
					$_SESSION["insert_token"] = sha1($_SESSION["insert_token"]);
					header("Location: " . base_url() . "ch0");
				} else {
					echo "帳號重複存在";
					header("Refresh: 3; url=" . base_url() . "ch0");
				}
				unset($_POST);
			} else {
				echo "請勿重複儲存";
				header("Refresh: 3; url=" . base_url() . "ch0");
			}
		}
	}
}