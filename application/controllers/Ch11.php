<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch11 extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model("ch11_model","",TRUE);
		$this->load->model("common_model","",TRUE);
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$this->write($_SESSION["hw_id"], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function write($hw_id = "", $team_id = ""){
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);

		$care_text ="";
		$comment = "";
		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			$permission = 1;
			$data = $this->pageInit("護理問題與計畫");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$Care_data = $this->ch11_model->getCare_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$care_problem = $Care_data["care_problem"];
			$care_plan = $Care_data["care_plan"];
			$care_supplement = $Care_data["care_supplement"];
			$comment = $Care_data["comment"];

			$data["permission"] =$permission;
			$data["care_problem"] = $care_problem;
			$data["care_plan"] = $care_plan;
			$data["care_supplement"] =$care_supplement;
			$data["comment"] = $comment;
			$this->load->view("ch11_view" , $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			$permission = '2';
			$data = $this->pageInit("護理問題與計畫");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$Care_data = $this->ch11_model->getCare_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$care_problem = $Care_data["care_problem"];
			$care_plan = $Care_data["care_plan"];
			$care_supplement = $Care_data["care_supplement"];
			$comment = $Care_data["comment"];

			$data["permission"] =$permission;
			$data["care_problem"] = $care_problem;
			$data["care_plan"] = $care_plan;
			$data["care_supplement"] =$care_supplement;
			$data["comment"] = $comment;
			$this->load->view("ch11_view" , $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			$permission = '3';
			$data = $this->pageInit("護理問題與計畫");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$Care_data = $this->ch11_model->getCare_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$care_problem = $Care_data["care_problem"];
			$care_plan = $Care_data["care_plan"];
			$care_supplement = $Care_data["care_supplement"];
			$comment = $Care_data["comment"];

			$data["permission"] =$permission;
			$data["care_problem"] = $care_problem;
			$data["care_plan"] = $care_plan;
			$data["care_supplement"] =$care_supplement;
			$data["comment"] = $comment;
			$this->load->view("ch11_view" , $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ){
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ){
			redirect("mark");
		}
		else{
			$permission = '6';
			$data = $this->pageInit("護理問題與計畫");
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$Care_data = $this->ch11_model->getCare_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$care_problem = $Care_data["care_problem"];
			$care_plan = $Care_data["care_plan"];
			$care_supplement = $Care_data["care_supplement"];
			$comment = $Care_data["comment"];

			$data["permission"] =$permission;
			$data["care_problem"] = $care_problem;
			$data["care_plan"] = $care_plan;
			$data["care_supplement"] =$care_supplement;
			$data["comment"] = $comment;
			$this->load->view("ch11_view" , $data);
		}
	}



	public function editCare() {
		// print_r($_POST);
		// $care_text = $this->security->xss_clean($_POST);
		$this->ch11_model->editCare_model($this->security->xss_clean($_POST), $_SESSION["team_id"],  $_SESSION["hw_id"]);
		echo true;
	}

	/**
	 * 老師評語
	 */
	public function addComment() {
		$comment_value = !empty($_POST["comment_value"]) ? $this->security->xss_clean($_POST["comment_value"]) : "";
		$this->ch11_model->addComment_model($comment_value, $_SESSION["team_id"], $_SESSION["hw_id"]);
		echo true;
	}
	
}
