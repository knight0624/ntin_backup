<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch3 extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model("ch3_model","",TRUE);
		$this->load->model("common_model","",TRUE);
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$this->load->library("form_validation");
			$this->write($_SESSION["hw_id"], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function write($hw_id = "", $team_id = ""){
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);

		$ch3_comment ="";
		$ch3_text = "";
		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			$data = $this->pageInit("個人資料簡介");
			$data["permission"] = 1;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["case_data"] = $this->ch3_model->getCaseProfile_model($_SESSION["team_id"],$_SESSION["hw_id"]);

			$this->load->view("ch3_view" , $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			$data = $this->pageInit("個人資料簡介");
			$data["permission"] = 2;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["case_data"] = $this->ch3_model->getCaseProfile_model($_SESSION["team_id"],$_SESSION["hw_id"]);

			$this->load->view("ch3_view" , $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			$data = $this->pageInit("個人資料簡介");
			$data["permission"] = 3;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["case_data"] = $this->ch3_model->getCaseProfile_model($_SESSION["team_id"],$_SESSION["hw_id"]);

			$this->load->view("ch3_view" , $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ){
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ){
			redirect("mark");
		}
		else{
			$data = $this->pageInit("個人資料簡介");
			$data["permission"] = 6;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["case_data"] = $this->ch3_model->getCaseProfile_model($_SESSION["team_id"],$_SESSION["hw_id"]);

			$this->load->view("ch3_view" , $data);
		}
	}

	function editCaseProfile(){
		$data = $this->security->xss_clean($_POST);
		$case_name = $this->security->xss_clean($_POST["case_name"]);
		$case_sex = $this->security->xss_clean($_POST["case_sex"]);
		$case_age = $this->security->xss_clean($_POST["case_age"]);
		$case_height = $this->security->xss_clean($_POST["case_height"]);
		$case_weight = $this->security->xss_clean($_POST["case_weight"]);
		$case_occpational = $this->security->xss_clean($_POST["case_occpational"]);
		$case_education = $this->security->xss_clean($_POST["case_education"]);
		$case_marital_status = $this->security->xss_clean($_POST["case_marital_status"]);
		$case_religious_faith = $this->security->xss_clean($_POST["case_religious_faith"]);
		$case_medication_history = $this->security->xss_clean($_POST["case_medication_history"]);
		$case_exercise_habits = $this->security->xss_clean($_POST["case_exercise_habits"]);
		$case_diet = $this->security->xss_clean($_POST["case_diet"]);
		$case_family_madical_history = $this->security->xss_clean($_POST["case_family_madical_history"]);
		$case_other = $this->security->xss_clean($_POST["case_other"]);

		$this->ch3_model->editCaseProfile_model($_SESSION["team_id"], $_SESSION["hw_id"], $case_name, $case_sex, $case_age,
												$case_height, $case_weight, $case_occpational,
												$case_education, $case_marital_status, $case_religious_faith,
												$case_medication_history, $case_exercise_habits, $case_diet,
												$case_family_madical_history, $case_other);
	}

	public function addComment() {
		$comment = !empty($_POST["comment"]) ? $this->security->xss_clean($_POST["comment"]) : "";
		$this->ch3_model->addComment_model($comment, $_SESSION["team_id"], $_SESSION["hw_id"]);
		echo true;
	}

	public function editOthers($edit_type) {
		$edit_data = $this->input->post();
		$this->ch3_model->editOthers_model($edit_data, $edit_type, $_SESSION["team_id"], $_SESSION["hw_id"]);
	}
}