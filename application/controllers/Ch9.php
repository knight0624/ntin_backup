<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch9 extends MY_Authentication {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('ch9_model','',TRUE);
		$this->load->model("common_model","",TRUE);
	}

	public function index() 
	{
		if ($this->getLoginStatus() === 1){
			$this->write($_SESSION['hw_id'], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	/**
	 * 權限判斷
	 * @param  string $hw_id   [description]
	 * @param  string $team_id [description]
	 */
	public function write($hw_id = "", $team_id = "") 
	{
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);

		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			// 學生有main_id
			$data = $this->pageInit("家庭壓力");
			$data["permission"] = 1;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch9_model->getFamilyStress_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch9_view" , $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			// 老師有main_id
			$data = $this->pageInit("家庭壓力");
			$data["permission"] = 2;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch9_model->getFamilyStress_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch9_view" , $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			// admin
			$data = $this->pageInit("家庭壓力");
			$data["permission"] = 3;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch9_model->getFamilyStress_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch9_view" , $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ) {
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ) {
			redirect("mark");
		}
		/*6*/
		else {
			// other
			$data = $this->pageInit("家庭壓力");
			$data["permission"] = 6;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch9_model->getFamilyStress_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch9_view" , $data);
		}
	}

	/**
	 * 生活改變事件評值表
	 */
	public function LCU()
	{
		$check = json_decode($this->input->post('LCU_value'));
		$LCU_date = implode("," ,  $check);
		$insert_status = $this->ch9_model->editLCU_model($_SESSION["team_id"], $_SESSION["hw_id"], $LCU_date); //寫入
		echo $insert_status;
	}

	/**
	 * SELECT_LCU
	 */
	public function Select_LCU_status()
	{
		$SELECT_LCU = 'fstress_lcu';
		$insert_status = $this->ch9_model->getSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], $SELECT_LCU); //找LCU的值
		$LCU_value = json_encode(explode(",", $insert_status['fstress_lcu']));
		print_r($LCU_value);
	}

	/**
	 * 補充
	 */
	public function Save_Supplement()
	{
		$supplement_value = $this->input->post('supplement');
		!empty($comment) ? $this->security->xss_clean($supplement) : "";
		$insert_status = $this->ch9_model->editSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'fs_supplement', $supplement_value); //寫入
		echo $insert_status;
	}
	
	/**
	 * 老師評語
	 */
	public function addComment()
	{
		$comment = $this->input->post('comment_value');
		!empty($comment) ? $this->security->xss_clean($comment) : "";
		$insert_status = $this->ch9_model->editSomething_model($_SESSION["team_id"], $_SESSION["hw_id"], 'comment', $comment); //寫入
		echo $insert_status;
	}
}
