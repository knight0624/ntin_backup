<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch1 extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model("ch1_model","",TRUE);
		$this->load->model("common_model","",TRUE);
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$this->write($_SESSION["hw_id"], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function write($hw_id = "", $team_id = ""){
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);

		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			// 學生有main_id
			$data = $this->pageInit("前言");
			$data["permission"] = 1;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$Foreword_data = $this->ch1_model->getForeword_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			// 評語 作業的值
			$data["foreword_text"] = $Foreword_data["foreword_text"];
			$data["comment"] = $Foreword_data["comment"];
			$data["respond"] = $Foreword_data["respond"];

			$this->load->view("ch1_view" , $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			//老師有main_id
			$data = $this->pageInit("前言");
			$data["permission"] = 2;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$Foreword_data = $this->ch1_model->getForeword_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			// 評語 作業的值
			$data["foreword_text"] = $Foreword_data["foreword_text"];
			$data["comment"] = $Foreword_data["comment"];
			$data["respond"] = $Foreword_data["respond"];

			$this->load->view("ch1_view" , $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			// admin
			$data = $this->pageInit("前言");
			$data["permission"] = 3;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$Foreword_data = $this->ch1_model->getForeword_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			// 評語 作業的值
			$data["foreword_text"] = $Foreword_data["foreword_text"];
			$data["comment"] = $Foreword_data["comment"];
			$data["respond"] = $Foreword_data["respond"];

			$this->load->view("ch1_view" , $data);
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "3") ){
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ){
			redirect("mark");
		}
		/*7*/
		else if( (empty($_SESSION["main_id"])) &&  ($_SESSION['permission'] === "4") && ($hw_end_time > time()) === true ){
			//老師有main_id
			$data = $this->pageInit("前言");
			$data["permission"] = 7;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$Foreword_data = $this->ch1_model->getForeword_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			// 評語 作業的值
			$data["foreword_text"] = $Foreword_data["foreword_text"];
			$data["comment"] = $Foreword_data["comment"];
			$data["respond"] = $Foreword_data["respond"];

			$this->load->view("ch1_view" , $data);
		}
		else{
			// other
			$data = $this->pageInit("前言");
			$data["permission"] = 6;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$Foreword_data = $this->ch1_model->getForeword_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			// 評語 作業的值
			$data["foreword_text"] = $Foreword_data["foreword_text"];
			$data["comment"] = $Foreword_data["comment"];
			$data["respond"] = $Foreword_data["respond"];

			$this->load->view("ch1_view" , $data);
		}
	}

	public function editForeword(){
		$foreword_text = !empty($_POST["foreword_text"]) ? $this->security->xss_clean($_POST["foreword_text"]) : "";
		$this->ch1_model->editForeword_model($foreword_text, $_SESSION["team_id"],  $_SESSION["hw_id"]);
		echo true;
	}

	public function addComment() {
		$comment_value = !empty($_POST["comment_value"]) ? $this->security->xss_clean($_POST["comment_value"]) : "";
		$this->ch1_model->addComment_model($comment_value, $_SESSION["team_id"], $_SESSION["hw_id"]);
		echo true;
	}
	public function addRespond() {
		$respond_value = !empty($_POST["respond_value"]) ? $this->security->xss_clean($_POST["respond_value"]) : "";
		$this->ch1_model->addRespond_model($respond_value, $_SESSION["team_id"], $_SESSION["hw_id"]);
		echo true;
	}
}