<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Login extends MY_Authentication {

	function index() {
		if( isset($_SESSION["username"]) && isset($_SESSION["password"]) ) {
			$this->load->model("common_model","",TRUE);
			$_SESSION["permission"] = $this->common_model->getLoginPermission($_SESSION["username"])->row()->permission_id;
			if($_SESSION["permission"] === "4" ){
				$_SESSION["team_id"] = $this->common_model->getLoginPermission($_SESSION["username"])->row()->team_id;
				$_SESSION["hw_id"] = $this->common_model->getLoginPermission($_SESSION["username"])->row()->hw_id;
				header("Location: " . base_url() . "ch1");
			}
			else{
				header("Location: " . base_url() . "home");
			}
		} else if( isset($_POST["username"]) && isset($_POST["password"]) ) {
			$_SESSION["username"] = $this->security->xss_clean($_POST["username"]);
			$_SESSION["password"] = $this->security->xss_clean($_POST["password"]);

			$this->load->model("common_model","",TRUE);
			$_SESSION["permission"] = $this->common_model->getLoginPermission($_SESSION["username"])->row()->permission_id;
			if($_SESSION["permission"] === "4" ){
				$_SESSION["team_id"] = $this->common_model->getLoginPermission($_SESSION["username"])->row()->team_id;
				$_SESSION["hw_id"] = $this->common_model->getLoginPermission($_SESSION["username"])->row()->hw_id;
				header("Location: " . base_url() . "ch1");
			}
			else{
				header("Location: " . base_url() . "home");
			}
		} else {
			$data["title"] = "系統登入";
			$this->load->view("login_view" , $data);
		}
	}

}
?>