<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ch5 extends MY_Authentication {

	public function __construct() {
		parent::__construct();
		$this->load->model("ch5_model","",TRUE);
		$this->load->helper("form");
		$this->load->model("common_model","",TRUE);
	}

	public function index() {
		if ($this->getLoginStatus() === 1){
			$this->write($_SESSION["hw_id"], $_SESSION["team_id"]);
		} elseif ($this->getLoginStatus() == 2) {
			//跳轉到無權限頁面
			header("Location: " . base_url() . "error");
		} else {
			$this->logout();
		}
	}

	public function write($hw_id = "", $team_id = "") {
		$_SESSION["hw_id"] = $this->security->xss_clean($hw_id);
		$_SESSION["team_id"] = $this->security->xss_clean($team_id);
		$result = $this->common_model->getMain($_SESSION["hw_id"], $_SESSION["team_id"])->row();
		$_SESSION["main_id"] = $result->main_id;
		$hw_end_time = strtotime($result->hw_end_time);
		/*1*/
		if ( (!empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") && ($hw_end_time > time()) === true ) {
			// 學生有main_id
			$data = $this->pageInit("家庭結構");
			$data["permission"] = 1;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch5_model->getTeam_All_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$data["upload"] = $this->ch5_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch5_view", $data);
		}
		/*2*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "2") && ($hw_end_time > time()) === true ) {
			//老師有main_id
			$data = $this->pageInit("家庭結構");
			$data["permission"] = 2;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch5_model->getTeam_All_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$data["upload"] = $this->ch5_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch5_view", $data);
		}
		/*3*/
		else if ( (!empty($_SESSION["main_id"])) && ($_SESSION['permission'] === "1") && ($hw_end_time > time()) === true ){
			// admin
			$data = $this->pageInit("家庭結構");
			$data["permission"] = 3;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch5_model->getTeam_All_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$data["upload"] = $this->ch5_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch5_view", $data);							
		}
		/*4*/
		else if ( (empty($_SESSION["main_id"])) && ($_SESSION["permission"] === "3") ){
			redirect("home");
		}
		/*5*/
		else if ( (empty($_SESSION["main_id"])) && (($_SESSION['permission'] === "2") || ($_SESSION['permission'] === "1")) ){
			redirect("mark");
		}
		else {
			// other
			$data = $this->pageInit("家庭結構");
			$data["permission"] = 6;
			$data["hw_name"] = $this->common_model->getHwName($_SESSION["hw_id"])->row()->hw_name;
			$data["DB_all"] = $this->ch5_model->getTeam_All_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$data["upload"] = $this->ch5_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			$this->load->view("ch5_view", $data);			
		}			
	}
	/**
	 * [溝通]
	 * @return [type]
	 */
	public function fstructureRoleStructure() {
		$fstructure_role_structure = $this->input->post("role_structure_value");
		! empty($fstructure_role_structure) ? $this->security->xss_clean($fstructure_role_structure) : "";
		$this->ch5_model->addRoleStructure_model($_SESSION["team_id"], $_SESSION["hw_id"], $fstructure_role_structure);
		echo ture;
	}

	/**
	 * [權力結構]
	 * @return [type]
	 */
	public function fstructurePowerStructure() {
		$fstructure_power_structure = $this->input->post("power_structure_value");
		! empty($fstructure_power_structure) ? $this->security->xss_clean($fstructure_power_structure) : "";
		$this->ch5_model->addPowerStructure_model($_SESSION["team_id"], $_SESSION["hw_id"], $fstructure_power_structure);
		echo ture;
	}

	/**
	 * [角色結構]
	 * @return [type]
	 */
	public function fstructureCommunication() {
		$fstructure_communication = $this->input->post("communication_value");
		! empty($fstructure_communication) ? $this->security->xss_clean($fstructure_communication) : "";
		$this->ch5_model->addCommunication_model($_SESSION["team_id"], $_SESSION["hw_id"], $fstructure_communication);
		echo ture;
	}

	/**
	 * [價值觀]
	 * @return [type]
	 */
	public function fstructureValues() {
		$fstructure_values = $this->input->post("values_value");
		! empty($fstructure_values) ? $this->security->xss_clean($fstructure_values) : "";
		$this->ch5_model->addValues_model($_SESSION["team_id"], $_SESSION["hw_id"], $fstructure_values);
		echo ture;
	}

	/**
	 * [補充]
	 * @return [type]
	 */
	public function fstructureSuppleMent() {
		$fstructure_supple_ment = $this->input->post("supple_ment_value");
		! empty($fstructure_supple_ment) ? $this->security->xss_clean($fstructure_supple_ment) : "";
		$this->ch5_model->addSuppleMent_model($_SESSION["team_id"], $_SESSION["hw_id"], $fstructure_supple_ment);
		echo ture;
	}

	/**
	 * [上傳PPT檔案]
	 * @return [type] [description]
	 */
	public function do_upload_ppt() {
		$filename = ["溝通過程", "權力連續線"];

		$newfilename = $filename[$_POST["number"]-1]."PPT_".$_SESSION["main_id"]."_".date("Ymdhi");

		$config["upload_path"] = "./uploads/ppt/";
		$config["allowed_types"] = "ppt|pptx";
		$config["max_size"] = 8192;
		$config["file_name"] = $newfilename;
		$config["overwrite"] = TRUE;
		$this->load->library("upload", $config);
		if ( ! $this->upload->do_upload("userfile")) {
			$error = array(
					"error" => $this->upload->display_errors(),
					"title" => "失敗"
					);
			$this->load->view("upload_error_view", $error);
		}
		else {
			$file_name = $this->upload->data("file_name");
			$old_name = $this->ch5_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			if ($file_name != $old_name["ppt".$_POST["number"]."_name"]) {
				if ( ! empty($old_name["ppt".$_POST["number"]."_name"]) && is_readable("uploads/ppt/".$old_name["ppt".$_POST["number"]."_name"])) {
					unlink("uploads/ppt/".$old_name["ppt".$_POST["number"]."_name"]);
				}					
			}
			$this->ch5_model->addPttName_model($_SESSION["team_id"], $_SESSION["hw_id"], $file_name, $_POST["number"]);
			redirect("Ch5","refresh");
			$this->load->view("ch5_view", $data);
		}
	}

	/**
	 * [上傳圖片檔案]
	 * @return [type] [description]
	 */
	public function do_upload_img() {
		$filename = ["溝通過程", "權力連續線"];

		$newfilename = $filename[$_POST["number"]-1]."圖片_".$_SESSION["main_id"]."_".date("Ymdhi");

		$config["upload_path"] = "./uploads/img/";
		$config["allowed_types"] = "gif|jpg|png|jpeg|bmp";
		$config["max_size"] = 8192;
		$config["file_name"] = $newfilename;
		$config["overwrite"] = TRUE;
		$this->load->library("upload", $config);
		if ( ! $this->upload->do_upload("userfile")) {
			$error = array(
					"error" => $this->upload->display_errors(),
					"title" => "失敗"
					);
			$this->load->view("upload_error_view", $error);
		}   
		else {
			$file_name = $this->upload->data("file_name");
			$old_name = $this->ch5_model->uploadName_model($_SESSION["team_id"], $_SESSION["hw_id"]);
			if ($file_name != $old_name["img".$_POST["number"]."_name"]) {
				if ( ! empty($old_name["img".$_POST["number"]."_name"]) && is_readable("uploads/img/".$old_name["img".$_POST["number"]."_name"])) {
					unlink("uploads/img/".$old_name["img".$_POST["number"]."_name"]);
				}				
			}
			$this->ch5_model->addImgName_model($_SESSION["team_id"], $_SESSION["hw_id"], $file_name, $_POST["number"]);
			redirect("Ch5","refresh");
			$this->load->view("ch5_view", $data);
			
		}
	}

	/**
	 * [刪除PPT檔案]
	 * @return [type]
	 */
	public function delete_Ptt() {
		$file_name = $this->input->post("uploadName");
		$delete = "";
		$filenumber = "ppt".$_POST["filenumber"]."_name";
		if (is_readable("uploads/ppt/".$file_name)) {
			$this->ch5_model->deleteFile_model($_SESSION["team_id"], $_SESSION["hw_id"], $delete, $filenumber);
			unlink("uploads/ppt/".$file_name);
			echo true;
		}else {
			echo false;
		}
	}

	/**
	 * [刪除圖片檔案]
	 * @return [type]
	 */
	public function delete_Img() {
		$file_name = $this->input->post("uploadName");
		$delete = "";
		$filenumber = "img".$_POST["filenumber"]."_name";
		if (is_readable("uploads/img/".$file_name)) {
			$this->ch5_model->deleteFile_model($_SESSION["team_id"], $_SESSION["hw_id"], $delete, $filenumber);
			unlink("uploads/img/".$file_name);
			echo true;
		}else {
			echo false;
		}
	}

	/**
	 * [老師評語]
	 */
	public function addComment() {
		$comment = $this->input->post("comment_value");
		! empty($comment) ? $this->security->xss_clean($comment) : "";
		$this->ch5_model->addComment_model($_SESSION["team_id"], $_SESSION["hw_id"], $comment);
		echo true;
	}
}