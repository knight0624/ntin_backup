<?php
/**
 *位置：家庭功能(family_function)
 */
class ch8_model extends CI_Model{

	/**
	 * 查詢這隊伍所有 family_function 資料
	 * @param  [type] $team_id  [description]
	 * @param  [type] $hw_id    [description]
	 * @return [type] $data_all [description]
	 */
	function getFamilyFunction_model($team_id, $hw_id)
	{
		$this->db->select();
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$data_all = $this->db->get('family_function')->row_array();
		return $data_all;
	}

	/**
	 * 查詢特定資料
	 * @param  [type] $team_id        [description]
	 * @param  [type] $hw_id          [description]
	 * @param  [type] $something      [description]
	 * @return [type] $data_something [description]
	 */
	function getSomething_model ($team_id, $hw_id, $something)
	{
		$this->db->select($something);
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$data_something = $this->db->get('family_function')->row_array();
		return $data_something;
	}

	/**
	 * 寫入特定資料
	 * @param  [type] $team_id   [description]
	 * @param  [type] $hw_id     [description]
	 * @param  [type] $field     [description]
	 * @param  [type] $something [description]
	 */
	function editSomething_model($team_id, $hw_id, $field, $something)
	{
		$this->db->select('team_id','hw_id');
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$check_id = $this->db->get('family_function')->row_array();
		if (empty($check_id['team_id']) && empty($check_id['hw_id'])) { //如果資料庫沒有這組的資料
			//沒有資料就做新增
			$this->db->set('team_id' , $team_id);
			$this->db->set('hw_id' , $hw_id);
			$this->db->set($field , $something);
			$this->db->insert('family_function');
			return ture;
		} else {
			//有資料就做修改
			$this->db->set($field , $something);
			$this->db->where('team_id' , $team_id);
			$this->db->where('hw_id' , $hw_id);
			$this->db->update('family_function');
			return ture; 
		}
	}

	/**
	 * 資料寫入:APGAR
	 * @param  [type] $team_id    [description]
	 * @param  [type] $hw_id      [description]
	 * @param  [type] $array_data [description]
	 */
	function editAPGAR_model($team_id, $hw_id, $array_data)
	{
		$this->db->select('team_id','hw_id');
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$check_id = $this->db->get('family_function')->row_array();
		if (empty($check_id['team_id']) && empty($check_id['hw_id'])) { //如果資料庫沒有這組的資料
			//沒有資料就做新增
			$this->db->set('team_id' , $team_id);
			$this->db->set('hw_id' , $hw_id);
			$this->db->set('ff_APGAR' , $array_data);
			$this->db->insert('family_function'); 
			return ture;
		} else {
			//有資料就做修改
			$this->db->set('ff_APGAR' , $array_data);
			$this->db->where('team_id' , $team_id);
			$this->db->where('hw_id' , $hw_id);
			$this->db->update('family_function');
			return ture; 
		}
	}

	/**
	 * 資料寫入:PRACTICE
	 * @param  [type] $team_id    [description]
	 * @param  [type] $hw_id      [description]
	 * @param  [type] $array_data [description]
	 */
	function editPRACTICE_model ($team_id, $hw_id, $array_data)
	{
		$this->db->select('team_id','hw_id');
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$check_id = $this->db->get('family_function')->row_array();
		if (empty($check_id['team_id']) && empty($check_id['hw_id'])) { //資料庫有沒有這組的資料
			//沒有資料就做新增
			$data = array(
				'team_id'           => $team_id, 
				'hw_id'             => $hw_id ,
				'ff_absence'        => $array_data['absence'], 
				'ff_attend'         => $array_data['attend'], 
				'ff_problem'        => $array_data['problem'], 
				'ff_roles'          => $array_data['roles'], 
				'ff_affect'         => $array_data['affect'], 
				'ff_communication'  => $array_data['communication'], 
				'ff_time'           => $array_data['time'], 
				'ff_illness'        => $array_data['illness'], 
				'ff_adaptability'   => $array_data['adaptability'], 
				'ff_ecology'        => $array_data['ecology']
			);
			$this->db->set($data);
			$this->db->insert('family_function');
			return ture;
		} else {
			//有資料就做修改
			$data = array(
				'ff_absence'        => $array_data['absence'], 
				'ff_attend'         => $array_data['attend'], 
				'ff_problem'        => $array_data['problem'], 
				'ff_roles'          => $array_data['roles'], 
				'ff_affect'         => $array_data['affect'], 
				'ff_communication'  => $array_data['communication'], 
				'ff_time'           => $array_data['time'], 
				'ff_illness'        => $array_data['illness'], 
				'ff_adaptability'   => $array_data['adaptability'], 
				'ff_ecology'        => $array_data['ecology']
			);
			$this->db->set($data);
			$this->db->where('team_id' , $team_id);
			$this->db->where('hw_id' , $hw_id);
			$this->db->update('family_function'); 
			return ture;
		}
	}

	/**
	 * 資料寫入:family_functioning
	 * @param  [type] $team_id    [description]
	 * @param  [type] $hw_id      [description]
	 * @param  [type] $array_data [description]
	 */
	function editFamily_Functioning_model ($team_id, $hw_id, $array_data)
	{
		$this->db->select('team_id','hw_id');
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$check_id = $this->db->get('family_function')->row_array();
		if (empty($check_id['team_id']) && empty($check_id['hw_id'])) { //如果資料庫沒有這組的資料
			//沒有資料就做新增
			$data = array(
				'team_id'          => $team_id ,
				'hw_id'            => $hw_id ,
				'ff_reproduction'  => $array_data['reproduction'], 
				'ff_economic'      => $array_data['economic'], 
				'ff_sexual'        => $array_data['sexual'], 
				'ff_socialisation' => $array_data['socialisation'], 
				'ff_health'        => $array_data['health']
			);
			$this->db->set($data);
			$this->db->insert('family_function'); 
			return ture;
		} else {
			//有資料就做修改
			$data = array(
				'ff_reproduction'  => $array_data['reproduction'], 
				'ff_economic'      => $array_data['economic'], 
				'ff_sexual'        => $array_data['sexual'], 
				'ff_socialisation' => $array_data['socialisation'], 
				'ff_health'        => $array_data['health']
			);
			$this->db->set($data);
			$this->db->where('team_id' , $team_id);
			$this->db->where('hw_id' , $hw_id);
			$this->db->update('family_function'); 
			return ture; 
		}
	}
}