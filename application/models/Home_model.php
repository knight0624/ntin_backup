<?php

class Home_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function getHomeworkDetial_model($hw_id){
		$this->db->select("homework.hw_name , homework.hw_end_time , homework.create_time");
		$this->db->from("homework");
		$this->db->where("homework.course_id" , $_SESSION["course_id"]);
		$this->db->where("SHA1(CONCAT('ntin_',homework.hw_id))" , $hw_id);

		return $this->db->get();
	}

	function setHomework_model($data){
		$this->db->set("course_id" , $_SESSION["course_id"]);
		$this->db->set("hw_name" , $data["hw_name"]);
		$this->db->set("hw_end_time" , $data["hw_end_time"]);
		$this->db->set("create_time" , "NOW()" , FALSE);
		$this->db->insert("homework");
	}

	function updateHomework_model($data){
		$this->db->set("hw_name" , $data["hw_name"]);
		$this->db->set("hw_end_time" , $data["hw_end_time"]);
		$this->db->where("homework.course_id" , $_SESSION["course_id"]);
		$this->db->where("SHA1(CONCAT('ntin_',homework.hw_id))" , $data["hw_id"]);
		$this->db->update("homework");
	}
}

?>