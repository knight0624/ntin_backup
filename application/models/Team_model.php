<?php

class Team_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function getCourseOpt_model($type){
		$this->db->select("SHA1(CONCAT('ntin_',course.course_id)) as course_id , academic_year , course.course_name , user.name ,
			CONCAT(academic_year , '學年-',user.name , '老師-' , course.course_name) as item_name");
		$this->db->from("course");
		$this->db->join("user" , "user.account = course.instructor");
		if($type == "2"){
			$this->db->where("course.instructor" , $_SESSION["username"]);
		}
		return $this->db->get();
	}

	function getCourseInfo_model($course_id){
		$this->db->select("course.course_id , course.course_name");
		$this->db->from("course");
		$this->db->where("SHA1(CONCAT('ntin_',course_id))" , $course_id);

		return $this->db->get();
	}

	function getNotHwStudent_model($year , $class){
		$this->db->select("team_member.member");
		$this->db->from("team_member");
		$this->db->join("team", "team_member.team_id = team.team_id");
		$this->db->join("homework", "team.hw_id = SHA1(CONCAT('ntin_',homework.hw_id))");
		$this->db->where("homework.course_id", $_SESSION["course_id"]);
		$this->db->where("SHA1(CONCAT('ntin_',homework.hw_id))", $_SESSION["hw_id"]);
		$result = $this->db->get()->result_array();
		$temp_array = array();
		$i = 0 ;
		foreach ($result as $row){
			$temp_array[$i] = $row["member"];
			$i++;
		}
		$this->db->select("user.account , user.name , user.entry , user.class , course_student.course_id");
		$this->db->from("course_student");
		$this->db->join("user" , "user.account = course_student.account");
		$this->db->where("course_id" , $_SESSION["course_id"]);
		if(count($temp_array) > 0){
			$this->db->where_not_in("course_student.account" , $temp_array);
		}
		$this->db->where("user.permission_id" , 3);
		$this->db->where("user.status" , 1);

		if(!is_null($year) && $year != "all"){
			$this->db->where("user.entry" , $year);
		}
		if(!is_null($class) && $class != "all"){
			$this->db->where("user.class" , $class);
		}
		$this->db->order_by("account", "ASC");
		return $this->db->get();
	}

	function getHwStudent_model($team_id){
		$this->db->select("user.account , user.name , user.entry , user.class , course_student.course_id , team_member.team_id");
		$this->db->from("user");
		$this->db->join("(select * from course_student where course_student.course_id = ".$_SESSION["course_id"].") as course_student" , "user.account = course_student.account");
		$this->db->join("team_member" , "team_member.member = user.account" , "LEFT");
		$this->db->where("team_member.team_id is not null");
		$this->db->where("team_member.team_id" , $team_id);
		$this->db->where("user.permission_id" , 3);
		$this->db->where("user.status" , 1);
		$this->db->order_by("account", "ASC");
		return $this->db->get();
	}

	function getTeamOpt_model(){
		$this->db->select("team_id , team.hw_id , team.name");
		$this->db->from("homework");
		$this->db->where("SHA1(CONCAT('ntin_',homework.hw_id))=" ,$_SESSION["hw_id"]);
		$this->db->join("team" , "team.hw_id = SHA1(CONCAT('ntin_',homework.hw_id))");
		return $this->db->get();

	}

	function setTeaminfo_model($data , $temp_name){
		$this->db->set("hw_id" , $_SESSION["hw_id"]);
		$this->db->set("name" , $temp_name);
		$this->db->insert("team");

		$id = $this->db->insert_id();
		$temp = array();
		$temp_history = array();
		for ($i=0; $i < count($data); $i++) {
			$temp[$i] = array("team_id"=>$id , "member"=>$data[$i]);
			$temp_history[$i] = array("account"=>$data[$i], "team_id"=>$id, "hw_id"=>$_SESSION["hw_id"]);
		}
		$this->db->insert_batch("team_member", $temp);
		$this->db->insert_batch("user_history", $temp_history);

		$this->db->set("hw_id" , $_SESSION["hw_id"]);
		$this->db->set("team_id" , $id);
		$this->db->insert("main");
	}

	function deleteTeaminfo_model($u_acc , $i_acc , $team , $team_name){
		if(count($u_acc)){
			$this->db->where_in("team_member.member" , $u_acc);
			$this->db->where("team_member.team_id" , $team);
			$this->db->delete("team_member");

			$this->db->where_in("user_history.account" , $u_acc);
			$this->db->where("user_history.team_id" , $team);
			$this->db->delete("user_history");
		}
		if(!empty($team_name)){
			$this->db->set("name" , $team_name);
			$this->db->where("team_id" , $team);
			$this->db->update("team");
		}
		if(count($i_acc)){
			$temp = array();
			$temp_history = array();
			for ($i=0; $i < count($i_acc); $i++) {
				$temp[$i] = array("team_id"=>$team , "member"=>$i_acc[$i]);
				$temp_history[$i] = array("account"=>$i_acc[$i], "team_id"=>$team, "hw_id"=>$_SESSION["hw_id"]);
			}
			$this->db->insert_batch("team_member", $temp);
			$this->db->insert_batch("user_history", $temp_history);
		}
	}

	function getHw_model($course_id){
		$this->db->select("SHA1(CONCAT('ntin_',hw_id)) as hw_id , hw_name");
		$this->db->from("homework");
		$this->db->where("SHA1(CONCAT('ntin_',homework.course_id))" ,$course_id);
		return $this->db->get();
	}
}

?>