<?php
/**
 *用途: 驗證帳密、權限，取得權限、帳戶等資訊 
 */
class ch3_model extends CI_Model{

	function editCaseProfile_model($team_id, $hw_id, $case_name, $case_sex, $case_age, $case_height, $case_weight, $case_occpational,
		$case_education, $case_marital_status, $case_religious_faith,
		$case_medication_history, $case_exercise_habits, $case_diet,
		$case_family_madical_history, $case_other){

		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->delete("case_profile");

		$this->db->set("team_id", $team_id);
		$this->db->set("hw_id", $hw_id);
		$this->db->set("case_name", $case_name);
		$this->db->set("case_sex", $case_sex);
		$this->db->set("case_age", $case_age);
		$this->db->set("case_height", $case_height);
		$this->db->set("case_weight", $case_weight);
		$this->db->set("case_occpational", $case_occpational);
		$this->db->set("case_education", $case_education);
		$this->db->set("case_marital_status", $case_marital_status);
		$this->db->set("case_religious_faith", $case_religious_faith);
		$this->db->set("case_medication_history", $case_medication_history);
		$this->db->set("case_exercise_habits", $case_exercise_habits);
		$this->db->set("case_diet", $case_diet);
		$this->db->set("case_family_madical_history", $case_family_madical_history);
		$this->db->set("case_other", $case_other);
		$this->db->insert("case_profile");

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function getCaseProfile_model($team_id,$hw_id){
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$profile = $this->db->get("case_profile")->row_array();
		return $profile;
	}

	function addComment_model($text, $team_id, $hw_id) {
		$this->db->select("COUNT('comment') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("case_profile");
		$count = $this->db->get()->row()->count;
		print_r($team_id);
		if ($count>0) {
			$this->db->set("comment", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("case_profile");
		}
		else{
			$this->db->set("comment", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("case_profile");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function editOthers_model($data, $edit_type, $team_id, $hw_id){
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$this->db->set($edit_type,$data[$edit_type]);
		$this->db->update('case_profile');
	}
}

?>