<?php
/**
 *用途: 驗證帳密、權限，取得權限、帳戶等資訊
 */
class ch4_model extends CI_Model{

	function addMember_model($data, $team_id, $hw_id){
		$memberData = array(
					'fm_relatives' => $data['fm_relatives'],
					'fm_name' => $data['fm_name'],
					'fm_age' => $data['fm_age'],
					'fm_formal_role' =>$data['fm_formal_role'],
					'fm_informal_role' => $data['fm_informal_role'],
					'fm_education' => $data['fm_education'],
					'fm_religious_faith' => $data['fm_religious_faith'],
					'fm_occpational' => $data['fm_occpational'],
					'fm_language' => $data['fm_language'],
					'fm_disease' => $data['fm_disease'],
					'team_id' => $team_id,
					'hw_id' => $hw_id
		);
		$this->db->insert("family_member", $memberData);

		$this->db->select("COUNT('fm_other') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("family_member_comment");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("fm_other" , $data["fm_other"]);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_member_comment");
		}
		else{
			$this->db->set("fm_other" , $data["fm_other"]);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("family_member_comment");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function listMember_model($team_id,$hw_id){
		$member = array();

		$this->db->where('team_id = ',$team_id);
		$this->db->where('hw_id',$hw_id);

		$member[] = $this->db->get('family_member')->num_rows();

		$this->db->where('team_id = ',$team_id);
		$this->db->where('hw_id',$hw_id);
		$this->db->select('fm_id, fm_relatives, fm_name, fm_age, fm_formal_role, fm_informal_role, fm_education, fm_religious_faith, fm_occpational, fm_language, fm_disease');
		$member[] = $this->db->get('family_member');

		return $member;
	}

	function editMember_model($data, $edit_type, $team_id, $hw_id){
		$this->db->where('fm_id',$data['fm_id']);
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$this->db->set($edit_type,$data['edit_data']);
		$this->db->update('family_member');
	}

	function deleteMember_model($data){

		$delData = array('fm_id' => $data['del']);

		$this->db->delete('family_member', array('fm_id' => $delData['fm_id']));
	}

	function deleteFile_img_model($data){

		$delData = array('file_name' => $data['del_file']);
		// if (is_readable("uploads/".$delData['file_name'])){
			unlink("uploads/img/".$delData['file_name']);
		// }
		$this->db->delete('upload_ch4', array('file_name' => $delData['file_name']));
	}

	function deleteFile_ppt_model($data){

		$delData = array('file_name' => $data['del_file']);
		// if (is_readable("uploads/".$delData['file_name'])){
			unlink("uploads/ppt/".$delData['file_name']);
		// }
		$this->db->delete('upload_ch4', array('file_name' => $delData['file_name']));
	}

	function addFm_other_model($data, $team_id, $hw_id){
		$this->db->select("COUNT('fm_other') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("family_member_comment");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("fm_other" , $data["fm_other"]);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_member_comment");
		}
		else{
			$this->db->set("fm_other" , $data["fm_other"]);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("family_member_comment");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function getFm_other_model($team_id,$hw_id){

		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$result = $this->db->get('family_member_comment')->row_array();
		return $result;
	}

	function setFile_path_model($data,$hw_id,$team_id,$file_type){
		$addData = array(
						'file_name' =>$data['file_name'],
						'hw_id' => $hw_id,
						'team_id' => $team_id,
						'file_type' =>$file_type
					);
		$check = "";
		$this->db->select();
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$this->db->where('file_type',$file_type);
		$check = $this->db->get('upload_ch4')->row_array();
		if(count($check)){
		// if(!empty($check['upload_id'])){
			// $this->db->where('upload_id',$check['upload_id']);
			$this->db->where('team_id',$team_id);
			$this->db->where('hw_id',$hw_id);
			$this->db->where('file_type',$file_type);
			$this->db->set('team_id',$team_id);
			$this->db->set('hw_id',$hw_id);
			$this->db->set('file_type',$file_type);
			$this->db->update('upload_ch4',$addData);
		}
		else{
			// $this->db->where('team_id = ',$team_id);
			$this->db->insert('upload_ch4',$addData);
		}
	
	}

	function getFile_name_model($hw_id,$team_id, $file_type){
		$this->db->where('hw_id',$hw_id);
		$this->db->where('team_id',$team_id);
		$this->db->where('file_type',$file_type);
		$result = $this->db->get('upload_ch4')->row_array();
		return $result['file_name'];
	}

	function getComment_model($team_id, $hw_id){
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		return $this->db->get('family_member_comment')->row_array();
	}

	function addComment_model($text, $team_id, $hw_id) {
		$this->db->select("COUNT('comment') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("family_member_comment");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("comment", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_member_comment");
		}
		else{
			$this->db->set("comment", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("family_member_comment");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}
}

?>