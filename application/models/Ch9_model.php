<?php
/**
 *位置：家庭壓力(family_stress)
 */
class ch9_model extends CI_Model{

	/**
	 * 查詢這隊伍所有 family_stress 資料
	 * @param  [type] $team_id  [description]
	 * @param  [type] $hw_id    [description]
	 * @return [type] $data_all [description]
	 */
	function getFamilyStress_model($team_id, $hw_id)
	{
		$this->db->select();
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$data_all = $this->db->get('family_stress')->row_array();
		return $data_all;
	}

	/**
	 * 查詢特定資料
	 * @param  [type] $team_id        [description]
	 * @param  [type] $hw_id          [description]
	 * @param  [type] $something      [description]
	 * @return [type] $data_something [description]
	 */
	function getSomething_model($team_id, $hw_id, $something)
	{
		$this->db->select($something);
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$data_something = $this->db->get('family_stress')->row_array();
		return $data_something;
	}

	/**
	 * 資料寫入:LCU
	 * @param  [type] $team_id    [description]
	 * @param  [type] $hw_id      [description]
	 * @param  [type] $array_data [description]
	 */
	function editLCU_model ($team_id, $hw_id, $array_data)
	{
		$this->db->select('team_id','hw_id');
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$check_id = $this->db->get('family_stress')->row_array();

		if (empty($check_id['team_id']) && empty($check_id['hw_id'])) { //如果資料庫沒有這組的資料
			//沒有資料就做新增
			$this->db->set('team_id',$team_id);
			$this->db->set('hw_id',$hw_id);
			$this->db->set('fstress_lcu',$array_data);
			$this->db->insert('family_stress'); 
			return ture;
		} else {
			//有資料就做修改
			$this->db->set('fstress_lcu', $array_data);
			$this->db->where('team_id', $team_id);
			$this->db->where('hw_id', $hw_id);
			$this->db->update('family_stress');
			return ture; 
		}
	}

	/**
	 * 寫入特定資料
	 * @param  [type] $team_id   [description]
	 * @param  [type] $hw_id     [description]
	 * @param  [type] $field     [description]
	 * @param  [type] $something [description]
	 */
	function editSomething_model($team_id, $hw_id, $field, $something)
	{
		$this->db->select('team_id','hw_id');
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$check_id = $this->db->get('family_stress')->row_array();

		if (empty($check_id['team_id']) && empty($check_id['hw_id'])) { //如果資料庫沒有這組的資料
			//沒有資料就做新增
			$this->db->set('team_id',$team_id);
			$this->db->set('hw_id',$hw_id);
			$this->db->set($field,$something);
			$this->db->insert('family_stress');
			return ture;
		} else {
			//有資料就做修改
			$this->db->set($field,$something);
			$this->db->where('team_id',$team_id);
			$this->db->where('hw_id',$hw_id);
			$this->db->update('family_stress');
			return ture; 
		}
	}
}