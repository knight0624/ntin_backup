<?php
/**
 *用途: 驗證帳密、權限，取得權限、帳戶等資訊 
 */
class Ch0_model extends CI_Model{

	function editForeword_model($text, $team_id, $hw_id){
		$this->db->select("COUNT('foreword_text') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("foreword");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("foreword_text", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("foreword");
		}
		else{
			$this->db->set("foreword_text", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("foreword");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function getForeword_model($team_id, $hw_id){
		$this->db->select("*");
		$this->db->from("foreword");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);

		return $this->db->get()->row_array();
	}

	function addComment_model($text, $team_id, $hw_id) {
		$this->db->select("COUNT('comment') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("foreword");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("comment", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("foreword");
		}
		else{
			$this->db->set("comment", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("foreword");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function addrecord($status , $date){
		$this->db->set("account", $_SESSION["username"]);
		$this->db->set("hw_id" , $_SESSION["hw_id"]);
		$this->db->set("team_id" ,$_SESSION["team_id"]);
		$this->db->set("date", $date);
		$this->db->set("invite_note" , $status);
		$this->db->insert("record");

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function getrecord($team_id, $hw_id)
	{
		$this->db->select();
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);

		$data_all = $this->db->get('record')->result_array();
		return $data_all;
	}

	function editstatus($id , $status){
	
		$this->db->set("invite_note", $status);
		$this->db->where("record_id" , $id);
		$this->db->update("record");
	}
	function deletestatus($id){
		$this->db->where("record_id", $id);
		$this->db->delete("record");
		
	}

	/**
	 * [新增使用者帳號]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	function insertRespondent_model($data, $enry, $class){
		$this->db->set("account", $data["account"]);
		$this->db->set("password", $data["account"]);
		$this->db->set("name", $data["user_name"]);
		if(!is_null($data["email"])){
			$this->db->set("email", $data["email"]);
		}
		$this->db->set("permission_id", 4);
		$this->db->set("entry", $enry);
		$this->db->set("class", $class);
		$this->db->set("create_time" , "NOW()" , FALSE);

		$this->db->insert("user");

		$this->db->set("team_id", $_SESSION["team_id"]);
		$this->db->set("member", $data["account"]);
		$this->db->insert("team_member");


		$this->db->select("account");
		$this->db->from("user");
		$this->db->order_by("create_time", "DESC");
		$this->db->limit(1);

		return $this->db->get();
	}

	function checkAc_model($data){
		$this->db->from("user");
		$this->db->where("account" , $data["account"]);

		$result = $this->db->get();

		if($result->num_rows() > 0){
			return FALSE;
		}
		return TRUE;
	}

	function getEntryClass(){
		$this->db->select("entry, class");
		$this->db->from("user");
		$this->db->where("account" , $_SESSION["username"]);

		return $this->db->get();
	}
}

?>