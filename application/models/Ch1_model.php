<?php
/**
 *用途: 驗證帳密、權限，取得權限、帳戶等資訊 
 */
class Ch1_model extends CI_Model{

	function editForeword_model($text, $team_id, $hw_id){
		$this->db->select("COUNT('foreword_text') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("foreword");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("foreword_text", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("foreword");
		}
		else{
			$this->db->set("foreword_text", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("foreword");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function getForeword_model($team_id, $hw_id){
		$this->db->select("*");
		$this->db->from("foreword");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);

		return $this->db->get()->row_array();
	}

	function addComment_model($text, $team_id, $hw_id) {
		$this->db->select("COUNT('comment') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("foreword");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("comment", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("foreword");
		}
		else{
			$this->db->set("comment", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("foreword");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function addRespond_model($text, $team_id, $hw_id) {
		$this->db->select("COUNT('respond') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("foreword");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("respond", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("foreword");
		}
		else{
			$this->db->set("respond", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("foreword");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}


	function addrecord($account , $datetime){
		$this->db->set("account", $account);
		$this->db->set("datetime", $datetime);
		$this->db->set("hw_id" , $_SESSION["hw_id"]);
		$this->db->insert("record");

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}


}

?>