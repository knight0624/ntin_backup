<?php
/**
 *用途: 驗證帳密、權限，取得權限、帳戶等資訊 
 */
class Ch12_model extends CI_Model{

	public function editDiscuss_model ($data , $team_id , $hw_id)
	{
		$this->db->select("COUNT('care_problem') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("discuss");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("discuss_content", $data["discuss"]);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("discuss");
		}
		else{
			$this->db->set("discuss_content", $data["discuss"]);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("discuss");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function addComment_model($text, $team_id, $hw_id) {
		$this->db->select("COUNT('comment') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("discuss");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("comment", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("discuss");
		}
		else{
			$this->db->set("comment", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("discuss");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}
	function getDiscuss_model($team_id, $hw_id){
		$this->db->select("*");
		$this->db->from("discuss");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);

		return $this->db->get()->row_array();
	}
}

?>