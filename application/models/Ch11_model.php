<?php
/**
 *用途: 驗證帳密、權限，取得權限、帳戶等資訊 
 */
class Ch11_model extends CI_Model{


	public function editCare_model ($data, $team_id , $hw_id)
	{
		$this->db->select("COUNT('care_problem') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("care_problem");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("care_problem", $data["care_problem"]);
			$this->db->set("care_plan" , $data["care_plan"]);
			$this->db->set("care_supplement" , $data["care_supplement"]);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("care_problem");
		}
		else{
			$this->db->set("care_problem", $data["care_problem"]);
			$this->db->set("care_plan" , $data["care_plan"]);
			$this->db->set("care_supplement" , $data["care_supplement"]);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("care_problem");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function addComment_model($text, $team_id, $hw_id) {
		$this->db->select("COUNT('comment') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("care_problem");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("comment", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("care_problem");
		}
		else{
			$this->db->set("comment", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("care_problem");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	public function editSomething_model($team_id, $field, $something)
	{
		$this->db->select('team_id');
		$this->db->where('team_id',$team_id);
		$check_team_id = $this->db->get('care_problem')->row_array();
		if (empty($check_team_id['team_id'])) { //如果資料庫沒有這組的資料
			//沒有資料就做新增
			$this->db->set('team_id' , $team_id);
			$this->db->set($field , $something);
			$this->db->insert('care_problem');
			return ture;
		}
		else {
			//有資料就做修改
			$this->db->set($field , $something);
			$this->db->where('team_id' , $team_id);
			$this->db->update('care_problem');
			return ture; 
		}
	}



	function getCare_model($team_id, $hw_id){
		$this->db->select("*");
		$this->db->from("care_problem");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);

		return $this->db->get()->row_array();
	}
}

?>