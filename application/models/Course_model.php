<?php

class Course_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function getCourseDetial_model($course_id , $permission , $second){
		$this->db->select("course.course_id , course.course_name , course.academic_year , course.instructor , course.semester");
		$this->db->from("course");
		$this->db->where("SHA1(CONCAT('ntin_',course.course_id))" , $course_id);
		if($permission == $second){
			$this->db->where("course.instructor" , $_SESSION["username"]);
		} elseif($permission > $second){
			$this->db->join("(select * from course_student where account ='".$_SESSION["username"]."') as course_student" , "course.course_id = course_student.course_id");
		}

		return $this->db->get();
	}

	function setCourse_model($data){
		$this->db->set("course_name" , $data["course_name"]);
		$this->db->set("academic_year" , $data["academic_year"]);
		$this->db->set("instructor" , $data["instructor"]);
		$this->db->set("semester" , $data["semester"]);
		$this->db->insert("course");
	}

	function updateCourse_model($data){
		$this->db->set("course_name" , $data["course_name"]);
		$this->db->set("academic_year" , $data["academic_year"]);
		$this->db->set("instructor" , $data["instructor"]);
		$this->db->set("semester" , $data["semester"]);
		$this->db->where("SHA1(CONCAT('ntin_',course.course_id))" , $data["course_id"]);
		$this->db->update("course");
	}

	function lockCourse_model($data){
		$this->db->set("course_status" , "IF(course_status=1,0,1)", FALSE);
		$this->db->where("SHA1(CONCAT('ntin_',course.course_id))" , $data["course_id"]);
		$this->db->update("course");
	}

	function getCourseStudent_model($course_id , $class , $year){
		$this->db->select("user.account , user.name , user.entry , user.class , course_student.course_id");
		$this->db->from("user");
		$this->db->join("(select * from course_student where course_student.course_id = ".$course_id.") as course_student" , "user.account = course_student.account" , "LEFT");
		$this->db->where("user.permission_id" , 3);
		$this->db->where("user.status" , 1);
		if(!is_null($year)){
			$this->db->where("user.entry" , $year);
		}
		if(!is_null($class) && $class != "all"){
			$this->db->where("user.class" , $class);
		}
		return $this->db->get();
	}

	function setStudent_model($acc){
		$this->db->set("course_id" , $_SESSION["course_id"]);
		$this->db->set("account" , $acc);
		$this->db->insert("course_student");
	}

	function deleteStudent_model($acc){
		$this->db->where("course_id" , $_SESSION["course_id"]);
		$this->db->where("account" , $acc);
		$this->db->delete("course_student");
	}

	function batch_model($data ,$fun){
		$this->db->where("course_id" , $_SESSION["course_id"]);
		$this->db->where_in("account" , $data);
		$this->db->delete("course_student");
		if($fun == "insert"){
			$temp = array();
			for ($i=0; $i < count($data); $i++) {
				$temp[$i] = array("course_id"=>$_SESSION["course_id"] , "account"=>$data[$i]);
			}
			$this->db->insert_batch("course_student", $temp);
		}
	}

	function getNum_model(){
		$this->db->where("course_id" , $_SESSION["course_id"]);
		$this->db->from("course_student");
		return $this->db->count_all_results();
	}

	function getHwName($hw_id){
		$this->db->select("homework.hw_name, course.course_name");
		$this->db->from("homework");
		$this->db->join("course", "homework.course_id = course.course_id");
		$this->db->where("SHA1(CONCAT('ntin_', hw_id)) = " , $hw_id);

		return $this->db->get();

	}
}

?>