<?php
/**
 *用途: 驗證帳密、權限，取得權限、帳戶等資訊
 */
class Ch13_model extends CI_Model{

	public function editReference_model ($text , $team_id, $hw_id)
	{
		$this->db->select("COUNT('reference_content') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("reference");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("reference_content", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("reference");
		}
		else{
			$this->db->set("reference_content", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("reference");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function addComment_model($text,$team_id, $hw_id) {
		$this->db->select("COUNT('comment') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("reference");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("comment", $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("reference");
		}
		else{
			$this->db->set("comment", $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("reference");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function getReference_model($team_id, $hw_id){
		$this->db->select("*");
		$this->db->from("reference");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);

		return $this->db->get()->row_array();
	}

	function editScore_model($score, $team_id, $hw_id){
		$this->db->set("score", $score);
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id ", $hw_id);
		$this->db->update("main");
	}

	function getScore_model($team_id, $hw_id){
		$this->db->select("*");
		$this->db->from("main");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);

		return $this->db->get();
	}

	public function uploadName_model ($team_id, $hw_id) {
		$this->db->select("*");
		$this->db->from("reference_file");
		$this->db->where("team_id",$team_id);
		$this->db->where("hw_id", $hw_id);
		return $this->db->get()->result_array();
	}

	public function addPttName_model($team_id, $hw_id, $data) {
		// $filename = 'ppt'.$number.'_name';
		$this->db->select('team_id');
		$this->db->where('team_id',$team_id);
		$check_id = $this->db->get('reference_file')->row_array();
		// if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set("file_name" , $data);
			$this->db->insert('reference_file');
		// } else {
		// 	$this->db->set("file_name" , $data);
		// 	$this->db->update('reference_file');
		// }
	}

	public function deleteFile_model($team_id, $hw_id, $filename) {
		$this->db->select('team_id');
		$this->db->where('team_id',$team_id);
		$this->db->where("SHA1(CONCAT('ntin_', hw_id)) =", $hw_id);
		$check_id = $this->db->get('reference_file')->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->where("file_name", $filename);
			$this->db->delete('reference_file');
		}
	}

}

?>
