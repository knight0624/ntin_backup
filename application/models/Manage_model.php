<?php

class Manage_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	/**
	 * [取得比當前權限還小的權限okok]
	 * @param  [type] $myPermission [description]
	 * @return [type]               [description]
	 */
	function getPermission_model($myPermission){
		$this->db->select("opt_permission_id, opt_permission_name");
		$this->db->from("opt_permission");

		$this->db->where("opt_permission_id > ", $myPermission);

		return $this->db->get();
	}

	/**
	 * [新增使用者帳號]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	function insertUser_model($data){
		$this->db->set("account", $data["account"]);
		$this->db->set("password", $data["account"]);
		$this->db->set("name", $data["user_name"]);
		$this->db->set("email", $data["email"]);
		$this->db->set("permission_id", $data["permission"]);
		$this->db->set("entry", $data["entry"]);
		$this->db->set("class", $data["class"]);
		$this->db->set("create_time" , "NOW()" , FALSE);

		$this->db->insert("user");

		$this->db->select("account");
		$this->db->from("user");
		$this->db->order_by("create_time", "DESC");
		$this->db->limit(1);

		return $this->db->get();
	}

	function getUserInfo_model($account){
		$this->db->select("name , email , opt_permission.opt_permission_name , opt_user_status.status_name , IF(entry != '', entry , '無') as entry, IF(class != '', class , '無') as class");
		$this->db->from("user");
		$this->db->join("opt_permission" , "user.permission_id = opt_permission.opt_permission_id");
		$this->db->join("opt_user_status" , "user.status = opt_user_status.status_id");
		$this->db->where("SHA(CONCAT('ntin_',user.account))" , $account);
		$result = $this->db->get();

		return $result;
	}

	function updateUserInfo_model($data){
		if(!empty($_POST["pw"])){
			$this->db->set("password" , $data["pw"]);
		}
		$this->db->set("name" , $data["user_name"]);
		$this->db->set("email" , $data["email"]);
		$this->db->set("entry" , $data["entry"]);
		$this->db->set("class" , $data["class"]);
		$this->db->set("permission_id" , $data["permission"]);
		$this->db->where("SHA1(CONCAT('ntin_',account))" , $_SESSION["account_index"]);
		$this->db->update("user");
	}

	function getDefault_model(){
		$this->db->select("name , email , permission_id , status , entry , class");
		$this->db->from("user");
		$this->db->where("SHA1(CONCAT('ntin_',account))" , $_SESSION["account_index"]);

		return $this->db->get();
	}

	function getInfo_model(){

		$this->db->select("name , email , opt_position.position_name , opt_department.dep_name , phone , fax , cell , opt_permission.opt_permission_name ,opt_user_status.status_name");
		$this->db->from("user");
		$this->db->join("opt_position" , "user.position_id = opt_position.position_id");
		$this->db->join("opt_department" , "user.dep_id = opt_department.dep_id");
		$this->db->join("opt_permission" , "user.permission_id = opt_permission.opt_permission_id");
		$this->db->join("opt_user_status" , "user.status = opt_user_status.status_id");
		$this->db->where("user.account" , $_SESSION["username"]);
		$result = $this->db->get();

		return $result;

	}

	function checkAc_model($data){
		$this->db->from("user");
		$this->db->where("account" , $data["account"]);

		$result = $this->db->get();

		if($result->num_rows() > 0){
			return FALSE;
		}
		return TRUE;
	}

	function checkPw_model($pw){
		$this->db->from("user");
		$this->db->where("account" , $_SESSION["username"]);
		$this->db->where("password" , $pw);

		$result = $this->db->get();

		if($result->num_rows() > 0){
			return TRUE;
		}
		return FALSE;
	}

	function updateInfo_model($data){
		if(!empty($_POST["pw"])){
			$this->db->set("password" , $data["pw"]);
		}
		$this->db->set("name" , $data["user_name"]);
		$this->db->set("email" , $data["email"]);
		$this->db->set("position_id" , $data["po"] , FALSE);
		$this->db->set("dep_id" , $data["de"] , FALSE);
		$this->db->set("phone" , $data["phone"]);
		$this->db->set("fax" , $data["fax"]);
		$this->db->set("cell" , $data["cell"]);
		$this->db->where("account" , $_SESSION["username"]);
		$this->db->update("user");
	}

	/**
	 * [使用狀態切換]
	 * @param  [type] $account [description]
	 * @return [type]          [description]
	 */
	function updateStatus_model($account){
		$this->db->set("status" , "IF(status=1,0,1)" , false);
		$this->db->where("SHA1(CONCAT('ntin_',account))" , $account);
		$this->db->update("user");
	}
}

?>