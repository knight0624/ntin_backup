<?php

class Info_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function getInfo_model(){
		$this->db->select("name , email , opt_permission.opt_permission_name , opt_user_status.status_name , IF(entry != '', entry , '無') as entry, IF(class != '', class , '無') as class");
		$this->db->from("user");
		$this->db->join("opt_permission" , "user.permission_id = opt_permission.opt_permission_id");
		$this->db->join("opt_user_status" , "user.status = opt_user_status.status_id");
		$this->db->where("user.account" , $_SESSION["username"]);
		$result = $this->db->get();

		return $result;

	}

	function checkPw_model($pw){
		$this->db->from("user");
		$this->db->where("account" , $_SESSION["username"]);
		$this->db->where("password" , $pw);

		$result = $this->db->get();

		if($result->num_rows() > 0){
			 return TRUE;
		}
		return FALSE;
	}

	function updateInfo_model($data){
		if(!empty($_POST["pw"])){
			$this->db->set("password" , $data["pw"]);
		}
		$this->db->set("name" , $data["user_name"]);
		$this->db->set("email" , $data["email"]);
		$this->db->set("entry" , $data["entry"]);
		$this->db->set("class" , $data["class"]);
		$this->db->where("account" , $_SESSION["username"]);
		$this->db->update("user");
	}

	function getDefault_model(){
		$this->db->select("name , email , permission_id , status , entry , class");
		$this->db->from("user");
		$this->db->where("account" , $_SESSION["username"]);

		return $this->db->get();
	}
}

?>