<?php

class Common_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function getCourseYear_model(){
		$this->db->distinct();
		$this->db->select("academic_year");
		$this->db->from("course");
		$this->db->order_by("academic_year" , "DESC");

		return $this->db->get();
	}

	function getCourseEntry_model(){
		$this->db->distinct();
		$this->db->select("entry");
		$this->db->from("user");
		$this->db->where("user.permission_id" , 3);
		$this->db->order_by("entry" , "DESC");

		return $this->db->get();
	}

	function getStudentClass_model(){
		$this->db->distinct();
		$this->db->select("class");
		$this->db->from("user");
		$this->db->where("user.permission_id" , 3);
		$this->db->order_by("class" , "DESC");

		return $this->db->get();
	}

	function getInstructor_model(){
		$this->db->select("account , name");
		$this->db->from("user");
		$this->db->where("permission_id" , 2);
		return $this->db->get();
	}

	function getPermission_model(){
		return $this->db->get("opt_permission");
	}

	function getNumofPermission_model($number){
		if (is_numeric ($number) || $number == "max" || $number == "min"){
			if (is_numeric ($number)) {
				$this->db->distinct("permission_id");
				$this->db->select("permission_id");
				$this->db->from("user");
				$this->db->order_by("permission_id", "asc");
				$this->db->limit(1, ($number-1));
			} elseif ($number == "max") {
				$this->db->select_max("permission_id");
				$this->db->from("user");
			} elseif ($number == "min") {
				$this->db->select_min("permission_id");
				$this->db->from("user");
			}
			return $this->db->get();
		}
	}
	/**
	 * 取得 Main_id
	 * @param  [type] $hw_id   [description]
	 * @param  [type] $team_id [description]
	 */
	function getMain($hw_id, $team_id){
		$this->db->select("*");
		$this->db->from("user_history");
		$this->db->where("user_history.hw_id = ", $hw_id);
		$this->db->where("user_history.team_id", $team_id);
		$this->db->join("main", "user_history.team_id = main.team_id");
		$this->db->join("homework", "main.hw_id = SHA1(CONCAT('ntin_', homework.hw_id))");

		return $this->db->get();
	}

	/**
	 * 取得作業名稱
	 * @param  [type] $hw_id [description]
	 */
	function getHwName($hw_id){
		$this->db->select("hw_name");
		$this->db->from("homework");
		$this->db->where("CONVERT(SHA1(CONCAT('ntin_', hw_id)),char) = ", $hw_id);

		return $this->db->get();
	}

	function getLoginPermission($account){
		$this->db->select("permission_id, team.team_id, team.hw_id");
		$this->db->join("team_member", "user.account = team_member.member");
		$this->db->join("team", "team_member.team_id = team.team_id");
		$this->db->from("user");
		$this->db->where("account ", $account);

		return $this->db->get();
	}

}

?>