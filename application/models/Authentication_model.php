<?php
/**
 *用途: 驗證帳密、權限，取得權限、帳戶等資訊 
 */
class Authentication_Model extends CI_Model{

	/**
	 * [判斷是否為合法帳密]
	 * @param	[type] $username帳號
	 * @param	[type] $password密碼
	 * @return [type] 帳號姓名
	 */
	function verify_User($username , $password){

		$this->db->select("account , name , permission_id , status");
		$this->db->from("user");
		$this->db->where("account" , $username);
		$this->db->where("password" , $password);

		$result = $this->db->get();

		return $result;
	}

	/**
	 * [判斷是否為合法權限]
	 * @param  [type] $fetch_class [description]
	 * @param  [type] $username    [description]
	 * @return [type]              [description]
	 */
	function verify_Permission($fetch_class){

		$this->db->select("user.account");
		$this->db->from("user");
		$this->db->join("permission" , "user.permission_id = permission.permission_id");
		$this->db->join("menu_detail" , "permission.menu_detailed_id = menu_detail.menu_detailed_id");
		$this->db->where("user.account" , $_SESSION["username"]);
		$this->db->where("menu_detail.menu_path" , $fetch_class);

		$result = $this->db->get();

		if($result->num_rows() > 0){
			 return TRUE;
		}
		return FALSE;

	}

	/**
	 * [從資料庫取得該權限所有功能]
	 * @param  [type] $permission_level [description]
	 * @return [type]                   [description]
	 */
	function getMenuList($permission_level){

		$this->db->select("menu_detail.menu_name , menu_detail.menu_path , menu_detail.menu_group_id");
		$this->db->from("permission");
		$this->db->join("menu_detail" , "menu_detail.menu_detailed_id = permission.menu_detailed_id");
		$this->db->where("permission.permission_id" , $permission_level);
		$this->db->order_by("menu_detail.menu_detailed_id", "ASC");
		$result = $this->db->get();

		return $result;

	}

	/**
	 * [從資料庫取得該權限功能群組]
	 * @param  [type] $permission_level [description]
	 * @return [type]                   [description]
	 */
	function getMenuGroup($permission_level){

		$this->db->distinct();
		$this->db->select("menu_group.menu_group_id , menu_group.menu_group_name");
		$this->db->from("permission");
		$this->db->join("menu_detail" , "menu_detail.menu_detailed_id = permission.menu_detailed_id");
		$this->db->join("menu_group" , "menu_detail.menu_group_id = menu_group.menu_group_id");
		$this->db->where("permission.permission_id" , $permission_level);
		$this->db->order_by("menu_group.menu_group_id", "ASC");
		$result = $this->db->get();

		return $result;

	}

	/**
	 * [getFunctiondescribe 取得功能資訊]
	 * @param	[type] $fetch_class 目前使用功能
	 * @return [type] array
	 */
	function getMenuInfo($fetch_class){

		$this->db->select("menu_name , menu_describe");
		$this->db->from("menu_detail");
		$this->db->where("menu_path" , $fetch_class);
		$result = $this->db->get();

		return $result;
	}
}

?>