<?php
/**
 * 用途: 記錄"柒、家庭發展"資料
 */
class ch7_model extends CI_Model{

	/**
	* [儲存表單資訊]
	* @param  [type] $team_id [description]
	* @param  [type] $data	[description]
	* @return [type]		  [description]
	*/
	public function tableUpdate_model($team_id, $hw_id, $data) {
		$this->db->select("COUNT('fd_periods') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("family_developmental");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_developmental", $data);
		}
		else{
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("family_developmental", $data);
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}

	}

	/**
	 * [儲存"補充"內容]
	 * @param [type] $team_id [description]
	 * @param [type] $data	[description]
	 */
	public function addSuppleMent_model($team_id, $hw_id, $data) {
		$this->db->select("COUNT('fd_supplement') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("family_developmental");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set('fd_supplement' , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_developmental");
		}
		else{
			$this->db->set('fd_supplement' , $data);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("family_developmental");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}


	/**
	 * [取出隊伍所有資料]
	 * @param [type] $team_id [description]
	 */
	public function getTeam_All_model($team_id, $hw_id) {
		$this->db->select("*");
		$this->db->from('family_developmental');
		$this->db->where('team_id', $team_id);
		$this->db->where('hw_id', $hw_id);

		return $this->db->get()->row_array();
	}

	public function addComment_model($team_id, $hw_id, $data) {
		$this->db->select("COUNT('comment') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("family_developmental");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("comment", $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_developmental");
		}
		else{
			$this->db->set("comment", $data);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("family_developmental");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}
}
?>