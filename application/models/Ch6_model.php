<?php
/**
 * 用途: 記錄"陸、家庭環境"資料
 */
class ch6_model extends CI_Model{

	/**
	 * [儲存"家中平面圖"內容]
	 * @param [type] $team_id [description]
	 * @param [type] $hw_id   [description]
	 * @param [type] $data    [description]
	 */
	public function addPlanarGraph_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get("family_environment")->row_array();
		if (empty($check_id["team_id"]) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set("fe_planar_graph", $data);
			$this->db->insert("family_environment");
		} else {
			$this->db->set("fe_planar_graph", $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_environment");
		}
	}

	/**
	 * [儲存"家庭鄰近圖"內容]
	 * @param [type] $team_id [description]
	 * @param [type] $hw_id   [description]
	 * @param [type] $data    [description]
	 */
	public function addNeighborhoodGraph_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get("family_environment")->row_array();
		if (empty($check_id["team_id"]) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set("fe_neighborhood_graph", $data);
			$this->db->insert("family_environment");
		} else {
			$this->db->set("fe_neighborhood_graph", $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_environment");
		}
	}

	/**
	 * [儲存"社區關係圖"內容]
	 * @param [type] $team_id [description]
	 * @param [type] $hw_id   [description]
	 * @param [type] $data    [description]
	 */
	public function addCommunityRelationshipGraph_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get("family_environment")->row_array();
		if (empty($check_id["team_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set("fe_community_relationship_graph", $data);
			$this->db->insert("family_environment");
		} else {
			$this->db->set("fe_community_relationship_graph", $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_environment");
		}
	}

	/**
	 * [儲存PPT檔案名稱]
	 * @param [type] $team_id [description]
	 * @param [type] $hw_id   [description]
	 * @param [type] $data    [description]
	 * @param [type] $number  [description]
	 */
	public function addPttName_model($team_id, $hw_id, $data, $number) {
		$filename = "ppt".$number."_name";
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get("upload")->row_array();
		if (empty($check_id["team_id"]) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set($filename, $data);
			$this->db->insert("upload");
		} else {
			$this->db->set($filename, $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("upload");
		}
	}

	/**
	 * [儲存圖片檔案名稱]
	 * @param [type] $team_id [description]
	 * @param [type] $hw_id   [description]
	 * @param [type] $data    [description]
	 * @param [type] $number  [description]
	 */
	public function addImgName_model($team_id, $hw_id, $data, $number) {
		$filename = "img".$number."_name";
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get("upload")->row_array();
		if (empty($check_id["team_id"]) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set($filename, $data);
			$this->db->insert("upload");
		} else {
			$this->db->set($filename, $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("upload");
		}
	}

	/**
	 * [取出隊伍所有資料]
	 * @param  [type] $team_id [description]
	 * @param  [type] $hw_id   [description]
	 * @return [type]          [description]
	 */
	public function getTeam_All_model ($team_id, $hw_id) {
		$this->db->select("*");
		$this->db->from("family_environment");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		return $this->db->get()->row_array();
	}

	/**
	 * [取出隊伍上傳資料]
	 * @param  [type] $team_id [description]
	 * @param  [type] $hw_id   [description]
	 * @return [type]          [description]
	 */
	public function uploadName_model ($team_id, $hw_id) {
		$this->db->select("*");
		$this->db->from("upload");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		return $this->db->get()->row_array();
	}

	/**
	 * [刪除隊伍上傳資料]
	 * @param  [type] $team_id [description]
	 * @param  [type] $hw_id   [description]
	 * @param  [type] $data    [description]
	 * @param  [type] $number  [description]
	 * @return [type]          [description]
	 */
	public function deleteFile_model($team_id, $hw_id, $data, $number) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get("upload")->row_array();
		if (empty($check_id["team_id"]) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set($number, $data);
			$this->db->insert("upload");
		} else {
			$this->db->set($number, $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("upload");
		}
	}

	/**
	 * [儲存老師評語]
	 * @param [type] $team_id [description]
	 * @param [type] $hw_id   [description]
	 * @param [type] $data    [description]
	 */
	public function addComment_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get("family_environment")->row_array();
		if (empty($check_id["team_id"]) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set("comment", $data);
			$this->db->insert("family_environment");
		} else {
			$this->db->set("comment", $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("family_environment");
		}
	}
}
?>