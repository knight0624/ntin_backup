
<?php
/**
 *用途: 驗證帳密、權限，取得權限、帳戶等資訊 
 */
class Ch2_model extends CI_Model{

	public function editLiterature_model($data, $team_id , $hw_id)
	{
		$this->db->select("COUNT('literature_text') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("literature");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("literature_text" , $data["literature_text"]);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("literature");
		}
		else{
			$this->db->set("literature_text" , $data["literature_text"]);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("literature");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}



	function getLiterature_model($team_id,$hw_id){
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$result = $this->db->get("literature")->row_array();
		return $result;
	}

	public function editComment_model($text, $team_id,$hw_id) {
		$this->db->select("COUNT('comment') as count");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("literature");
		$count = $this->db->get()->row()->count;

		if ($count>0) {
			$this->db->set("comment" , $text);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update("literature");
		}
		else{
			$this->db->set("comment" , $text);
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->insert("literature");
		}

		if($this->db->affected_rows() > 0){
			return true;
		} else {
			return false;
		}
	}
}

?>