<?php
/**
 * 用途: 記錄"伍、家庭結構"資料
 */
class ch5_model extends CI_Model{

	/**
  	 * [儲存"角色結構"內容]
  	 * @param [type] $team_id [description]
  	 * @param [type] $data	[description]
  	 */
	public function addRoleStructure_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get('family_structure')->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set('fstructure_role_structure' , $data);
			$this->db->insert('family_structure');
		} else {
			$this->db->set('fstructure_role_structure' , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update('family_structure');
		}
	}

	/**
  	 * [儲存"權力結構"內容]
  	 * @param [type] $team_id [description]
  	 * @param [type] $data	[description]
  	 */
	public function addPowerStructure_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get('family_structure')->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set('fstructure_power_structure' , $data);
			$this->db->insert('family_structure');
		} else {
			$this->db->set('fstructure_power_structure' , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update('family_structure');
		}
	}

	/**
  	 * [儲存"溝通"內容]
  	 * @param [type] $team_id [description]
  	 * @param [type] $data	[description]
  	 */
	public function addCommunication_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$this->db->from("family_structure");
		$check_id = $this->db->get()->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set('fstructure_communication' , $data);
			$this->db->insert('family_structure');
		} else {
			$this->db->set('fstructure_communication' , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update('family_structure');
		}
	}

	/**
  	 * [儲存"價值觀"內容]
  	 * @param [type] $team_id [description]
  	 * @param [type] $data	[description]
  	 */
	public function addValues_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get('family_structure')->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set('fstructure_values' , $data);
			$this->db->insert('family_structure');
		} else {
			$this->db->set('fstructure_values' , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update('family_structure');
		}
	}

	/**
	 * [儲存"補充"內容]
	 * @param [type] $team_id [description]
	 * @param [type] $data	[description]
	 */
	public function addSuppleMent_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get('family_structure')->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set('fstructure_supplement' , $data);
			$this->db->insert('family_structure');
		} else {
			$this->db->set('fstructure_supplement' , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update('family_structure');
		}
	}

	/**
	 * [儲存PPT檔案名稱]
	 * @param [type] $team_id [description]
	 * @param [type] $data	[description]
	 */
	public function addPttName_model($team_id, $hw_id, $data, $number) {
		$filename = 'ppt'.$number.'_name';
		$this->db->select('team_id');
		$this->db->where('team_id',$team_id);
		$check_id = $this->db->get('upload')->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set($filename , $data);
			$this->db->insert('upload');
		} else {
			$this->db->set($filename , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update('upload');
		}
	}

	/**
	 * [儲存圖片檔案名稱]
	 * @param [type] $team_id [description]
	 * @param [type] $data	[description]
	 */
	public function addImgName_model($team_id, $hw_id, $data, $number) {
		$filename = 'img'.$number.'_name';
		$this->db->select('team_id');
		$this->db->where('team_id',$team_id);
		$check_id = $this->db->get('upload')->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set($filename , $data);
			$this->db->insert('upload');
		} else {
			$this->db->set($filename , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update('upload');
		}
	}

	/**
	 * [取出隊伍所有資料]
	 * @param [type] $team_id [description]
	 */
	public function getTeam_All_model($team_id, $hw_id) {
		$this->db->select("*");
		$this->db->from('family_structure');
		$this->db->where('team_id',$team_id);
		$this->db->where("hw_id", $hw_id);
		return $this->db->get()->row_array();
	}

	/**
	 * [取出隊伍上傳資料]
	 * @param [type] $team_id [description]
	 */	
	public function uploadName_model ($team_id, $hw_id) {
		$this->db->select("*");
		$this->db->from('upload');
		$this->db->where('team_id',$team_id);
		$this->db->where("hw_id", $hw_id);
		return $this->db->get()->row_array();
	}

	/**
	 * [刪除隊伍上傳資料]
	 * @param  [type] $team_id [description]
	 * @param  [type] $data	[description]
	 * @return [type]		  [description]
	 */
	public function deleteFile_model($team_id, $hw_id, $data, $number) {
		$this->db->select('team_id');
		$this->db->where('team_id',$team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get('upload')->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set($number , $data);
			$this->db->insert('upload');
		} else {
			$this->db->set($number , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update('upload');
		}
	}

	/**
	 * [儲存老師評語]
	 * @param [type] $team_id [description]
	 * @param [type] $hw_id   [description]
	 * @param [type] $data    [description]
	 */
	public function addComment_model($team_id, $hw_id, $data) {
		$this->db->select("*");
		$this->db->where("team_id", $team_id);
		$this->db->where("hw_id", $hw_id);
		$check_id = $this->db->get('family_structure')->row_array();
		if (empty($check_id['team_id']) && empty($check_id["hw_id"])) {
			$this->db->set("team_id", $team_id);
			$this->db->set("hw_id", $hw_id);
			$this->db->set('comment' , $data);
			$this->db->insert('family_structure');
		} else {
			$this->db->set('comment' , $data);
			$this->db->where("team_id", $team_id);
			$this->db->where("hw_id", $hw_id);
			$this->db->update('family_structure');
		}
	}
}
?>