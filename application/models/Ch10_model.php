<?php
/**
 *位置：家庭資源(family_resources)
 */
class ch10_model extends CI_Model{
	
	/**
	 * 查詢這隊伍所有 family_resources 資料
	 * @param  [type] $team_id  [description]
	 * @param  [type] $hw_id    [description]
	 * @return [type] $data_all [description]
	 */
	function getFamilyResources_model ($team_id, $hw_id)
	{
		$this->db->select();
		$this->db->where("team_id",$team_id);
		$this->db->where("hw_id",$hw_id);
		$data_all = $this->db->get("family_resources")->row_array();
		return $data_all;
	}

	/**
	 * 查詢特定資料
	 * @param  [type] $team_id        [description]
	 * @param  [type] $hw_id          [description]
	 * @param  [type] $something      [description]
	 * @return [type] $data_something [description]
	 */
	function getSomething_model ($team_id, $hw_id, $something)
	{
		$this->db->select($something);
		$this->db->where('team_id',$team_id);
		$this->db->where('hw_id',$hw_id);
		$data_something = $this->db->get('family_resources')->row_array();
		return $data_something;
	}

	/**
	 * 資料寫入:FAMLIS
	 * @param  [type] $team_id    [description]
	 * @param  [type] $hw_id      [description]
	 * @param  [type] $array_data [description]
	 */
	function editFAMLIS_model ($team_id, $hw_id, $array_data)
	{
		$this->db->select("team_id","hw_id");
		$this->db->where("team_id",$team_id);
		$this->db->where("hw_id",$hw_id);
		$check_team_id = $this->db->get("family_resources")->row_array();

		if (empty($check_team_id["team_id"])) { //資料庫有沒有這組的資料
			//沒有資料就做新增
			$data = array(
				"team_id"               => $team_id,
				"hw_id"                 => $hw_id,
				"fr_financial_sup"      => $array_data["financial_sup"],
				"fr_advocacy_sup"       => $array_data["advocacy_sup"],
				"fr_medical_management" => $array_data["medical_management"],
				"fr_love"               => $array_data["love"],
				"fr_ioe"                => $array_data["IOE"],
				"fr_structure_sup"      => $array_data["structure_sup"]
			);
			$this->db->set($data);
			$this->db->insert("family_resources");
			return ture;
		} else {
			$data = array(
				"fr_financial_sup"      => $array_data["financial_sup"],
				"fr_advocacy_sup"       => $array_data["advocacy_sup"],
				"fr_medical_management" => $array_data["medical_management"],
				"fr_love"               => $array_data["love"],
				"fr_ioe"                => $array_data["IOE"],
				"fr_structure_sup"      => $array_data["structure_sup"]
			);
			$this->db->set($data);
			$this->db->where("team_id",$team_id);
			$this->db->where("hw_id",$hw_id);
			$this->db->update("family_resources");
			return ture;
		}
	}

	/**
	 * 資料寫入:SCREEEM
	 * @param  [type] $team_id    [description]
	 * @param  [type] $hw_id      [description]
	 * @param  [type] $array_data [description]
	 */
	function editSCREEEM_model($team_id, $hw_id, $array_data)
	{
		$this->db->select("team_id","hw_id");
		$this->db->where("team_id",$team_id);
		$this->db->where("hw_id",$hw_id);
		$check_team_id = $this->db->get("family_resources")->row_array();

		if (empty($check_team_id["team_id"])) { //資料庫有沒有這組的資料
			//沒有資料就做新增
			$data = array(
				"team_id"        => $team_id,
				"hw_id"          => $hw_id,
				"fr_community"   => $array_data["community"],
				"fr_culture"     => $array_data["culture"],
				"fr_religion"    => $array_data["religion"],
				"fr_income"      => $array_data["income"],
				"fr_education"   => $array_data["education"],
				"fr_surrounding" => $array_data["surrounding"],
				"fr_medical"     => $array_data["medical"]
			);
			$this->db->set($data);
			$this->db->insert("family_resources");
			return ture;
		} else {
			$data = array(
				"fr_community"   => $array_data["community"],
				"fr_culture"     => $array_data["culture"],
				"fr_religion"    => $array_data["religion"],
				"fr_income"      => $array_data["income"],
				"fr_education"   => $array_data["education"],
				"fr_surrounding" => $array_data["surrounding"],
				"fr_medical"     => $array_data["medical"]
			);
			$this->db->set($data);
			$this->db->where("team_id" , $team_id);
			$this->db->where("hw_id",$hw_id);
			$this->db->update("family_resources");
			return ture;
		}
	}

	/**
	 * 寫入特定資料
	 * @param  [type] $team_id   [description]
	 * @param  [type] $hw_id     [description]
	 * @param  [type] $field     [description]
	 * @param  [type] $something [description]
	 */
	function editSomething_model($team_id, $hw_id, $field, $something)
	{
		$this->db->select("team_id","hw_id");
		$this->db->where("team_id",$team_id);
		$this->db->where("hw_id",$hw_id);
		$check_team_id = $this->db->get("family_resources")->row_array();
		if (empty($check_team_id["team_id"])) { //如果資料庫沒有這組的資料
			//沒有資料就做新增
			$this->db->set("team_id" , $team_id);
			$this->db->set("hw_id" , $hw_id);
			$this->db->set($field , $something);
			$this->db->insert("family_resources");
			return ture;
		} else {
			//有資料就做修改
			$this->db->set($field , $something);
			$this->db->where("team_id" , $team_id);
			$this->db->where("hw_id" , $hw_id);
			$this->db->update("family_resources");
			return ture; 
		}
	}
}