<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
      .p-style{
        color: #04438e!important;
      }
			.btn-group{
				margin-top: 20px;
			}
			.upload-group{
				margin: 0px auto;
				text-align: center;
			}
			.upload-group div{
				display: inline-block;
				vertical-align: top;
			}
			.btn-group{
				margin-left: 5px;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 捌、家庭功能</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '<h3>一、生育功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><textarea rows="20" class="form-control" id="reproduction">'.$DB_all['ff_reproduction'].'</textarea></div>'.
														'</div>'.
														'<h3>二、情感功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><textarea rows ="20" class="form-control" id="sexual">'.$DB_all['ff_sexual'].'</textarea></div>'.
														'</div>'.
														'<h3>三、社會化功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><textarea rows ="20" class="form-control" id="socialisation">'.$DB_all['ff_socialisation'].'</textarea></div>'.
														'</div>'.
														'<h3>四、經濟功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><textarea rows ="20" class="form-control" id="economic">'.$DB_all['ff_economic'].'</textarea></div>'.
														'</div>'.
														'<h3>五、健康照護功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><textarea rows ="20" class="form-control" id="health">'.$DB_all['ff_health'].'</textarea></div>'.
														'</div>'.
														'<div class="form-group">'.
															'<div class="col-sm-2 ">'.
																'<button class="btn btn-primary" id="save_Ch8">儲存</button>'.
															'</div>'.
													'</div>';
											break;

											case "2" :
											case "6" :
												echo '<h3>一、生育功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><p rows ="20" class="form-control" id="reproduction">'.$DB_all['ff_reproduction'].'</p></div>'.
														'</div>'.
														'<h3>二、情感功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><p rows ="20" class="form-control" id="sexual">'.$DB_all['ff_sexual'].'</p></div>'.
														'</div>'.
														'<h3>三、社會化功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><p rows ="20" class="form-control" id="socialisation">'.$DB_all['ff_socialisation'].'</p></div>'.
														'</div>'.
														'<h3>四、經濟功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><p rows ="20" class="form-control" id="economic">'.$DB_all['ff_economic'].'</p></div>'.
														'</div>'.
														'<h3>五、健康照護功能：</h3>'.
														'<div class="form-group">'.
															'<div class="col-sm-12"><p rows ="20" class="form-control" id="health">'.$DB_all['ff_health'].'</p></div>'.
														'</div>';
											break;
										}
									?>
									</div>
								</div>
								<div class="ibox-content">
									<h3>
										六、家庭圈
									</h3>
									<p class="p-style">　　Thrower於1982年提出家庭圈的理論，它是應用心理投射的概念，由家庭成員主觀認為個成員在家中的重要性與親密性，以了解其家庭互動。圈圈越大，代表權力越大，圈圈互相靠近表示彼此關係密切。
									</p>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '<div style="text-align: center;">'.
														( !empty($DB_all['ff_familycircle_img']) ? ('<img src="'.base_url().'uploads/img/'.$DB_all['ff_familycircle_img'].'" style="display:block; margin:auto; height:auto; max-width:50%;">') : ('<img src="'.base_url().'dist/img/family_circle.png" style="display:block; margin:auto; height:auto; max-width:50%;">') ) .
													'<br>'.
													'<p class="text-center p-style">家庭圈範例</p>'.
													'</div>'.
													'<div class="upload-group">'.
														'<div>'.
															'<a  href="'.base_url().'download/家庭圈.pptx" download>'.
																'<img src="'.base_url().'dist/img/download_example.png" style="width:150px;">'.
															'</a>'.
														'</div>'.
														'<div>'.
															form_open_multipart('Ch8/FamilyCircle_ppt_upload').
															'<input type="file" name="userfile_ppt" id="FamilyCircle_ppt" style="display:none;">'.
															'<img src="'.base_url().'dist/img/select_ppt.png" id="FamilyCircle_ppt_new" value="選擇 PPT 檔" style="width:150px;">'.
															'<input type="submit" id="updata_FamilyCircle_ppt" style="display:none;">'.
															'<img src="'.base_url().'dist/img/upload_ppt.png" id="updata_FamilyCircle_ppt_new" value="上傳" style="width:150px;">'.
															'</form>'.
															'<div class="btn-group">'.
																( !empty($DB_all['ff_familycircle_ppt']) ?
																	'<a class="btn btn-primary btn-xs" href="'.base_url().'uploads/ppt/'.$DB_all['ff_familycircle_ppt'].'" download style="margin:auto;">下載：'.$DB_all['ff_familycircle_ppt'].'</a>'.
																	'<a class="btn btn-danger btn-xs delete_file" id="delete_file" data-file="'.$DB_all['ff_familycircle_ppt'].'"><i class="fa fa-trash"></i></a>'
																	: ''
																).
															'</div>'.
														'</div>'.
														'<div>'.
															form_open_multipart('Ch8/FamilyCircle_img_upload').
															'<input type="file" name="userfile_img" id="FamilyCircle_img" style="display:none;">'.
															'<img src="'.base_url().'dist/img/select_img.png" id="FamilyCircle_img_new" value="選擇圖片檔" style="width:150px;">'.
															'<input type="submit" id="updata_FamilyCircle_img" style="display:none;">'.
															'<img src="'.base_url().'dist/img/upload_img.png" id="updata_FamilyCircle_img_new" value="上傳" style="width:150px;">'.
															'</form>'.
															'<div class="btn-group">'.
																( !empty($DB_all['ff_familycircle_img']) ?
																	'<a class="btn btn-primary btn-xs" href="'.base_url().'uploads/img/'.$DB_all['ff_familycircle_img'].'" download style="margin:auto;">下載：'.$DB_all['ff_familycircle_img'].'</a>'.
																	'<a class="btn btn-danger btn-xs delete_file" id="delete_file" data-file="'.$DB_all['ff_familycircle_img'].'"><i class="fa fa-trash"></i></a>'
																	: ''
																).
															'</div>'.
														'</div>'.
													'</div>'.
													'<div align="center">'.
														'<p class="text-danger p-style">檔案大小限制為8MB</p>'.
													'</div>';
											break;

											case "2" :
											case "6" :
												echo '<div style="text-align: center;">'.
														( !empty($DB_all['ff_familycircle_img']) ? ('<img src="'.base_url().'uploads/img/'.$DB_all['ff_familycircle_img'].'" style="display:block; margin:auto; height:auto; max-width:50%;">') : ('<img src="'.base_url().'dist/img/family_circle.png" style="display:block; margin:auto; height:auto; max-width:50%;">') ) .
													'<br>'.
													'<p class="text-center">家庭圈範例</p>'.
													'</div>'.
													'<div class="upload-group">'.
														'<div class="btn-group">'.
															( !empty($DB_all['ff_familycircle_ppt']) ?
																'<a class="btn btn-primary btn-xs" href="'.base_url().'uploads/ppt/'.$DB_all['ff_familycircle_ppt'].'" download style="margin:auto;">下載：'.$DB_all['ff_familycircle_ppt'].'</a>'
																: ''
															).
														'</div>'.
														'<div class="btn-group">'.
															( !empty($DB_all['ff_familycircle_img']) ?
																'<a class="btn btn-primary btn-xs" href="'.base_url().'uploads/img/'.$DB_all['ff_familycircle_img'].'" download style="margin:auto;">下載：'.$DB_all['ff_familycircle_img'].'</a>'
																: ''
															).
														'</div>'.
													'</div>';
											break;
										}
									?>
								</div>
								<div class="ibox-content">
									<h3>
										七、家庭APGAR簡易量表
									</h3>
									<p class="p-style">　　由史麥克史坦(Smikstein)提出，此量表適用於初次家庭評估，其測量表中共五個問題：調適能力(adaptation; A)、合作程度(partnership; P)、成長度(growth; G)、情愛程度(affection; A)、融洽度(resolve; R)。</p>
									<p class="p-style">　　每題答案選項包括經常、有時、幾乎很少，分別２、１、０分，總分若為０～３分表示重度功能障礙；４～６分表示中度功能障礙；７～１０分表示家庭功能無障礙。</p>
									<br>
									<div class="form-horizontal">
										<table class="table table-striped">
										<?php
											// 1 學生
											// 2 老師
											// 3 admin
											switch ($permission) {
												case "1" :
												case "3" :
													echo '<table class="table table-bordered" border="1">'.
															'<thead>'.
																'<th class="col-md-1"></th>'.
																'<th class="col-md-6 col-md-offset-3">請回答下列問題　(每一個問題請選擇一個答案)</th>'.
																'<th class="text-center col-md-1">經常</th>'.
																'<th class="text-center col-md-1">有時</th>'.
																'<th class="text-center col-md-1">幾乎很少</th>'.
															'</thead>'.
															'<tbody>'.
																'<tr>'.
																	'<th class="text-center" scope="row">1.</th>'.
																	'<td>我滿意於當我遭遇困難時，可以向家人求助。(Adaptation)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="adaptation" class="APGAR_1"></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="adaptation" class="APGAR_1"></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="adaptation" class="APGAR_1"></td>'.
																'</tr>'.
																'<tr>'.
																	'<th class="text-center" scope="row">2.</th>'.
																	'<td>我滿意於家人與我討論各種事情。(Partnership)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="partnership" class="APGAR_2"></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="partnership" class="APGAR_2"></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="partnership" class="APGAR_2"></td>'.
																'</tr>'.
																'<tr>'.
																	'<th class="text-center" scope="row">3.</th>'.
																	'<td>我滿意於當我希望從事新的活動或發展方向時，家人能夠接受且給予支持。(Growth)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="growth" class="APGAR_3"></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="growth" class="APGAR_3"></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="growth" class="APGAR_3"></td>'.
																'</tr>'.

																	'<th class="text-center" scope="row">4.</th>'.
																	'<td>我滿意於家人對我表達情感的方式以及對我的情緒(如憤怒、悲傷，愛)的反應。(Affection)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="affection" class="APGAR_4"></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="affection" class="APGAR_4"></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="affection" class="APGAR_4"></td>'.
																'</tr>'.
																'<tr>'.
																	'<th class="text-center" scope="row">5.</th>'.
																	'<td>我滿意於家人與我共處時光的方式。(Resolve)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="resolve" class="APGAR_5"></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="resolve" class="APGAR_5"></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="resolve" class="APGAR_5"></td>'.
																'</tr>'.
                                '<tr>'.
                                  '<th class="text-center">結果</th>'.
                                  '<td colspan=4><p class="text-center" id="APGAR_result"></p></td>'.
                                '</tr>'.
															'</tbody>'.
														'</table>'.
														'<div class="form-group">'.
															'<div class="col-sm-2 ">'.
																'<button class="btn btn-primary" id="save_APGAR">儲存</button>'.
															'</div>'.
														'</div>';
												break;

												case "2" :
												case "6" :
													echo '<table class="table table-bordered" border="1">'.
															'<thead>'.
																'<th class="col-md-1"></th>'.
																'<th class="col-md-6 col-md-offset-3">請回答下列問題　(每一個問題請選擇一個答案)</th>'.
																'<th class="text-center col-md-1">經常</th>'.
																'<th class="text-center col-md-1">有時</th>'.
																'<th class="text-center col-md-1">幾乎很少</th>'.
															'</thead>'.
															'<tbody>'.
																'<tr>'.
																	'<th class="text-center" scope="row">1.</th>'.
																	'<td>我滿意於當我遭遇困難時，可以向家人求助。(Adaptation)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="adaptation" class="APGAR_1" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="adaptation" class="APGAR_1" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="adaptation" class="APGAR_1" disabled></td>'.
																'</tr>'.
																'<tr>'.
																	'<th class="text-center" scope="row">2.</th>'.
																	'<td>我滿意於家人與我討論各種事情。(Partnership)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="partnership" class="APGAR_2" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="partnership" class="APGAR_2" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="partnership" class="APGAR_2" disabled></td>'.
																'</tr>'.
																'<tr>'.
																	'<th class="text-center" scope="row">3.</th>'.
																	'<td>我滿意於當我希望從事新的活動或發展方向時，家人能夠接受且給予支持。(Growth)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="growth" class="APGAR_3" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="growth" class="APGAR_3" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="growth" class="APGAR_3" disabled></td>'.
																'</tr>'.

																	'<th class="text-center" scope="row">4.</th>'.
																	'<td>我滿意於家人對我表達情感的方式以及對我的情緒(如憤怒、悲傷，愛)的反應。(Affection)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="affection" class="APGAR_4" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="affection" class="APGAR_4" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="affection" class="APGAR_4" disabled></td>'.
																'</tr>'.
																'<tr>'.
																	'<th class="text-center" scope="row">5.</th>'.
																	'<td>我滿意於家人與我共處時光的方式。(Resolve)</td>'.
																	'<td class="text-center"><input type="radio" value="2" name="resolve" class="APGAR_5" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="1" name="resolve" class="APGAR_5" disabled></td>'.
																	'<td class="text-center"><input type="radio" value="0" name="resolve" class="APGAR_5" disabled></td>'.
																'</tr>'.
                                '<tr>'.
                                  '<th class="text-center">結果</th>'.
                                  '<td colspan=4><p class="text-center" id="APGAR_result"></p></td>'.
                                '</tr>'.
															'</tbody>'.
														'</table>'.
														'<div class="form-group">'.
														'</div>';
												break;
											}
										?>
									</div>
								</div>
								<div class="ibox-content">
									<h3>
										八、PRACTICE家庭功能評估表
									</h3>
									<p class="p-style">　　由麥克吉爾(McGill)設計，經由家訪會談，藉問題(problem)、角色(role)、情感(affection)、溝通(communication)、時間(time)、疾病(illness)、調適(coping)及生活環境(environment)，之項目加以評估了解家庭問題。</p>
									<br>
									<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '<div class="form-group">'.
														'<label class="col-sm-2 control-label">缺席的家中成員：</label>'.
														'<div class="col-sm-10">'.
															'<input type="text" class="form-control" id="absence" value="'.$DB_all['ff_absence'].'">'.
														'</div>'.
													'</div>'.
													'<div class="form-group">'.
														'<label class="col-sm-2 control-label">出席的家中成員：</label>'.
														'<div class="col-sm-10">'.
															'<input type="text" class="form-control" id="attend" value="'.$DB_all['ff_attend'].'">'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">P：呈現的問題或家庭會談的原因 (Presenting problem<small>(s)</small>)</label>'.
                            '<br>'.
                            '<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="problem">'.$DB_all['ff_problem'].'</textarea> <span class="help-block m-b-none text-right">問題描述，由何人發覺？如何發生？家人嘗試解決的方式？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">R：角色、結構、組織 (Roles)</label>'.
														'<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="roles">'.$DB_all['ff_roles'].'</textarea> <span class="help-block m-b-none text-right">誰做主？「親子間聯合」、「三角關係」與「同盟」的本質？彼此間界限的特色？角色彈性？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">A：情感 (Affect)</label>'.
														'<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="affect">'.$DB_all['ff_affect'].'</textarea> <span class="help-block m-b-none text-right">主要的情緒表現？會談中情感波動的範圍？難以表達的情緒？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">C：溝通 (Communication)</label>'.
														'<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="communication">'.$DB_all['ff_communication'].'</textarea> <span class="help-block m-b-none text-right">明朗？直接？隱藏？轉移？一致？誰在發言？誰聽誰的話？肢體語言的溝通情形？經由疾病來溝通的情形？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">T：時期 (Time)</label>'.
														'<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="time">'.$DB_all['ff_time'].'</textarea> <span class="help-block m-b-none text-right">求偶？成家？生兒育女？教養兒女？孩子離家發展？家庭縮小？退休？鰥寡？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">I：疾病 (Illness)</label>'.
														'<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="illness">'.$DB_all['ff_illness'].'</textarea> <span class="help-block m-b-none text-right">過去或現在的嚴重疾病？慢性病或經常發生的急性病？疾病角色-家中何人易生病？最近有無家人過世？與醫療保健機構接觸的經驗？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">C：調適或適應性 (Coping or Adaptability)</label>'.
														'<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="adaptability">'.$DB_all['ff_adaptability'].'</textarea> <span class="help-block m-b-none text-right">家庭力量及資源？過去與目前的調適？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">C：生活環境 (Ecology or Environment)</label>'.
														'<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="ecology">'.$DB_all['ff_ecology'].'</textarea> <span class="help-block m-b-none text-right">與宗族的關係？經濟狀況？文化修養與宗教？社會、學校及專業資源的運用情形？娛樂消遣？</span>'.
														'</div>'.
													'</div>'.
													'<div class="form-group">'.
														'<div class="col-sm-2">'.
															'<button class="btn btn-primary" id="save_PRACTICE">儲存</button>'.
														'</div>'.
													'</div>';
											break;

											case "2" :
											case "6" :
												echo '<div class="form-group">'.
														'<label class="col-sm-2 control-label">缺席的家中成員：</label>'.
														'<div class="col-sm-10">'.
															'<p class="form-control" id="absence">'.$DB_all['ff_absence'].'</p>'.
														'</div>'.
													'</div>'.
													'<div class="form-group">'.
														'<label class="col-sm-2 control-label">出席的家中成員：</label>'.
														'<div class="col-sm-10">'.
															'<p class="form-control" id="absence">'.$DB_all['ff_attend'].'</p>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">P：呈現的問題或家庭會談的原因 (Presenting problem<small>(s)</small>)</label>'.
														'<div class="col-sm-12">'.
															'<p rows ="20" class="form-control" id="problem">'.$DB_all['ff_problem'].'</p> <span class="help-block m-b-none text-right">問題描述，由何人發覺？如何發生？家人嘗試解決的方式？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">R：角色、結構、組織 (Roles)</label>'.
														'<div class="col-sm-12">'.
															'<p rows ="20" class="form-control" id="roles">'.$DB_all['ff_roles'].'</p> <span class="help-block m-b-none text-right">誰做主？「親子間聯合」、「三角關係」與「同盟」的本質？彼此間界限的特色？角色彈性？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">A：情感 (Affect)</label>'.
														'<div class="col-sm-12">'.
															'<p rows ="20" class="form-control" id="affect">'.$DB_all['ff_affect'].'</p> <span class="help-block m-b-none text-right">主要的情緒表現？會談中情感波動的範圍？難以表達的情緒？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">C：溝通 (Communication)</label>'.
														'<div class="col-sm-12">'.
															'<p rows ="20" class="form-control" id="communication">'.$DB_all['ff_communication'].'</p> <span class="help-block m-b-none text-right">明朗？直接？隱藏？轉移？一致？誰在發言？誰聽誰的話？肢體語言的溝通情形？經由疾病來溝通的情形？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">T：時期 (Time)</label>'.
														'<div class="col-sm-12">'.
															'<p rows ="20" class="form-control" id="time">'.$DB_all['ff_time'].'</p> <span class="help-block m-b-none text-right">求偶？成家？生兒育女？教養兒女？孩子離家發展？家庭縮小？退休？鰥寡？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">I：疾病 (Illness)</label>'.
														'<div class="col-sm-12">'.
															'<p rows ="20" class="form-control" id="illness">'.$DB_all['ff_illness'].'</p> <span class="help-block m-b-none text-right">過去或現在的嚴重疾病？慢性病或經常發生的急性病？疾病角色-家中何人易生病？最近有無家人過世？與醫療保健機構接觸的經驗？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">C：調適或適應性 (Coping or Adaptability)</label>'.
														'<div class="col-sm-12">'.
															'<p rows ="20" class="form-control" id="adaptability">'.$DB_all['ff_adaptability'].'</p> <span class="help-block m-b-none text-right">家庭力量及資源？過去與目前的調適？</span>'.
														'</div>'.
													'</div>'.
													'<div class="hr-line-dashed"></div>'.
													'<div class="form-group">'.
														'<label class="col-sm-12">C：生活環境 (Ecology or Environment)</label>'.
														'<div class="col-sm-12">'.
															'<p rows ="20" class="form-control" id="ecology">'.$DB_all['ff_ecology'].'</p> <span class="help-block m-b-none text-right">與宗族的關係？經濟狀況？文化修養與宗教？社會、學校及專業資源的運用情形？娛樂消遣？</span>'.
														'</div>'.
													'</div>';
											break;
										}

									?>
									</div>
								</div>
								<div class="ibox-content">
									<h3>九、補充</h3>
									<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '<div class="form-group">'.
														'<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="supplement">'.$DB_all['ff_supplement'].'</textarea>'.
														'</div>'.
													'</div>'.
													'<div class="form-group">'.
														'<div class="col-sm-2">'.
															'<button class="btn btn-primary" id="save_supplement">儲存</button>'.
														'</div>'.
													'</div>';
											break;

											case "2" :
											case "6" :
												echo '<div class="form-group">'.
														'<div class="col-sm-12">'.
															'<p rows ="20" class="form-control" id="supplement">'.$DB_all['ff_supplement'].'</p>'.
														'</div>'.
													'</div>';
											break;
										}
									?>
									</div>
								</div>
								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch7");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch9");?>" class="btn btn-default">下一步</a>
								</div>
							</div>

							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2>老師評語</h2>
							 	</div>
							 	<div class="ibox-content">
							 	<?php
							 		// 1 學生
									// 2 老師
									// 3 admin
									switch ($permission) {
										case "1" :
										case "6" :
											echo '<div class="form-horizontal">' .
													'<p class="text-danger form-control">'.$DB_all["comment"].'</p>' .
										 		'</div>';
										break;

										case "2" :
										case "3" :
											echo '<div class="form-horizontal">' .
										 			'<textarea rows ="20" class="form-control" name="comment" rows="10" placeholder="老師評語">'.$DB_all['comment'].'</textarea>' .
										 			'<div class="form-group">' .
										 				'<div class="col-sm-10"><br>' .
										 					'<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>' .
										 				'</div>' .
										 			'</div>' .
										 		'</div>';
										break;
									}
							 	?>
							 	</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'           : {},
				'.chosen-select-deselect'  : {allow_single_deselect:true},
				'.chosen-select-no-single' : {disable_search_threshold:10},
				'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}

			/**
			 * 家庭圈.img檔案上傳按鈕
			 */
			if (document.getElementById("FamilyCircle_img_new")) {
				document.getElementById("FamilyCircle_img_new").onclick = function() {
					document.getElementById("FamilyCircle_img").click(true);
				}
			}
			if (document.getElementById("updata_FamilyCircle_img_new")) {
				document.getElementById("updata_FamilyCircle_img_new").onclick = function() {
					document.getElementById("updata_FamilyCircle_img").click(true);
				}
			}

			/**
			 * 家庭圈.ppt/pptx檔案上傳按鈕
			 */
			if (document.getElementById("FamilyCircle_ppt_new")) {
				document.getElementById("FamilyCircle_ppt_new").onclick = function() {
					document.getElementById("FamilyCircle_ppt").click(true);
				}
			}
			if (document.getElementById("updata_FamilyCircle_ppt_new")) {
				document.getElementById("updata_FamilyCircle_ppt_new").onclick = function() {
					document.getElementById("updata_FamilyCircle_ppt").click(true);
				}
			}
			/**
			 * 刪除文件
			 */
			var file_array = document.querySelectorAll(".delete_file")
			for (var i = 0; i < file_array.length; i++) {
				file_array[i].onclick = function() {
					var file_name = this.dataset.file
					swal({	title: "刪除文件?",
						text: "按下刪除後文件不可復原",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "刪除!",
						cancelButtonText: "取消",
						closeOnConfirm: false,
						closeOnCancel: false },
						function(isConfirm) {
							if (isConfirm) {
								$.ajax({
									url: "<?php echo base_url(); ?>Ch8/Delete_File" ,
									type: "POST" ,
									data: {
										file_name : file_name
									} ,
									dataType: "text" ,
									success: function(data){
										if (data) {
											location.reload();
										}
										else{
											swal("錯誤!", "刪除時發生錯誤!", "error");
										}
									}
								});
							}
							else {
								swal("已取消!", "您的文件是安全的", "error");
							}
						}
					);
				}
			}

			/**
			 * APGAR_checkbox status
			 */
			window.onload=Check_checked;
			function Check_checked() {
			    var APGAR_1 = document.querySelectorAll(".APGAR_1");
				var APGAR_2 = document.querySelectorAll(".APGAR_2");
				var APGAR_3 = document.querySelectorAll(".APGAR_3");
				var APGAR_4 = document.querySelectorAll(".APGAR_4");
				var APGAR_5 = document.querySelectorAll(".APGAR_5");
				$.ajax({
					url: "<?php echo base_url(); ?>Ch8/Select_APGAR_Status" ,
					success: function(data) {
						var json_value = JSON.parse(data);
						for (var i = 1; i <= 5; i++) {
							for (var x = 0; x <= (eval('APGAR_'+i+'.length') - 1) ; x++) {
								if (json_value[i-1] == eval('APGAR_'+i)[x].value) {
									eval('APGAR_'+i)[x].checked = true;
									break;
								}
							}
						}
						var result = [];
						var APGAR_result = 0;
						for (var i = 1; i <= 5 ; i++) {
							for (var x = 0; x <= (eval('APGAR_'+i+'.length') - 1) ; x++) {
								if (eval('APGAR_'+i)[x].checked) {
									result[i-1] = eval('APGAR_'+i)[x].value;
									APGAR_result += parseInt(eval('APGAR_'+i)[x].value);
								}
							}
						}
						if (APGAR_result >= 0 && APGAR_result <= 3) {
							document.querySelector("#APGAR_result").innerHTML = "重度功能障礙";
							document.querySelector("#APGAR_result").style.color = "rgb(240, 0, 0)";
						} else if (APGAR_result >= 4 && APGAR_result <= 6) {
							document.querySelector("#APGAR_result").innerHTML = "中度功能障礙";
							document.querySelector("#APGAR_result").style.color = "rgb(240, 180, 0)";
						} else if (APGAR_result >= 7 && APGAR_result <= 10) {
							document.querySelector("#APGAR_result").innerHTML = "無功能障礙";
							document.querySelector("#APGAR_result").style.color = "rgb(15, 200, 0)";
						}
					}
				});
			}

			/**
			 * APGAR's value加總
			 * @return 功能狀態
			 */
			if(document.querySelector("#save_APGAR")) {
				document.querySelector("#save_APGAR").onclick = function() {
					var APGAR_1 = document.querySelectorAll(".APGAR_1");
					var APGAR_2 = document.querySelectorAll(".APGAR_2");
					var APGAR_3 = document.querySelectorAll(".APGAR_3");
					var APGAR_4 = document.querySelectorAll(".APGAR_4");
					var APGAR_5 = document.querySelectorAll(".APGAR_5");
					var result = [];
					var APGAR_result = 0;
					for (var i = 1; i <= 5 ; i++) {
						for (var x = 0; x <= (eval('APGAR_'+i+'.length') - 1) ; x++) {
							if (eval('APGAR_'+i)[x].checked) {
								result[i-1] = eval('APGAR_'+i)[x].value;
								APGAR_result += parseInt(eval('APGAR_'+i)[x].value);
							}
						}
					}
					$.ajax({
						url: "<?php echo base_url(); ?>Ch8/APGAR" ,
						type: "POST" ,
						data: {
							APGAR_result : JSON.stringify(result)
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								if (APGAR_result >= 0 && APGAR_result <= 3) {
									document.querySelector("#APGAR_result").innerHTML = "重度功能障礙";
									document.querySelector("#APGAR_result").style.color = "rgb(240, 0, 0)";
								} else if (APGAR_result >= 4 && APGAR_result <= 6) {
									document.querySelector("#APGAR_result").innerHTML = "中度功能障礙";
									document.querySelector("#APGAR_result").style.color = "rgb(240, 180, 0)";
								} else if (APGAR_result >= 7 && APGAR_result <= 10) {
									document.querySelector("#APGAR_result").innerHTML = "無功能障礙";
									document.querySelector("#APGAR_result").style.color = "rgb(15, 200, 0)";
								}
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "您有欄位尚未填寫",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}

			/**
			 * PRACTICE
			 */
			if(document.querySelector("#save_PRACTICE")) {
				document.querySelector("#save_PRACTICE").onclick = function() {
					var PRACTICE_result = {};
					PRACTICE_result.absence        = document.querySelector("#absence").value;
					PRACTICE_result.attend         = document.querySelector("#attend").value;
					PRACTICE_result.problem        = document.querySelector("#problem").value;
					PRACTICE_result.roles          = document.querySelector("#roles").value;
					PRACTICE_result.affect         = document.querySelector("#affect").value;
					PRACTICE_result.communication  = document.querySelector("#communication").value;
					PRACTICE_result.time           = document.querySelector("#time").value;
					PRACTICE_result.illness        = document.querySelector("#illness").value;
					PRACTICE_result.adaptability   = document.querySelector("#adaptability").value;
					PRACTICE_result.ecology        = document.querySelector("#ecology").value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch8/PRACTICE" ,
						type: "POST" ,
						data: {
							PRACTICE_result : PRACTICE_result
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}

			/**
			 * 儲存 Ch8
			 */
			if(document.querySelector("#save_Ch8")) {
				document.querySelector("#save_Ch8").onclick = function() {
					var ff_result = {};
					ff_result.reproduction  = document.querySelector("#reproduction").value;
					ff_result.economic      = document.querySelector("#economic").value;
					ff_result.sexual        = document.querySelector("#sexual").value;
					ff_result.socialisation = document.querySelector("#socialisation").value;
					ff_result.health        = document.querySelector("#health").value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch8/Insert_Ch8" ,
						type: "POST" ,
						data: {
							ff_result : ff_result
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}

			/**
			 * 儲存 補充
			 */
			if(document.querySelector("#save_supplement")) {
				document.querySelector("#save_supplement").onclick = function() {
					var supplement = document.querySelector("#supplement").value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch8/Save_Supplement" ,
						type: "POST" ,
						data: {
							supplement : supplement
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}

			/**
			 * 儲存 老師評語
			 */
			if(document.querySelector("#save_comment")) {
				document.querySelector("#save_comment").onclick = function() {
					var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch8/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
