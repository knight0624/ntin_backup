<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 拾貳、討論</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
										<?php
											switch ($permission) {
												case '1':
												case '3':
													echo '
														<div class="form-group">
															<label class="col-sm-12">一、心得與問題解決：</label>
															<div class="col-sm-12">
																<textarea class="form-control" rows="20" placeholder="一、心得與問題解決" id="discuss">'.$discuss_content.'</textarea>
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-12">
																<button class="btn btn-primary" id="btn_discuss">儲存</button>
															</div>
														</div>';
												break;
												case '2':
												case '6':
													echo '
														<div class="form-group">
															<label class="col-sm-12">一、心得與問題解決：</label>
															<div class="col-sm-12">
																<p class="form-control" id="btn_discuss">'.$discuss_content.'</p>
															</div>
														</div>';
												break;
											}
										?>
									</div>
								</div>

								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch11");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch13");?>" class="btn btn-default">下一步</a>
								</div>
							</div>

							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2>老師評語</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
										<?php
											switch ($permission) {
												case '1':
												case '6':
													echo '<p class="text-danger form-control">'.$comment.'</p>';
												break;
												case '2':
												case '3':
													echo '
														<textarea class="form-control" name="comment" rows="10" placeholder="老師評語">'.$comment.'</textarea>
														<div class="form-group">
															<div class="col-sm-12"><br>
																<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>
															</div>
														</div>';
												break;
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>

		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>

		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			/**
			 * [onclick Foreword AJAX]
			 * @return {[type]} [description]
			 */
			 if(document.querySelector("#btn_discuss")){
				document.querySelector("#btn_discuss").onclick = function(){
					var discuss = document.querySelector("#discuss").value;
						$.ajax({
							url: "<?php echo base_url(); ?>Ch12/editDiscuss" ,
							type: "POST" ,
							data: {
								discuss : discuss
							} ,
							dataType: "text" ,
							success: function(data){

								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});

							}
						});
				}
			}

			/**
			 * 儲存 老師評語
			 */
			if(document.querySelector("#save_comment")){
				document.querySelector("#save_comment").onclick = function(){
					var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch12/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
