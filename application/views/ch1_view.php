<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<style>
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 壹、前言</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
										<div class="form-group">
											<label class="col-sm-12">前言：</label>
											<?php
													// 1 學生
													// 2 老師
													// 3 admin
													// 6 超過時間
													// 7 受訪者
												switch ($permission) {
													case "1":
													case "3":
														echo '<div class="col-sm-12"><textarea rows="20" class="form-control" rows="4" placeholder="前言" id="foreword_text">'.$foreword_text.'</textarea></div><div class="col-sm-12"><br><button class="btn btn-primary" id="btn_foreword">儲存</button></div>';
														break;

													case "2":
													case "6":
													case "7":
														echo '<div class="col-sm-12"><p class="form-control">'.$foreword_text.'</p></div>';
														break;
												}
											?>
										</div>
									</div>
								</div>
								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch2");?>" class="btn btn-default">下一步</a>
								</div>
							</div>

							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2>老師評語</h2>
							 	</div>
							 	<div class="ibox-content">
							 		<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										// 6 超過時間
										// 7 受訪者
										switch ($permission) {
											case "1":
											case "6":
											case "7":
												echo '<p class="text-danger form-control">'.$comment.'</p>';
												break;

											case "2":
											case "3":
												echo '<textarea rows="10" class="form-control" name="comment" placeholder="老師評語">'.$comment.'</textarea><div class="form-group"><div class="col-sm-10"><br><button type="submit" class="btn btn-primary" id="save_comment">儲存</button></div></div>';
												break;
										}
									?>
									</div>
								</div>
							</div>
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2>受訪者疑問與回答</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										// 6 超過時間
										// 7 受訪者
										switch ($permission) {
											case "1":
											case "3":
											case "7":
												echo '<textarea rows="10" class="form-control" name="respond" placeholder="老師評語">'.$respond.'</textarea><div class="form-group"><div class="col-sm-10"><br><button type="submit" class="btn btn-primary" id="save_respond">儲存</button></div></div>';
												break;

											case "2":
											case "6":
												echo '<p class="text-danger form-control">'.$respond.'</p>';
												break;
										}
									?>
							 		</div>
							 	</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>

		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>
		<script>
			if(document.querySelector("#btn_foreword")){
				document.querySelector("#btn_foreword").onclick = function(){
					var foreword_text = document.querySelector("#foreword_text").value;
					if(foreword_text != null){
						$.ajax({
							url: "<?php echo base_url(); ?>ch1/editForeword" ,
							type: "POST" ,
							data: {
								foreword_text : foreword_text
							} ,
							dataType: "text" ,
							success: function(data){
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							}
						});
					 } else {
						swal({
							title: "警告",
							text: "標題或內文不能為空",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				}
			}

			if(document.querySelector("#save_comment")){
				document.querySelector("#save_comment").onclick = function(){
				var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch1/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data){
							swal({
								title: "成功",
								text: "已儲存",
								type: "success",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					});
				}
			}
			if(document.querySelector("#save_respond")){
				document.querySelector("#save_respond").onclick = function(){
				var respond = document.getElementsByName("respond")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch1/addRespond" ,
						type: "POST" ,
						data: {
							respond_value : respond
						} ,
						dataType: "text" ,
						success: function(data){
							swal({
								title: "成功",
								text: "已儲存",
								type: "success",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
