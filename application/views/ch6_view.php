<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
			.modal {
				text-align: center;
				padding: 0!important;
			}

			.modal:before {
				content: '';
				display: inline-block;
				height: 100%;
				vertical-align: middle;
				margin-right: -4px;
			}

			.modal-dialog {
				display: inline-block;
				text-align: left;
				vertical-align: middle;
				width: 70%;
			}
			.modal-body {
				padding: 15px;
			}
			.showSweetAlert[data-animation=slide-from-top]{
				animation: slideFromTop 0.5s;
			}
			.file-zoom-fullscreen .modal-dialog{
				position: static;
			}
			.btn-group{
				margin-top: 20px;
			}
			.upload-group{
				margin: 0px auto;
				text-align: center;
			}
			.upload-group div{
				display: inline-block;
				vertical-align: top;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 陸、家庭環境 </h2>
								</div>
								<div class="ibox-content">
									<h3>一、家中平面圖</h3>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													"<textarea class='form-control' name='planar' rows='20'>".$DB_all['fe_planar_graph'] ."</textarea>" .
													"<div align='left'>" .
														"<button type='submit' class='btn btn-primary' id='save_Planar'>儲存</button>" .
													"</div>";
												break;
											case '2':
											case '6':
												echo
													"<p class='form-control' name='planar' rows='4'>".$DB_all['fe_planar_graph'] ."</p>";
												break;
										}
									?>
									<img style="display:block; margin:auto; max-width:100%;" src=
										<?php
											if (! empty($upload['img3_name']))
												echo base_url()."uploads/img/".$upload['img3_name'];
											else
												echo base_url()."dist/img/圖3家中平面圖.jpg";
										?>
									>
									<p class="text-center">圖3家中平面圖</p>
									<div class="upload-group">
										<!-- 下載範例 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															'<a href="'.base_url().'download/範例3.pptx" download>' .
																'<img src="'.base_url().'dist/img/download_example.png" style="width:150px;">' .
															'</a>';
														break;
												}
											?>
										</div>
										<!-- 上傳PPT按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch6/do_upload_ppt') .
															'<input type="file" name="userfile" id="ppt3_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_ppt.png" id="ppt3_file_new" value="選擇PPT檔" style="width:150px;">' .
															'<input type="submit" id="ppt3_upload" style="display:none;" value="3" name="number">' .
															'<img src="'.base_url().'dist/img/upload_ppt.png" id="ppt3_upload_new" value="上傳PPT" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['ppt3_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/ppt/".$upload['ppt3_name']."' download style='margin:auto;'>下載：".$upload['ppt3_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(0)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												 ?>
											</div>
										</div>
										<!-- 上傳圖片按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch6/do_upload_img') .
															'<input type="file" name="userfile" id="img3_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_img.png" id="img3_file_new" value="選擇圖片檔" style="width:150px;">' .
															'<input type="submit" id="img3_upload" style="display:none;" value="3" name="number">' .
															'<img src="'.base_url().'dist/img/upload_img.png" id="img3_upload_new" value="上傳圖片" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['img3_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/img/".$upload['img3_name']."' download style='margin:auto;'>下載：".$upload['img3_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(1)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												?>
											</div>
										</div>
									</div>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<div align="center">' .
														'<p class="text-danger">檔案大小限制為8MB</p>' .
													'</div>';
												break;
										}
									?>
								</div>
								<div class="ibox-content">
									<h3>二、家庭鄰近圖</h3>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													"<textarea class='form-control' name='neighborhood' rows='20'>".$DB_all['fe_neighborhood_graph']."</textarea>" .
													"<div align='left'>" .
														"<button type='submit' class='btn btn-primary' id='save_Neighborhood'>儲存</button>" .
													"</div>";
												break;
											case '2':
											case '6':
												echo
													"<p class='form-control' name='neighborhood' rows='4'>".$DB_all['fe_neighborhood_graph']."</p>";
												break;
										}
									?>
									<img style="display:block; margin:auto; max-width:100%;" src=
										<?php
											if (! empty($upload['img4_name']))
												echo base_url()."uploads/img/".$upload['img4_name'];
											else
												echo base_url()."dist/img/圖4家庭近鄰圖.jpg";
										?>
									>
									<p class="text-center">圖4家庭鄰近圖</p>
									<div class="upload-group">
										<!-- 下載範例 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															'<a href="'.base_url().'download/範例4.pptx" download>' .
																'<img src="'.base_url().'dist/img/download_example.png" style="width:150px;">' .
															'</a>';
														break;
												}
											?>
										</div>
										<!-- 上傳PPT按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch6/do_upload_ppt') .
															'<input type="file" name="userfile" id="ppt4_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_ppt.png" id="ppt4_file_new" value="選擇PPT檔" style="width:150px;">' .
															'<input type="submit" id="ppt4_upload" style="display:none;" value="4" name="number">' .
															'<img src="'.base_url().'dist/img/upload_ppt.png" id="ppt4_upload_new" value="上傳PPT" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['ppt4_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/ppt/".$upload['ppt4_name']."' download style='margin:auto;'>下載：".$upload['ppt4_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(2)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												?>
											</div>
										</div>
										<!-- 上傳圖片按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch6/do_upload_img') .
															'<input type="file" name="userfile" id="img4_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_img.png" id="img4_file_new" value="選擇圖片檔" style="width:150px;">' .
															'<input type="submit" id="img4_upload" style="display:none;" value="4" name="number">' .
															'<img src="'.base_url().'dist/img/upload_img.png" id="img4_upload_new" value="上傳圖片" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['img4_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/img/".$upload['img4_name']."' download style='margin:auto;'>下載：".$upload['img4_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(3)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												?>
											</div>
										</div>
									</div>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<div align="center">' .
														'<p class="text-danger">檔案大小限制為8MB</p>' .
													'</div>';
												break;
										}
									?>
								</div>
								<div class="ibox-content">
									<h3>三、社區關係圖</h3>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													"<textarea class='form-control' name='community_relationship' rows='20'>".$DB_all['fe_community_relationship_graph']."</textarea>" .
													"<div align='left'>" .
														"<button type='submit' class='btn btn-primary' id='save_Community_Relationship'>儲存</button>" .
													"</div>";
												break;
											case '2':
											case '6':
												echo
													"<p class='form-control' name='community_relationship' rows='4'>".$DB_all['fe_community_relationship_graph']."</p>";
												break;
										}
									?>
									<img style="display:block; margin:auto; max-width:100%;" src=
										<?php
											if (! empty($upload['img5_name']))
												echo base_url()."uploads/img/".$upload['img5_name'];
											else
												echo base_url()."dist/img/圖5家庭社會關係圖.jpg";
										?>
									>
									<p class="text-center">圖5社區關係圖</p>
									<div class="upload-group">
										<!-- 下載範例 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															'<a href="'.base_url().'download/範例5.pptx" download>' .
																'<img src="'.base_url().'dist/img/download_example.png" style="width:150px;">' .
															'</a>';
														break;
												}
											?>
										</div>
										<!-- 上傳PPT按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch6/do_upload_ppt') .
															'<input type="file" name="userfile" id="ppt5_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_ppt.png" id="ppt5_file_new" value="選擇PPT檔" style="width:150px;">' .
															'<input type="submit" id="ppt5_upload" style="display:none;" value="5" name="number">' .
															'<img src="'.base_url().'dist/img/upload_ppt.png" id="ppt5_upload_new" value="上傳PPT" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['ppt5_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/ppt/".$upload['ppt5_name']."' download style='margin:auto;'>下載：".$upload['ppt5_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(4)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												?>
											</div>
										</div>
										<!-- 上傳圖片按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch6/do_upload_img') .
															'<input type="file" name="userfile" id="img5_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_img.png" id="img5_file_new" value="選擇圖片檔" style="width:150px;">' .
															'<input type="submit" id="img5_upload" style="display:none;" value="5" name="number">' .
															'<img src="'.base_url().'dist/img/upload_img.png" id="img5_upload_new" value="上傳圖片" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['img5_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/img/".$upload['img5_name']."' download style='margin:auto;'>下載：".$upload['img5_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(5)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												?>
											</div>
										</div>
									</div>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<div align="center">' .
														'<p class="text-danger">檔案大小限制為8MB</p>' .
													'</div>';
												break;
										}
									?>
								</div>
								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch5");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch7");?>" class="btn btn-default">下一步</a>
								</div>
							</div>
							<?php
								// 1 學生
								// 2 老師
								// 3 admin
								switch ($permission) {
									case '1':
									case '6':
										echo
											'<div class="ibox float-e-margins">' .
												'<div class="ibox-title">' .
													'<h2>老師評語</h2>' .
												'</div>' .
												'<div class="ibox-content">' .
													'<div class="form-horizontal">' .
														'<p class="text-danger form-control">'.$DB_all["comment"].'</p>' .
													'</div>' .
												'</div>' .
											'</div>' ;
										break;
									case '2':
									case '3':
										echo
											'<div class="ibox float-e-margins">' .
												'<div class="ibox-title">' .
													'<h2>老師評語</h2>' .
												'</div>' .
												'<div class="ibox-content">' .
													'<div class="form-horizontal">' .
														'<textarea class="form-control" name="comment" rows="10" placeholder="老師評語">'.$DB_all["comment"].'</textarea>' .
														'<div class="form-group">' .
															'<div class="col-sm-10"><br>' .
																'<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>' .
															'</div>' .
														'</div>' .
													'</div>' .
												'</div>' .
											'</div>';
										break;
								}
							?>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'           : {},
				'.chosen-select-deselect'  : {allow_single_deselect:true},
				'.chosen-select-no-single' : {disable_search_threshold:10},
				'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}

			if (document.getElementById("ppt3_file_new")) {
				document.getElementById("ppt3_file_new").onclick = function(){
					document.getElementById("ppt3_file").click(true);
				}
			}
			if (document.getElementById("ppt3_upload_new")) {
				document.getElementById("ppt3_upload_new").onclick = function(){
					document.getElementById("ppt3_upload").click(true);
				}
			}
			if (document.getElementById("img3_file_new")) {
				document.getElementById("img3_file_new").onclick = function(){
					document.getElementById("img3_file").click(true);
				}
			}
			if (document.getElementById("img3_upload_new")) {
				document.getElementById("img3_upload_new").onclick = function(){
					document.getElementById("img3_upload").click(true);
				}
			}

			if (document.getElementById("ppt4_file_new")) {
				document.getElementById("ppt4_file_new").onclick = function(){
					document.getElementById("ppt4_file").click(true);
				}
			}
			if (document.getElementById("ppt4_upload_new")) {
				document.getElementById("ppt4_upload_new").onclick = function(){
					document.getElementById("ppt4_upload").click(true);
				}
			}
			if (document.getElementById("img4_file_new")) {
				document.getElementById("img4_file_new").onclick = function(){
					document.getElementById("img4_file").click(true);
				}
			}
			if (document.getElementById("img4_upload_new")) {
				document.getElementById("img4_upload_new").onclick = function(){
					document.getElementById("img4_upload").click(true);
				}
			}

			if (document.getElementById("ppt5_file_new")) {
				document.getElementById("ppt5_file_new").onclick = function(){
					document.getElementById("ppt5_file").click(true);
				}
			}
			if (document.getElementById("ppt5_upload_new")) {
				document.getElementById("ppt5_upload_new").onclick = function(){
					document.getElementById("ppt5_upload").click(true);
				}
			}
			if (document.getElementById("img5_file_new")) {
				document.getElementById("img5_file_new").onclick = function(){
					document.getElementById("img5_file").click(true);
				}
			}
			if (document.getElementById("img5_upload_new")) {
				document.getElementById("img5_upload_new").onclick = function(){
					document.getElementById("img5_upload").click(true);
				}
			}

			if (document.querySelector("#save_Planar")) {
				document.querySelector("#save_Planar").onclick = function(){
					var planar = document.getElementsByName("planar")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch6/fePlanarGraph",
						type: "POST",
						data: {
							planar_value : planar
						},
						dataType: "text",
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
			if (document.querySelector("#save_Neighborhood")) {
				document.querySelector("#save_Neighborhood").onclick = function(){
					var neighborhood = document.getElementsByName("neighborhood")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch6/feNeighborhoodGraph",
						type: "POST",
						data: {
							neighborhood_value : neighborhood
						},
						dataType: "text",
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
			if (document.querySelector("#save_Community_Relationship")) {
				document.querySelector("#save_Community_Relationship").onclick = function(){
					var community_relationship = document.getElementsByName("community_relationship")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch6/feCommunityRelationshipGraph",
						type: "POST",
						data: {
							community_relationship_value : community_relationship
						},
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
			function fileDelete(d) {
				if (d==0) {
					$uploadName = "<?php echo $upload['ppt3_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch6/delete_Ptt"
					$filenumber = 3;
				} else if (d==1) {
					$uploadName = "<?php echo $upload['img3_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch6/delete_Img"
					$filenumber = 3;
				} else if (d==2) {
					$uploadName = "<?php echo $upload['ppt4_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch6/delete_Ptt"
					$filenumber = 4;
				} else if (d==3) {
					$uploadName = "<?php echo $upload['img4_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch6/delete_Img"
					$filenumber = 4;
				} else if (d==4) {
					$uploadName = "<?php echo $upload['ppt5_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch6/delete_Ptt"
					$filenumber = 5;
				} else if (d==5) {
					$uploadName = "<?php echo $upload['img5_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch6/delete_Img"
					$filenumber = 5;
				}
				swal({	title: "刪除文件?",
						text: "按下刪除後文件不可復原",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "刪除!",
						cancelButtonText: "取消",
						closeOnConfirm: false,
						closeOnCancel: false },
						function(isConfirm){
							if (isConfirm) {
								$.ajax({
									url: $uploadUrl,
									type: "POST",
									data: {uploadName:$uploadName, filenumber:$filenumber},
									dataType: "text",
									success: function(data){
										if (data) {
											location.reload();
										}
										else{
											swal("錯誤!", "刪除時發生錯誤!", "error");
										}
									}
								});
							}
							else {
								swal("已取消!", "您的文件是安全的", "error");
							}
						}
				);
			}
			if (document.querySelector("#save_comment")) {
				document.querySelector("#save_comment").onclick = function(){
					var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch6/addComment",
						type: "POST",
						data: {
							comment_value : comment
						},
						dataType: "text",
						success: function(data){
							swal({
								title: "成功",
								text: "已儲存",
								type: "success",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
