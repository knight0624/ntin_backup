<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/dataTables/datatables.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<style>
			.modal {
				text-align: center;
				padding: 0!important;
			}

			.modal:before {
				content: '';
				display: inline-block;
				height: 100%;
				vertical-align: middle;
				margin-right: -4px;
			}

			.modal-dialog {
				display: inline-block;
				text-align: left;
				vertical-align: middle;
				width: 40%;
			}

			.modal-body {
				padding: 15px;
			}

			.showSweetAlert[data-animation=slide-from-top]{
				animation: slideFromTop 0.5s;
			}

			.file-zoom-fullscreen .modal-dialog{
				position: static;
			}

			.btn-grp select {
				width: 170px;
				height: 34px;
				text-align-last: center;
			}

			.vertical-align {
				display: flex;
				align-items: center;
			}

			.btn-grp {
				margin-bottom: 13px;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="ibox">
						<div class="ibox-title">
							<h3>課程列表</h3>
						</div>
						<div class="ibox-content">
							<div style="height:48px;">
								<?php
									if ($writable) {
										echo "<button class='btn btn-primary' id='setModal'>新增課程</button>";
									}
								?>
								<button type="button" class="btn btn-warning" onclick="javascript:history.back()">回上一頁</button>
							</div>
							<div class="btn-grp">
								<select id="select_year" >
									<option value="">選擇年度</option>
									<?php echo $year_opt; ?>
								</select>
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover datatable" style="width:100%;">
									<thead>
										<tr>
											<th></th>
											<th>學年度</th>
											<th>學期</th>
											<th>課程名稱</th>
											<th>授課老師</th>
											<th>狀態</th>
										</tr>
									</thead>

									<tfoot>
										<tr>
											<th></th>
											<th>學年度</th>
											<th>學期</th>
											<th>課程名稱</th>
											<th>授課老師</th>
											<th>狀態</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>

					<div class="modal fade" id="modal_page" role="dialog" tabindex='-1'>
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h3 class="modal-title">新增課程</h3>
								</div>

								<div class="form-horizontal">
									<div class="modal-body">
										<div class="form-group">
											<label class="col-sm-2 col-sm-offset-1 control-label">課程名稱</label>
											<div class="col-sm-9">
												<input type="text" class="form-control" id="course_name" placeholder="請輸入課程名稱">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 col-sm-offset-1 control-label">學年度</label>
											<div class="col-sm-9">
												<input type="number" class="form-control" id="academic_year" placeholder="請輸入學年度">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 col-sm-offset-1 control-label">授課老師</label>
											<div class="col-sm-9">
												<select class="form-control" id="instructor">
													<option value="">請選擇授課講師</option>
													<?php echo $instructor_opt; ?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 col-sm-offset-1 control-label">學期</label>
											<div class="col-sm-9">
												<select class="form-control" id="semester">
													<option>請選擇學期</option>
													<option value="1">上學期</option>
													<option value="2">下學期</option>
												</select>
											</div>
										</div>
										<hr>
										<div class="modal-footer">
											<input type="button" id="setItem" class="btn btn-primary" value="新增">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/dataTables/datatables.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>
		<script>
			var datatable = $(".datatable").DataTable({
				"language":{
					"sEmptyTable":        "沒有搜尋到結果",
					"sInfo":              "顯示第 _START_ 至 _END_ 項結果 , 共 _TOTAL_ 項",
					"sInfoEmpty":         "顯示第 0 至 0 項結果 , 共 0 項",
					"sInfoFiltered":      "（由 _MAX_ 項結果過濾）",
					"sInfoPostFix":       "",
					"sInfoThousands":     ",",
					"sLengthMenu":        "顯示 _MENU_ 項結果",
					"sLoadingRecords":   "載入中...",
					"sProcessing":       "<img src='<?php echo base_url(); ?>dist/img/loader.gif'>",
					"sSearch":            "搜尋:",
					"sZeroRecords":       "沒有搜尋結果",
					"oPaginate": {
						"sFirst":           "首頁",
						"sPrevious":        "上頁",
						"sNext":            "下頁",
						"sLast":            "末頁"
					},
					"oAria": {
						"sSortAscending":  ":以遞增排列此列",
						"sSortDescending": ":以遞減排列此列"
					}
				},
				"processing": true,
				"serverSide": true,
				"scrollX": true,
				"ajax": {
					"url": "<?php echo base_url(); ?>course/getCourse",
					"data":function(d){
						d.select_year = $("#select_year").val();
					}
				},
				"serverMethod": "POST",
				"sorting": [[1, "desc"]],
				"columnDefs": [
					{"width": "100px", "targets": [0] },
					{"orderable": false, "targets": [0] },
					{"bSearchable": false, "targets": [0] }
				],
				"columns": [
					{ "data": "edit_btn" },
					{ "data": "academic_year" },
					{ "data": "semester" },
					{ "data": "course_name" },
					{ "data": "name" },
					{ "data": "course_status" }
				]
			});

			$("#select_year").change(function(){
				datatable.draw();
			});
			var fun , edit , lock;

			$("#setModal").click(function(){
				$("#setItem").val("新增");
				fun = "set";
				$("#modal_page").modal();
			});

			$(".datatable").on("click", ".editbtn", function(){
				$("#setItem").val("更新");
				fun = "update";
				edit = $(this).data("edit");
				$.ajax({
					url: "<?php echo base_url(); ?>course/getCourseDetial",
					type: "POST",
					data: {
						edit : edit
					},
					dataType: "text",
					success: function(data){
						var temp_item = JSON.parse(data);
						document.querySelector("#course_name").value = temp_item.course_name;
						document.querySelector("#academic_year").value = temp_item.academic_year;
						document.querySelector("#instructor").value = temp_item.instructor;
						document.querySelector("#semester").value = temp_item.semester;
						$("#modal_page").modal();
					}
				});
			});

			$("#modal_page").on("hidden.bs.modal", function () {
				initArguments(
					document.querySelector("#course_name"),
					document.querySelector("#academic_year"),
					document.querySelector("#instructor"),
					document.querySelector("#semester")
				);
			})

			$("#setItem").click(function(){
				var course_name = document.querySelector("#course_name").value;
				var academic_year = document.querySelector("#academic_year").value;
				var instructor = document.querySelector("#instructor").value;
				var semester = document.querySelector("#semester").value;

				var check_value = checkArguments(course_name , academic_year , instructor , semester);
				if( check_value ){
					swal({
						title: "注意!",
						text: "課程資料將永久保留在資料庫",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#ec4758",
						confirmButtonText: "確定",
						cancelButtonText: "取消",
						closeOnConfirm: false,
						closeOnCancel: false,
						animation: "slide-from-top"
					},function(isConfirm) {
						if (isConfirm) {
							if(fun=="set"){
								$.ajax({
									url: "<?php echo base_url(); ?>course/setCourse" ,
									type: "POST" ,
									data: {
										course_name: course_name,
										academic_year : academic_year,
										instructor : instructor,
										semester : semester,
									} ,
									dataType: "text" ,
									success: function(){
										datatable.draw();
										swal({
											title: "成功",
											text: "操作成功!",
											type: "success",
											confirmButtonText: "確定",
											animation: "slide-from-top"
										});
										$("#modal_page").modal("hide");
									}
								});
							} else if (fun=="update"){
								$.ajax({
									url: "<?php echo base_url(); ?>course/updateCourse" ,
									type: "POST" ,
									data: {
										course_id: edit,
										course_name: course_name,
										academic_year : academic_year,
										instructor : instructor,
										semester : semester,
									} ,
									dataType: "text" ,
									success: function(){
										datatable.draw();
										swal({
											title: "成功",
											text: "操作成功!",
											type: "success",
											confirmButtonText: "確定",
											animation: "slide-from-top"
										});
										$("#modal_page").modal("hide");
									}
								});
							}
						} else {
							swal({
								title: "取消",
								text: "您已取消刪除!",
								type: "error",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					}
					);
				} else {
					swal({
						title: "警告",
						text: "請檢查資料不可為空",
						type: "error",
						confirmButtonText: "確定",
						animation: "slide-from-top"
					});
				}
			});

			$(".datatable").on( "click", ".lockbtn", function (){
				lock = $(this).data("lock");
				swal({
					title: "您確定要隱藏/開啟課程嗎?",
					text: "您的操作將影響課程填寫，請謹慎選擇",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#ec4758",
					confirmButtonText: "變更",
					cancelButtonText: "取消",
					closeOnConfirm: false,
					closeOnCancel: false,
					animation: "slide-from-top"
				},
				function(isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: "<?php echo base_url(); ?>course/lockCourse" ,
							type: "POST" ,
							data: {
								course_id : lock
							} ,
							success: function(){
								datatable.draw();
								swal({
									title: "成功",
									text: "您已成功操作!",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						});
					} else {
						swal({
							title: "取消",
							text: "您已取消操作!",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				});
			});
		</script>
<?php $this->load->view("basic/end");?>
