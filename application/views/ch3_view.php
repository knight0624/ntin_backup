<?php $this->load->view("basic/begin");?>
	<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
	<!-- Data picker -->
	<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

	<!-- Data picker -->
	<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

	<!-- Input Mask-->
	<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
	<style>
	</style>
	<?php $this->load->view("basic/top")?>
			<div class="wrapper wrapper-content animated fadeInRight">
				<!-- 頁面內容開始 -->
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h2><?php echo $hw_name; ?> - 參、主要個案簡介</h2>
							</div>
							<div class="ibox-content">
								<?php
									// 1 學生
									// 2 老師
									// 3 admin
									switch ($permission) {
										case '1':
										case '3':
											echo
												'<div class="form-group">' .
													'<div class="text-danger">' . form_error("case_name") . '</div>'.
													'<div class="text-danger">' . form_error("case_sex") . '</div>'.
													'<div class="text-danger">' . form_error("case_age") . '</div>'.
													'<div class="text-danger">' . form_error("case_height") . '</div>'.
													'<div class="text-danger">' . form_error("case_weight") . '</div>'.
													'<div class="text-danger">' . form_error("case_occpational") . '</div>'.
													'<div class="text-danger">' . form_error("case_education") . '</div>'.
													'<div class="text-danger">' . form_error("case_marital_status") . '</div>'.
													'<div class="text-danger">' . form_error("case_religious_faith") . '</div>'.
													'<div class="text-danger">' . form_error("case_medication_history") . '</div>'.
													'<div class="text-danger">' . form_error("case_exercise_habits"). '</div>'.
													'<div class="text-danger">' . form_error("case_diet") . '</div>'.
													'<div class="text-danger">' . form_error("case_family_madical_history") . '</div>'.

													'<label class="col-sm-2 control-label"><h3>姓名：</h3></label>' .
													'<div class="col-sm-10"><input type="text" class="form-control" name="case_name" value="' . $case_data['case_name'] . '" required>' .
														'<div class="hr-line-dashed"></div>' .
													'</div>' .

													'<label class="col-sm-2 control-label"><h3>性別：</h3></label>' .
													'<div class="col-sm-10"><select class="form-control m-b" name="case_sex" value="' . $case_data['case_sex'] .'" required>' .
														'<option' . (($case_data["case_sex"]=="男性") ? " selected" :"") . '>男性</option>' .
														'<option' . (($case_data["case_sex"]=="女性") ? " selected" :"") . '>女性</option>' .
													'</select>' .
														'<div class="hr-line-dashed"></div>' .
													'</div>' .

													'<label class="col-sm-2 control-label"><h3>年齡：</h3></label>' .
													'<div class="col-sm-10"><input type="number" class="form-control" name="case_age" value="' . $case_data['case_age'] . '" required><div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>身高：</h3></label>' .
													'<div class="col-sm-10"><input type="number" class="form-control" placeholder="公分" name="case_height" value="' . $case_data['case_height'] . '" required><div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>體重：</h3></label>' .
													'<div class="col-sm-10"><input type="number" class="form-control" placeholder="公斤" name="case_weight" value="' . $case_data['case_weight'] . '" required><div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>職業：</h3></label>' .
													'<div class="col-sm-10"><input type="text" class="form-control" name="case_occpational" value="' . $case_data['case_occpational'] . '" required><div class="hr-line-dashed"></div></div>' .


													'<label class="col-sm-2 control-label"><h3>教育：</h3></label>' .
													'<div class="col-sm-10"><select class="form-control m-b" id="case_education" name="case_education" value="' . $case_data['case_education'] . '" required>' .
														'<option value="未就學"' . (($case_data["case_education"]=="未就學") ? " selected" : "") . '>未就學</option>' .
														'<option value="國小畢業"' . (($case_data["case_education"]=="國小畢業") ? " selected" : "") . '>國小畢業</option>' .
														'<option value="國中畢業"' . (($case_data["case_education"]=="國中畢業") ? " selected" : "") . '>國中畢業</option>' .
														'<option value="高中畢業"' . (($case_data["case_education"]=="高中畢業") ? " selected" : "") . '>高中畢業</option>' .
														'<option value="大學畢業"' . (($case_data["case_education"]=="大學畢業") ? " selected" : "") . '>大學畢業</option>' .
														'<option value="其他"' . (($case_data["case_education"]=="其他") ? " selected" : "") . '>其他</option>' ;
											if ($case_data["case_education"]!="未就學" && $case_data["case_education"]!="國小畢業" && $case_data["case_education"]!="國中畢業" && $case_data["case_education"]!="高中畢業" && $case_data["case_education"]!="大學畢業" && $case_data["case_education"]!="其他" ){
												echo '<option value="其他"' . (($case_data["case_education"]!="未就學" && $case_data["case_education"]!="國小畢業" && $case_data["case_education"]!="國中畢業" && $case_data["case_education"]!="高中畢業" && $case_data["case_education"]!="大學畢業" ) ? " selected" : "" ) . '>'.(($case_data["case_education"]!="") ? $case_data["case_education"] :"其他").'</option>' ;
											}
												echo
													'</select>' .
													'<input type="text" class="form-control" id="text_case_education" style="display:none;">'.
													'<button class="btn btn-primary" id="btn_case_education" style="display:none;">儲存</button>'.
													'<div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>婚姻狀況：</h3></label>' .
													'<div class="col-sm-10"><select class="form-control m-b" id="case_marital_status" name="case_marital_status" value="' . $case_data['case_marital_status'] . '" required>' .
														'<option value="未婚"' . (($case_data["case_marital_status"]=="未婚") ? " selected" :"" ) . '>未婚</option>' .
														'<option value="已婚"' . (($case_data["case_marital_status"]=="已婚") ? " selected" :"" ) . '>已婚</option>' .
														'<option value="離婚"' . (($case_data["case_marital_status"]=="離婚") ? " selected" :"" ) . '>離婚</option>' .
														'<option value="其他"' . (($case_data["case_marital_status"]=="其他") ? " selected" :"" ) . '>其他</option>' ;
											if ($case_data["case_marital_status"]!="未婚" && $case_data["case_marital_status"]!="已婚" && $case_data["case_marital_status"]!="離婚" && $case_data["case_marital_status"]!="其他"){
												echo
													'<option value="其他"' . (($case_data["case_marital_status"]!="未婚" && $case_data["case_marital_status"]!="已婚" && $case_data["case_marital_status"]!="離婚" && $case_data["case_marital_status"]!="其他") ? " selected" : "" ) . '>'.(($case_data["case_marital_status"]!="") ? $case_data["case_marital_status"] :"其他").'</option>' ;
											}
												echo
													'</select>' .
													'<input type="text" class="form-control" id="text_case_marital_status" style="display:none;">'.
													'<button class="btn btn-primary" id="btn_case_marital_status" style="display:none;">儲存</button>'.
													'<div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>宗教信仰：</h3></label>'.
													'<div class="col-sm-10"><select class="form-control m-b" id="case_religious_faith" name="case_religious_faith" value="' . $case_data['case_religious_faith'] . '" required>'.
														'<option value="佛教"' . (($case_data["case_religious_faith"]=="佛教") ? " selected" : "") . '>佛教</option>' .
														'<option value="道教"' . (($case_data["case_religious_faith"]=="道教") ? " selected" : "") . '>道教</option>' .
														'<option value="天主教"' . (($case_data["case_religious_faith"]=="天主教") ? " selected" : "") . '>天主教</option>' .
														'<option value="基督教"' . (($case_data["case_religious_faith"]=="基督教") ? " selected" : "") . '>基督教</option>' .
														'<option value="回教"' . (($case_data["case_religious_faith"]=="回教") ? " selected" : "") . '>回教</option>' .
														'<option value="其他"' . (($case_data["case_religious_faith"]=="其他") ? " selected" : "") . '>其他</option>' ;
											if($case_data["case_religious_faith"]!="佛教" && $case_data["case_religious_faith"]!="道教" && $case_data["case_religious_faith"]!="天主教" && $case_data["case_religious_faith"]!="基督教" && $case_data["case_religious_faith"]!="回教"){
												echo
													'<option value="其他"' . (($case_data["case_religious_faith"]!="佛教" && $case_data["case_religious_faith"]!="道教" && $case_data["case_religious_faith"]!="天主教" && $case_data["case_religious_faith"]!="基督教" && $case_data["case_religious_faith"]!="回教") ? " selected" : "" ) . '>'.(($case_data["case_religious_faith"]!="") ? $case_data["case_religious_faith"] :"其他").'</option>' ;
											}
												echo
													'</select>' .
													'<input type="text" class="form-control" id="text_case_religious_faith" style="display:none;">'.
													'<button class="btn btn-primary" id="btn_case_religious_faith" style="display:none;">儲存</button>'.
													'<div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>疾病用藥史：</h3></label>' .
													'<div class="col-sm-10"><textarea rows="20" class="form-control" name="case_medication_history" value="' . '" required>'. $case_data['case_medication_history'] .'</textarea><div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>運動習慣：</h3></label>' .
													'<div class="col-sm-10"><textarea rows="20" class="form-control" name="case_exercise_habits" value="' . '" required>' . $case_data['case_exercise_habits'] . '</textarea><div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>飲食習慣：</h3></label>' .
													'<div class="col-sm-10"><textarea rows="20" class="form-control" name="case_diet" value="' . '" required>' . $case_data['case_diet'] . '</textarea><div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>家族病史：</h3></label>' .
													'<div class="col-sm-10"><textarea rows="20" class="form-control" name="case_family_madical_history" value="' . '" required>' . $case_data['case_family_madical_history'] . '</textarea><div class="hr-line-dashed"></div></div>' .

													'<label class="col-sm-2 control-label"><h3>其他：</h3></label>' .
													'<div class="col-sm-10"><textarea rows="20" class="form-control" name="case_other" value="' . '">' . $case_data['case_other'] . '</textarea><div class="hr-line-dashed"></div></div>' .
													'<div class="col-sm-2"></div>' .
													'<div class="col-sm-10"><button class="btn btn-primary" id="save_case" type="submit" >儲存</button></div>' .
											'</div>' ;
											break;
										case '2':
										case '6':
											echo
												'<div class="form-group">' .
														'<div class="text-danger">' . form_error("case_name") . '</div>'.
														'<div class="text-danger">' . form_error("case_sex") . '</div>'.
														'<div class="text-danger">' . form_error("case_age") . '</div>'.
														'<div class="text-danger">' . form_error("case_height") . '</div>'.
														'<div class="text-danger">' . form_error("case_weight") . '</div>'.
														'<div class="text-danger">' . form_error("case_occpational") . '</div>'.
														'<div class="text-danger">' . form_error("case_education") . '</div>'.
														'<div class="text-danger">' . form_error("case_marital_status") . '</div>'.
														'<div class="text-danger">' . form_error("case_religious_faith") . '</div>'.
														'<div class="text-danger">' . form_error("case_medication_history") . '</div>'.
														'<div class="text-danger">' . form_error("case_exercise_habits"). '</div>'.
														'<div class="text-danger">' . form_error("case_diet") . '</div>'.
														'<div class="text-danger">' . form_error("case_family_madical_history") . '</div>'.

														'<label class="col-sm-2 control-label"><h3>姓名：</h3></label>' .
														'<div class="col-sm-10"><p type="text" class="form-control" name="case_name">' . $case_data['case_name'] . '</p>' .
															'<div class="hr-line-dashed"></div>' .
														'</div>' .

														'<label class="col-sm-2 control-label"><h3>性別：</h3></label>' .
														'<div class="col-sm-10"><p class="form-control m-b" name="case_sex">' . $case_data['case_sex'] .
														'</p>' .
															'<div class="hr-line-dashed"></div>' .
														'</div>' .

														'<label class="col-sm-2 control-label"><h3>年齡：</h3></label>' .
														'<div class="col-sm-10"><p type="number" class="form-control" name="case_age">'.$case_data['case_age'].'</p>'.'<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>身高：</h3></label>' .
														'<div class="col-sm-10"><p type="number" class="form-control" placeholder="公分" name="case_height">' . $case_data['case_height'] . '</p>'. '<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>體重：</h3></label>' .
														'<div class="col-sm-10"><p type="number" class="form-control" placeholder="公斤" name="case_weight">' . $case_data['case_weight'] . '</p>' . '<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>職業：</h3></label>' .
														'<div class="col-sm-10"><p type="text" class="form-control" name="case_occpational">' . $case_data['case_occpational'] . '</p>' . '<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>教育：</h3></label>' .
														'<div class="col-sm-10"><p class="form-control m-b" name="case_education">' . $case_data['case_education'] . '</p>' .
														'<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>婚姻狀況：</h3></label>' .
														'<div class="col-sm-10"><p class="form-control m-b" name="case_marital_status">' . $case_data['case_marital_status'] . '</p>' .
														'<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>宗教信仰：</h3></label>'.
														'<div class="col-sm-10"><p class="form-control m-b" name="case_religious_faith">'. $case_data['case_religious_faith'] . '</p>' .
														'<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>疾病用藥史：</h3></label>' .
														'<div class="col-sm-10"><p type="text" class="form-control" name="case_medication_history">' . $case_data['case_medication_history'] . '</p>' . '<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>運動習慣：</h3></label>' .
														'<div class="col-sm-10"><p type="text" class="form-control" name="case_exercise_habits">' . $case_data['case_exercise_habits'] . '</p>' . '<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>飲食習慣：</h3></label>' .
														'<div class="col-sm-10"><p type="text" class="form-control" name="case_diet">' . $case_data['case_diet'] . '</p>' . '<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>家族病史：</h3></label>' .
														'<div class="col-sm-10"><p type="text" class="form-control" name="case_family_madical_history" >' . $case_data['case_family_madical_history'] . '</p>' . '<div class="hr-line-dashed"></div></div>' .

														'<label class="col-sm-2 control-label"><h3>其他：</h3></label>' .
														'<div class="col-sm-10"><p type="text" class="form-control" name="case_other" >' . $case_data['case_other'] . '</p>' .'<div class="hr-line-dashed"></div></div>' .
														'<div class="col-sm-2"></div>' .
												'</div>' ;
											break;
									}
								?>
								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch2");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch4");?>" class="btn btn-default">下一步</a>
								</div>
							</div>
						</div>
						<?php
							// 1 學生
							// 2 老師
							// 3 admin
							switch ($permission) {
								case '1':
								case '6':
									echo
										'<div class="ibox float-e-margins">' .
											'<div class="ibox-title">' .
												'<h2>老師評語</h2>' .
											'</div>' .
											'<div class="ibox-content">' .
												'<div class="form-horizontal">' .
													'<p class="text-danger form-control">'.$case_data['comment'].'</p>' .
												'</div>' .
											'</div>' .
										'</div>' ;
									break;
								case '2':
								case '3':
									echo
										'<div class="ibox float-e-margins">' .
											'<div class="ibox-title">' .
												'<h2>老師評語</h2>' .
											'</div>' .
											'<div class="ibox-content">' .
												'<div class="form-horizontal">' .
													'<textarea class="form-control" name="comment" rows="10" placeholder="老師評語">'.$case_data['comment'].'</textarea>' .
													'<div class="form-group">' .
														'<div class="col-sm-10"><br>' .
															'<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>' .
														'</div>' .
													'</div>' .
												'</div>' .
											'</div>' .
										'</div>' ;
									break;
							}
						?>
					</div>
				</div>


				<!-- 頁面內容結束 -->
			</div>
	<?php $this->load->view("basic/bottom")?>
	<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Data picker -->
	<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

	<!-- Chosen -->
	<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

	<!-- Input Mask-->
	<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

	<script>
		if(document.querySelector("#case_education")){
			document.querySelector("#case_education").onchange = function(){
				if(this.value == "其他"){
					document.querySelector("#text_case_education").removeAttribute("style");
					document.querySelector("#btn_case_education").removeAttribute("style");
				}else{
					document.querySelector("#text_case_education").setAttribute("style", "display:none;");
					document.querySelector("#btn_case_education").setAttribute("style", "display:none;");
				}
			}
		}

		if(document.querySelector("#case_marital_status")){
			document.querySelector("#case_marital_status").onchange = function(){
				if(this.value == "其他"){
					document.querySelector("#text_case_marital_status").removeAttribute("style");
					document.querySelector("#btn_case_marital_status").removeAttribute("style");
				}else{
					document.querySelector("#text_case_marital_status").setAttribute("style", "display:none;");
					document.querySelector("#btn_case_marital_status").setAttribute("style", "display:none;");
				}
			}
		}

		if(document.querySelector("#case_religious_faith")){
			document.querySelector("#case_religious_faith").onchange = function(){
				if(this.value == "其他"){
					document.querySelector("#text_case_religious_faith").removeAttribute("style");
					document.querySelector("#btn_case_religious_faith").removeAttribute("style");
				}else{
					document.querySelector("#text_case_religious_faith").setAttribute("style", "display:none;");
					document.querySelector("#btn_case_religious_faith").setAttribute("style", "display:none;");
				}
			}
		}

		if(document.querySelector("#btn_case_education")){
			document.querySelector("#btn_case_education").onclick = function(){
				var case_education = document.querySelector("#text_case_education").value;
				$.ajax({
					url: "<?php echo base_url(); ?>Ch3/editOthers/case_education" ,
					type: "POST" ,
					data: {
						case_education : case_education
					} ,
					dataType: "text" ,
					success: function(data){
						swal({
							title: "成功",
							text: "已儲存",
							type: "success",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				});
			}
		}

		if(document.querySelector("#btn_case_marital_status")){
			document.querySelector("#btn_case_marital_status").onclick = function(){
				var case_marital_status = document.querySelector("#text_case_marital_status").value;
				$.ajax({
					url: "<?php echo base_url(); ?>Ch3/editOthers/case_marital_status" ,
					type: "POST" ,
					data: {
						case_marital_status : case_marital_status
					} ,
					dataType: "text" ,
					success: function(data){
						swal({
							title: "成功",
							text: "已儲存",
							type: "success",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				});
			}
		}

		if(document.querySelector("#btn_case_religious_faith")){
			document.querySelector("#btn_case_religious_faith").onclick = function(){
				var case_religious_faith = document.querySelector("#text_case_religious_faith").value;
				$.ajax({
					url: "<?php echo base_url(); ?>Ch3/editOthers/case_religious_faith" ,
					type: "POST" ,
					data: {
						case_religious_faith : case_religious_faith
					} ,
					dataType: "text" ,
					success: function(data){
						swal({
							title: "成功",
							text: "已儲存",
							type: "success",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				});
			}
		}

		// datepicker
		$("#data_1 .input-group.date").datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			autoclose: true
		});

		$("#data_2 .input-group.date").datepicker({
			startView: 1,
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			autoclose: true,
			format: "dd/mm/yyyy"
		});

		$("#data_3 .input-group.date").datepicker({
			startView: 2,
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			autoclose: true
		});

		$("#data_4 .input-group.date").datepicker({
			minViewMode: 1,
			keyboardNavigation: false,
			forceParse: false,
			autoclose: true,
			todayHighlight: true
		});

		$("#data_5 .input-daterange").datepicker({
			keyboardNavigation: false,
			forceParse: false,
			autoclose: true
		});

		// chosen
		var config = {
			".chosen-select"		   : {},
			".chosen-select-deselect"  : {allow_single_deselect:true},
			".chosen-select-no-single" : {disable_search_threshold:10},
			".chosen-select-no-results": {no_results_text:"Oops, nothing found!"},
			".chosen-select-width"	 : {width:"95%"}
		}
		for (var selector in config) {
			$(selector).chosen(config[selector]);
		}

		if(document.querySelector("#save_comment")){
			document.querySelector("#save_comment").onclick = function(){
				var comment = document.getElementsByName("comment")[0].value;
				$.ajax({
					url: "<?php echo base_url(); ?>Ch3/addComment" ,
					type: "POST" ,
					data: {
						comment : comment
					} ,
					dataType: "text" ,
					success: function(data){
						swal({
							title: "成功",
							text: "已儲存",
							type: "success",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});

					}
				});
			}
		}

		if(document.querySelector("#save_case")){
			document.querySelector("#save_case").onclick = function(){
				var case_name = document.getElementsByName("case_name")[0].value;
				var case_sex = document.getElementsByName("case_sex")[0].value;
				var case_age = document.getElementsByName("case_age")[0].value;
				var case_height = document.getElementsByName("case_height")[0].value;
				var case_weight = document.getElementsByName("case_weight")[0].value;
				var case_occpational = document.getElementsByName("case_occpational")[0].value;
				var case_education = document.getElementsByName("case_education")[0].value;
				var case_marital_status = document.getElementsByName("case_marital_status")[0].value;
				var case_religious_faith = document.getElementsByName("case_religious_faith")[0].value;
				var case_medication_history = document.getElementsByName("case_medication_history")[0].value;
				var case_exercise_habits = document.getElementsByName("case_exercise_habits")[0].value;
				var case_diet = document.getElementsByName("case_diet")[0].value;
				var case_family_madical_history = document.getElementsByName("case_family_madical_history")[0].value;
				var case_other = document.getElementsByName("case_other")[0].value;

				$.ajax({
					url: "<?php echo base_url(); ?>Ch3/editCaseProfile" ,
					type: "POST" ,
					data: {
						case_name:case_name,
						case_sex:case_sex,
						case_age:case_age,
						case_height:case_height,
						case_weight:case_weight,
						case_occpational:case_occpational,
						case_education:case_education,
						case_marital_status:case_marital_status,
						case_religious_faith:case_religious_faith,
						case_medication_history:case_medication_history,
						case_exercise_habits:case_exercise_habits,
						case_diet:case_diet,
						case_family_madical_history:case_family_madical_history,
						case_other:case_other
					} ,
					dataType: "text" ,
					success: function(data){
						swal({
							title: "成功",
							text: "已儲存",
							type: "success",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				});
			}
		}

	</script>
<?php $this->load->view("basic/end");?>
