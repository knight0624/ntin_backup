<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
			.showSweetAlert[data-animation=slide-from-top]{
				animation: slideFromTop 0.5s;
			}
			.btn-group{
				margin-top: 20px;
			}
			.upload-group{
				margin: 0px auto;
				text-align: center;
			}
			.upload-group div{
				display: inline-block;
				vertical-align: top;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 伍、家庭結構 </h2>
								</div>
								<div class="ibox-content">
									<h3>一、溝通</h3>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													"<textarea class='form-control' name='communication' rows='20'>".$DB_all['fstructure_communication']."</textarea>" .
													"<div align='left'>" .
														"<button type='submit' class='btn btn-primary' id='save_Communication'>儲存</button>" .
													"</div>";
												break;
											case '2':
											case '6':
												echo
													"<p class='form-control' name='communication' rows='4'>".$DB_all['fstructure_communication']."</p>";
												break;
										}
									?>
									<img style="display:block; margin:auto; max-width:100%;" src=
										<?php
											if (! empty($upload['img1_name']))
												echo base_url()."uploads/img/".$upload['img1_name'];
											else
												echo base_url()."dist/img/圖1家庭溝通過程.jpg";
										?>
									>
									<p class="text-center">圖1家庭溝通過程</p>
									<div class="upload-group">
										<!-- 下載範例 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															'<a href="'.base_url().'download/範例1.pptx" download>' .
																'<img src="'.base_url().'dist/img/download_example.png" style="width:150px;">' .
															'</a>';
														break;
												}
											?>
										</div>
										<!-- 上傳PPT按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch5/do_upload_ppt') .
															'<input type="file" name="userfile" id="ppt1_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_ppt.png" id="ppt1_file_new" value="選擇PPT檔" style="width:150px;">' .
															'<input type="submit" id="ppt1_upload" style="display:none;" value="1" name="number">' .
															'<img src="'.base_url().'dist/img/upload_ppt.png" id="ppt1_upload_new" value="上傳PPT" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['ppt1_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/ppt/".$upload['ppt1_name']."' download style='margin:auto;'>下載：".$upload['ppt1_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(0)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												?>
											</div>
										</div>
										<!-- 上傳圖片按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch5/do_upload_img') .
															'<input type="file" name="userfile" id="img1_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_img.png" id="img1_file_new" value="選擇圖片檔" style="width:150px;">' .
															'<input type="submit" id="img1_upload" style="display:none;" value="1" name="number">' .
															'<img src="'.base_url().'dist/img/upload_img.png" id="img1_upload_new" value="上傳圖片" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['img1_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/img/".$upload['img1_name']."' download style='margin:auto;'>下載：".$upload['img1_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(1)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												?>
											</div>
										</div>
									</div>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<div align="center">' .
														'<p class="text-danger">檔案大小限制為8MB</p>' .
													'</div>';
												break;
										}
									?>
								</div>
								<div class="ibox-content">
									<h3>二、權力結構</h3>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<textarea class="form-control" name="power_structure" rows="20">'.$DB_all['fstructure_power_structure'].'</textarea>' .
													'<div align="left">' .
														'<button type="submit" class="btn btn-primary" id="save_Power_Structure">儲存</button>' .
													'</div>';
												break;
											case '2':
											case '6':
												echo
													'<p class="form-control" name="power_structure" rows="4">'.$DB_all['fstructure_power_structure'].'</p>';
												break;
										}
									?>
									<img style="display:block; margin:auto; max-width:100%;" src=
										<?php
											if (! empty($upload['img2_name']))
												echo base_url()."uploads/img/".$upload['img2_name'];
											else
												echo base_url()."dist/img/圖2家庭權力連續線.jpg";
										?>
									>
									<p class="text-center">圖2家庭權力連續線</p>
									<div class="upload-group">
										<!-- 下載範例 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															'<a href="'.base_url().'download/範例2.pptx" download>' .
																'<img src="'.base_url().'dist/img/download_example.png" style="width:150px;">' .
															'</a>';
														break;
												}
											?>
										</div>
										<!-- 上傳PPT按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch5/do_upload_ppt') .
															'<input type="file" name="userfile" id="ppt2_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_ppt.png" id="ppt2_file_new" value="選擇PPT檔" style="width:150px;">' .
															'<input type="submit" id="ppt2_upload" style="display:none;" value="2" name="number">' .
															'<img src="'.base_url().'dist/img/upload_ppt.png" id="ppt2_upload_new" value="上傳PPT" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['ppt2_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/ppt/".$upload['ppt2_name']."' download style='margin:auto;'>下載：".$upload['ppt2_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(2)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												?>
											</div>
										</div>
										<!-- 上傳圖片按鈕 -->
										<div>
											<?php
												// 1 學生
												// 2 老師
												// 3 admin
												switch ($permission) {
													case "1":
													case "3":
														echo
															form_open_multipart('Ch5/do_upload_img') .
															'<input type="file" name="userfile" id="img2_file" style="display:none;">' .
															'<img src="'.base_url().'dist/img/select_img.png" id="img2_file_new" value="選擇圖片檔" style="width:150px;">' .
															'<input type="submit" id="img2_upload" style="display:none;" value="2" name="number">' .
															'<img src="'.base_url().'dist/img/upload_img.png" id="img2_upload_new" value="上傳圖片" style="width:150px;">' .
															'</form>';
														break;
												}
											?>
											<div class="btn-group">
												<?php
													if ( ! empty($upload['img2_name'])) {
														echo
															"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/img/".$upload['img2_name']."' download style='margin:auto;'>下載：".$upload['img2_name']."</a>";
														// 1 學生
														// 2 老師
														// 3 admin
														switch ($permission) {
															case "1":
															case "3":
																echo
																	'<a class="btn btn-danger btn-xs" onclick="fileDelete(3)">' .
																		'<i class="fa fa-trash"></i>' .
																	'</a>';
																break;
														}
													}
												?>
											</div>
										</div>
									</div>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<div align="center">' .
														'<p class="text-danger">檔案大小限制為8MB</p>' .
													'</div>';
												break;
										}
									?>
								</div>
								<div class="ibox-content">
									<h3>三、角色結構</h3>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<textarea class="form-control" name="role_structure" rows="20">'.$DB_all['fstructure_role_structure'].'</textarea>' .
													'<div align="left">' .
														'<button type="submit" class="btn btn-primary" id="save_Role_Structure">儲存</button>' .
													'</div>';
												break;
											case '2':
											case '6':
												echo
													'<p class="form-control" name="role_structure" rows="4">'.$DB_all['fstructure_role_structure'].'</p>';
												break;
										}
									?>
								</div>
								<div class="ibox-content">
									<h3>四、價值觀</h3>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<textarea class="form-control" name="values" rows="20">'.$DB_all['fstructure_values'].'</textarea>' .
													'<div align="left">' .
														'<button type="submit" class="btn btn-primary" id="save_Values">儲存</button>' .
													'</div>';
												break;
											case '2':
											case '6':
												echo
													'<p class="form-control" name="role_structure" rows="4">'.$DB_all['fstructure_values'].'</p>';
												break;
										}
									?>
								</div>
								<div class="ibox-content">
									<h3>五、補充</h3>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<textarea class="form-control" name="supple_ment" rows="20">'.$DB_all['fstructure_supplement'].'</textarea>' .
													'<div align="left">' .
														'<button type="submit" class="btn btn-primary" id="save_Supple_Ment">儲存</button>' .
													'</div>';
												break;
											case '2':
											case '6':
												echo
													'<p class="form-control" name="supple_ment" rows="4">'.$DB_all['fstructure_supplement'].'</p>';
												break;
										}
									?>
								</div>




								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch4");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch6");?>" class="btn btn-default">下一步</a>
								</div>
							</div>
							<?php
								// 1 學生
								// 2 老師
								// 3 admin
								switch ($permission) {
									case '1':
									case '6':
										echo
											'<div class="ibox float-e-margins">' .
												'<div class="ibox-title">' .
													'<h2>老師評語</h2>' .
												'</div>' .
												'<div class="ibox-content">' .
													'<div class="form-horizontal">' .
														'<p class="text-danger form-control">'.$DB_all["comment"].'</p>' .
													'</div>' .
												'</div>' .
											'</div>' ;
										break;
									case '2':
									case '3':
										echo
											'<div class="ibox float-e-margins">' .
												'<div class="ibox-title">' .
													'<h2>老師評語</h2>' .
												'</div>' .
												'<div class="ibox-content">' .
													'<div class="form-horizontal">' .
														'<textarea class="form-control" name="comment" rows="10" placeholder="老師評語">'.$DB_all["comment"].'</textarea>' .
														'<div class="form-group">' .
															'<div class="col-sm-10"><br>' .
																'<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>' .
															'</div>' .
														'</div>' .
													'</div>' .
												'</div>' .
											'</div>';
										break;
								}
							?>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'           : {},
				'.chosen-select-deselect'  : {allow_single_deselect:true},
				'.chosen-select-no-single' : {disable_search_threshold:10},
				'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}

			if (document.getElementById("ppt1_file_new")) {
				document.getElementById("ppt1_file_new").onclick = function(){
					document.getElementById("ppt1_file").click(true);
				}
			}
			if (document.getElementById("ppt1_upload_new")) {
				document.getElementById("ppt1_upload_new").onclick = function(){
					document.getElementById("ppt1_upload").click(true);
				}
			}
			if (document.getElementById("img1_file_new")) {
				document.getElementById("img1_file_new").onclick = function(){
					document.getElementById("img1_file").click(true);
				}
			}
			if (document.getElementById("img1_upload_new")) {
				document.getElementById("img1_upload_new").onclick = function(){
					document.getElementById("img1_upload").click(true);
				}
			}

			if (document.getElementById("ppt2_file_new")) {
				document.getElementById("ppt2_file_new").onclick = function(){
					document.getElementById("ppt2_file").click(true);
				}
			}
			if (document.getElementById("ppt2_upload_new")) {
				document.getElementById("ppt2_upload_new").onclick = function(){
					document.getElementById("ppt2_upload").click(true);
				}
			}
			if (document.getElementById("img2_file_new")) {
				document.getElementById("img2_file_new").onclick = function(){
					document.getElementById("img2_file").click(true);
				}
			}
			if (document.getElementById("img2_upload_new")) {
				document.getElementById("img2_upload_new").onclick = function(){
					document.getElementById("img2_upload").click(true);
				}
			}

			if (document.querySelector("#save_Role_Structure")) {
				document.querySelector("#save_Role_Structure").onclick = function(){
					var role_structure = document.getElementsByName("role_structure")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch5/fstructureRoleStructure" ,
						type: "POST" ,
						data: {
							role_structure_value : role_structure
						} ,
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
			if (document.querySelector("#save_Power_Structure")) {
				document.querySelector("#save_Power_Structure").onclick = function(){
					var power_structure = document.getElementsByName("power_structure")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch5/fstructurePowerStructure" ,
						type: "POST" ,
						data: {
							power_structure_value : power_structure
						} ,
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
			if (document.querySelector("#save_Communication")) {
				document.querySelector("#save_Communication").onclick = function(){
					var communication = document.getElementsByName("communication")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch5/fstructureCommunication" ,
						type: "POST" ,
						data: {
							communication_value : communication
						} ,
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
			if (document.querySelector("#save_Values")) {
				document.querySelector("#save_Values").onclick = function(){
					var values = document.getElementsByName("values")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch5/fstructureValues" ,
						type: "POST" ,
						data: {
							values_value : values
						} ,
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
			if (document.querySelector("#save_Supple_Ment")) {
				document.querySelector("#save_Supple_Ment").onclick = function(){
					var supple_ment = document.getElementsByName("supple_ment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch5/fstructureSuppleMent" ,
						type: "POST" ,
						data: {
							supple_ment_value : supple_ment
						} ,
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
			function fileDelete(d){
				if (d==0) {
					$uploadName = "<?php echo $upload['ppt1_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch5/delete_Ptt"
					$filenumber = 1;
				}else if (d==1) {
					$uploadName = "<?php echo $upload['img1_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch5/delete_Img"
					$filenumber = 1;
				}else if (d==2) {
					$uploadName = "<?php echo $upload['ppt2_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch5/delete_Ptt"
					$filenumber = 2;
				}else if (d==3) {
					$uploadName = "<?php echo $upload['img2_name'] ?>";
					$uploadUrl = "<?php echo base_url(); ?>Ch5/delete_Img"
					$filenumber = 2;
				}
				swal({	title: "刪除文件?",
						text: "按下刪除後文件不可復原",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "刪除!",
						cancelButtonText: "取消",
						closeOnConfirm: false,
						closeOnCancel: false },
						function(isConfirm){
							if (isConfirm) {
								$.ajax({
									url: $uploadUrl,
									type: "POST",
									data: {uploadName:$uploadName, filenumber:$filenumber},
									dataType: "text" ,
									success: function(data){
										if (data) {
											location.reload();
										}
										else{
											swal("錯誤!", "刪除時發生錯誤!", "error");
										}
									}
								});
							}
							else {
								swal("已取消!", "您的文件是安全的", "error");
							}
						}
				);
			}
			if (document.querySelector("#save_comment")) {
				document.querySelector("#save_comment").onclick = function(){
					var comment = document.getElementsByName("comment")[0].value;
					console.log(comment);
					$.ajax({
						url: "<?php echo base_url(); ?>Ch5/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data){
							swal({
								title: "成功",
								text: "已儲存",
								type: "success",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
