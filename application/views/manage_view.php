<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/dataTables/datatables.min.css">
		<style>
			.btn-xs{
				margin-left: 5px;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="ibox">
						<div class="ibox-title">
							<h5>使用者列表</h5>
						</div>
						<div class="ibox-content">
							<div style="height:48px;">
								<a class="btn btn-primary" href="<?php echo base_url(); ?>manage/insert">新增使用者</a>
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover datatable" style="width:100%;">
									<thead>
										<tr>
											<th></th>
											<th>帳號</th>
											<th>姓名</th>
											<th>信箱</th>
											<th>權限</th>
											<th>狀態</th>
											<th>入學年</th>
											<th>班級</th>
										</tr>
									</thead>

									<tfoot>
										<tr>
											<th></th>
											<th>帳號</th>
											<th>姓名</th>
											<th>信箱</th>
											<th>權限</th>
											<th>狀態</th>
											<th>入學年</th>
											<th>班級</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/dataTables/datatables.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src=""></script>
		<script>
			var datatable = $(".datatable").DataTable({
				"language":{
					"sEmptyTable":        "沒有搜尋到結果",
					"sInfo":              "顯示第 _START_ 至 _END_ 項結果 , 共 _TOTAL_ 項",
					"sInfoEmpty":         "顯示第 0 至 0 項結果 , 共 0 項",
					"sInfoFiltered":      "（由 _MAX_ 項結果過濾）",
					"sInfoPostFix":       "",
					"sInfoThousands":     ",",
					"sLengthMenu":        "顯示 _MENU_ 項結果",
					"sLoadingRecords":   "載入中...",
					"sProcessing":       "<img src='<?php echo base_url(); ?>dist/img/loader.gif'>",
					"sSearch":            "搜尋:",
					"sZeroRecords":       "沒有搜尋結果",
					"oPaginate": {
						"sFirst":           "首頁",
						"sPrevious":        "上頁",
						"sNext":            "下頁",
						"sLast":            "末頁"
					},
					"oAria": {
						"sSortAscending":  ":以遞增排列此列",
						"sSortDescending": ":以遞減排列此列"
					}
				},
				"processing": true,
				"serverSide": true,
				"ajax": "<?php echo base_url(); ?>manage/getUser",
				"serverMethod": "POST",
				"sorting": [[1, "desc"]],
				"columnDefs": [
					{"width": "110px", "targets": [0]},
					{"orderable": false, "targets": [0]},
					{ "bSearchable": false, "targets": [0]}
				],
				"columns": [
					{ "data": "edit_btn" },
					{ "data": "account" },
					{ "data": "name" },
					{ "data": "email" },
					{ "data": "opt_permission_name" },
					{ "data": "status_name" },
					{ "data": "entry" },
					{ "data": "class" }
				]
			});

			$(".datatable").on( "click", ".chg_status", function (){
				var acc = $(this).data("acc");
				$.ajax({
					url: "<?php echo base_url(); ?>manage/updateStatus" ,
					type: "POST" ,
					data: {
						acc : acc
					} ,
					dataType: "text" ,
					success: function(){
						datatable.draw();
					}
				});
			});
		</script>
<?php $this->load->view("basic/end");?>
