<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/dataTables/datatables.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<style>
			.modal {
				text-align: center;
				padding: 0!important;
			}

			.modal:before {
				content: '';
				display: inline-block;
				height: 100%;
				vertical-align: middle;
				margin-right: -4px;
			}

			.modal-dialog {
				display: inline-block;
				text-align: left;
				vertical-align: middle;
				width: 40%;
			}

			.modal-body {
				padding: 15px;
			}

			.showSweetAlert[data-animation=slide-from-top]{
				animation: slideFromTop 0.5s;
			}

			.file-zoom-fullscreen .modal-dialog{
				position: static;
			}

			.btn-grp select {
				width: 170px;
				height: 34px;
				text-align-last: center;
			}

			.vertical-align {
				display: flex;
				align-items: center;
			}

			.btn-grp {
				margin-bottom: 13px;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="ibox">
						<div class="ibox-title">
							<h3>作業批改 - <?php echo $course_name." - ".$hw_name; ?></h3>
						</div>
						<div class="ibox-content">
							<div style="height:48px;">
								<button type="button" class="btn btn-warning" onclick="javascript:history.back()">回上一頁</button>
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover datatable" style="width:100%;">
									<thead>
										<tr>
											<th>操作項目</th>
											<th>組別名稱</th>
										</tr>
									</thead>

									<tfoot>
										<tr>
											<th>操作項目</th>
											<th>組別名稱</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>

					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/dataTables/datatables.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>
		<script>
			var datatable = $(".datatable").DataTable({
				"language":{
					"sEmptyTable":        "沒有搜尋到結果",
					"sInfo":              "顯示第 _START_ 至 _END_ 項結果 , 共 _TOTAL_ 項",
					"sInfoEmpty":         "顯示第 0 至 0 項結果 , 共 0 項",
					"sInfoFiltered":      "（由 _MAX_ 項結果過濾）",
					"sInfoPostFix":       "",
					"sInfoThousands":     ",",
					"sLengthMenu":        "顯示 _MENU_ 項結果",
					"sLoadingRecords":   "載入中...",
					"sProcessing":       "<img src='<?php echo base_url(); ?>dist/img/loader.gif'>",
					"sSearch":            "搜尋:",
					"sZeroRecords":       "沒有搜尋結果",
					"oPaginate": {
						"sFirst":           "首頁",
						"sPrevious":        "上頁",
						"sNext":            "下頁",
						"sLast":            "末頁"
					},
					"oAria": {
						"sSortAscending":  ":以遞增排列此列",
						"sSortDescending": ":以遞減排列此列"
					}
				},
				"processing": true,
				"serverSide": true,
				"scrollX": true,
				"ajax": "<?php echo base_url(); ?>mark/getTeam/<?php echo $_SESSION["hw_id"]; ?>",
				"serverMethod": "POST",
				"sorting": [[1, "desc"]],
				"columnDefs": [
					{"width": "120px", "targets": [0] },
					{"orderable": false, "targets": [0] },
					{"bSearchable": false, "targets": [0] }
				],
				"columns": [
					{ "data": "edit_btn" },
					{ "data": "name" }
				]
			});
	
		</script>
<?php $this->load->view("basic/end");?>
