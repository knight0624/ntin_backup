<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 玖、家庭壓力</h2>
								</div>
								<div class="ibox-content">
									<h3>
										一、生活改變事件評值表　(Life Change Units; LCU)
									</h3>
									<p class="p-style">
										　　家庭壓力來自發展上的壓力及生活的壓力事件，最廣為應用的壓力評估工具為Holmes、Rahe、Masudau 等人於1972提出的生活改變事件評值表(Life Change Units; LCU)。
									</p>
									<br>
									<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '<table class="table table-bordered" border="1">'.
														'<thead>'.
															'<tr>'.
																'<th class="col-md-2"></th>'.
																'<th class="col-md-5 text-center">生活事件</th>'.
																'<th class="col-md-5 text-center">事件</th>'.
															'</tr>'.
														'</thead>'.
														'<tbody>'.
															'<tr>'.
																'<th class="text-center">家庭方面：</th>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="100"> 配偶死亡'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="73"> 離婚'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="65"> 分居'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="63"> 親人死亡'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="50"> 結婚'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="44"> 家人生病'.
																	'</label>'.
																'</td>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="40"> 懷孕'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="39"> 新家人的加入'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="35"> 與配偶爭吵'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="29"> 兒女離家'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="26"> 配偶開始工作或退休'.
																	'</label>'.
																'</td>'.
															'</tr>'.
															'<tr>'.
																'<th class="text-center">個人方面：</th>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="63"> 入獄'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="53"> 受傷或生病'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="39"> 性障礙'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="37"> 好友死亡'.
																	'</label>'.
																'</td>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="24"> 個人習慣有重大改變'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="20"> 轉學'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="20"> 搬家'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="13"> 渡假'.
																	'</label>'.
																'</td>'.
															'</tr>'.
															'<tr>'.
																'<th class="text-center">工作方面：</th>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="47"> 解職'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="45"> 退休'.
																	'</label>'.
																'</td>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="34"> 工作職務改變'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="34"> 工作情境有重大改變'.
																	'</label>'.
																'</td>'.
															'</tr>'.
															'<tr>'.
																'<th class="text-center">財務方面：</th>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="38"> 財務狀況有重大改變'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="31"> 抵押借款超過10,000美元'.
																	'</label>'.
																'</td>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="30"> 抵押借款被拒'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="17"> 抵押借款少於10,000美元'.
																	'</label>'.
																'</td>'.
															'</tr>'.
															'<tr>'.
																'<th class="text-center">結果：</th>'.
																	'<td colspan=2>'.
																		'<p class="text-center" id="LCU_result"></p>'.
																	'</td>'.
															'</tr>'.
														'</tbody>'.
													'</table>'.
													'<div class="form-group">'.
														'<div class="col-sm-2 ">'.
															'<button class="btn btn-primary" id="save_LCU" >儲存</button>'.
														'</div>'.
													'</div>';
											break;

											case "2":
											case "6":
												echo '<table class="table table-bordered" border="1">'.
														'<thead>'.
															'<tr>'.
																'<th class="col-md-2 control-label"></th>'.
																'<th class="text-center">生活事件</th>'.
																'<th class="text-center">事件</th>'.
															'</tr>'.
														'</thead>'.
														'<tbody>'.
															'<tr>'.
																'<th class="text-center">家庭方面：</th>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="100" disabled> 配偶死亡'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="73" disabled> 離婚'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="65" disabled> 分居'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="63" disabled> 親人死亡'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="50" disabled> 結婚'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="44" disabled> 家人生病'.
																	'</label>'.
																'</td>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="40" disabled> 懷孕'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="39" disabled> 新家人的加入'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="35" disabled> 與配偶爭吵'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="29" disabled> 兒女離家'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="26" disabled> 配偶開始工作或退休'.
																	'</label>'.
																'</td>'.
															'</tr>'.
															'<tr>'.
																'<th class="text-center">個人方面：</th>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="63" disabled> 入獄'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="53" disabled> 受傷或生病'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="39" disabled> 性障礙'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="37" disabled> 好友死亡'.
																	'</label>'.
																'</td>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="24" disabled> 個人習慣有重大改變'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="20" disabled> 轉學'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="20" disabled> 搬家'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="13" disabled> 渡假'.
																	'</label>'.
																'</td>'.
															'</tr>'.
															'<tr>'.
																'<th class="text-center">工作方面：</th>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="47" disabled> 解職'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="45" disabled> 退休'.
																	'</label>'.
																'</td>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="34" disabled> 工作職務改變'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="34" disabled> 工作情境有重大改變'.
																	'</label>'.
																'</td>'.
															'</tr>'.
															'<tr>'.
																'<th class="text-center">財務方面：</th>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="38" disabled> 財務狀況有重大改變'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="31" disabled> 抵押借款超過10,000美元'.
																	'</label>'.
																'</td>'.
																'<td class="text-center">'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="30" disabled> 抵押借款被拒'.
																	'</label>'.
																	'<label class="checkbox-inline">'.
																		'<input type="checkbox" class="LCU" value="17" disabled> 抵押借款少於10,000美元'.
																	'</label>'.
																'</td>'.
															'</tr>'.
															'<tr>'.
																'<th class="text-center">結果：</th>'.
																	'<td colspan=2>'.
																		'<p class="text-center" id="LCU_result"></p>'.
																	'</td>'.
															'</tr>'.
														'</tbody>'.
													'</table>'.
													'<div class="form-group">'.
													'</div>';
											break;
										}
									?>
									</div>
								</div>
								<div class="ibox-content">
									<h3>二、補充</h3>
									<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '
													<div class="form-group">
														<div class="col-sm-12">
															<textarea rows ="20" class="form-control" id="supplement">'.$DB_all['fs_supplement'].'</textarea>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-12">
															<button class="btn btn-primary" id="save_supplement">儲存</button>
														</div>
													</div>';
											break;

											case "2" :
											case "6" :
												echo '
													<div class="form-group">
														<div class="col-sm-12">
															<p class="form-control">'.$DB_all['fs_supplement'].'</p>
														</div>
													</div>';
											break;
										}
									?>
									</div>
								</div>
								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch8");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch10");?>" class="btn btn-default">下一步</a>
								</div>
							</div>

							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2>老師評語</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
										<?php
											// 1 學生
											// 2 老師
											// 3 admin
											switch ($permission) {
												case "1" :
												case "6" :
													echo '
														<p class="text-danger form-control">'.$DB_all["comment"].'</p>';
												break;

												case "2" :
												case "3" :
													echo '
														<textarea class="text-danger form-control" name="comment" rows="10" placeholder="老師評語">'.$DB_all['comment'].'</textarea>' .
														'<div class="form-group">' .
															'<div class="col-sm-12"><br>' .
																'<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>' .
															'</div>' .
														'</div>';
												break;
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'           : {},
				'.chosen-select-deselect'  : {allow_single_deselect:true},
				'.chosen-select-no-single' : {disable_search_threshold:10},
				'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}

			/**
			 * LCU_checkbox status
			 */
			window.onload=Check_checked;
			function Check_checked() {
				var LCU = document.querySelectorAll(".LCU");
				$.ajax({
					url: "<?php echo base_url(); ?>Ch9/Select_LCU_status" ,
					success: function (data) {
						var json_value = JSON.parse(data);
						for (var i = 0; i < LCU.length ; i++) {
							if (json_value[i] == LCU[i].value) {
								LCU[i].checked = true;
							}
						}
						var LCU_value  = [];
						var LCU_result = 0;
						for (var x = 0; x < LCU.length ; x++) {
							if (LCU[x].checked) {
								LCU_value[x] = LCU[x].value;
								LCU_result += parseInt(LCU[x].value);
							}
						}
						if (LCU_result > 300) {
							document.querySelector("#LCU_result").innerHTML = ""+LCU_result+"分，約有70%會在二年之內罹患疾病。";
							document.querySelector("#LCU_result").style.color = "rgb(240, 0, 0)";
						} else if (LCU_result >= 150 && LCU_result <= 300) {
							document.querySelector("#LCU_result").innerHTML = ""+LCU_result+"分，約有50%會在二年之內罹患疾病。";
							document.querySelector("#LCU_result").style.color = "rgb(240, 180, 0)";
						} else if (LCU_result <= 150) {
							document.querySelector("#LCU_result").innerHTML = ""+LCU_result+"分。";
							document.querySelector("#LCU_result").style.color = "rgb(15, 200, 0)";
						}
					}
				});
			}

			/**
			 * LCU's value加總
			 * @return 壓力狀態
			 */
			if(document.querySelector("#save_LCU")) {
				document.querySelector("#save_LCU").onclick = function() {
					var LCU = document.querySelectorAll(".LCU");
					var LCU_value  = [];
					var LCU_result = 0;
					for (var x = 0; x < LCU.length ; x++) {
						if (LCU[x].checked) {
							LCU_value[x] = LCU[x].value;
							LCU_result += parseInt(LCU[x].value);
						}
					}
					$.ajax({
						url: "<?php echo base_url(); ?>Ch9/LCU" ,
						type: "POST" ,
						data: {
							LCU_value : JSON.stringify(LCU_value)
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								if (LCU_result > 300) {
									document.querySelector("#LCU_result").innerHTML = LCU_result+"分，約有70%會在二年之內罹患疾病。";
									document.querySelector("#LCU_result").style.color = "rgb(240, 0, 0)";
								} else if (LCU_result >= 150 && LCU_result <= 300) {
									document.querySelector("#LCU_result").innerHTML = LCU_result+"分，約有50%會在二年之內罹患疾病。";
									document.querySelector("#LCU_result").style.color = "rgb(240, 180, 0)";
								} else if (LCU_result <= 150) {
									document.querySelector("#LCU_result").innerHTML = LCU_result+"分。";
									document.querySelector("#LCU_result").style.color = "rgb(15, 200, 0)";
								}
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "警告",
									text: "您有欄位尚未填寫",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}

						}
					});
				}
			}

			/**
			 * 儲存 補充
			 */
			if(document.querySelector("#save_supplement")) {
				document.querySelector("#save_supplement").onclick = function() {
					var supplement = document.querySelector("#supplement").value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch9/Save_Supplement" ,
						type: "POST" ,
						data: {
							supplement : supplement
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}

			/**
			 * 儲存 老師評語
			 */
			if(document.querySelector("#save_comment")) {
				document.querySelector("#save_comment").onclick = function() {
					var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch9/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
