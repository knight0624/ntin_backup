<?php $this->load->view("basic/begin");?>
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Data Picker <small>https://github.com/eternicode/bootstrap-datepicker</small></h5>
								</div>
								<div class="ibox-content">
									<h3>
										Data picker
									</h3>
									<p>
										Simple and easy select a time for a text input using your mouse.
									</p>

									<div class="form-group" id="data_1">
										<label class="font-noraml">Simple data input format</label>
										<div class="input-group date">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="03/04/2014">
										</div>
									</div>

									<div class="form-group" id="data_2">
										<label class="font-noraml">One Year view</label>
										<div class="input-group date">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="08/09/2014">
										</div>
									</div>

									<div class="form-group" id="data_3">
										<label class="font-noraml">Decade view</label>
										<div class="input-group date">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="10/11/2013">
										</div>
									</div>

									<div class="form-group" id="data_4">
										<label class="font-noraml">Month select</label>
										<div class="input-group date">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="07/01/2014">
										</div>
									</div>

									<div class="form-group" id="data_5">
										<label class="font-noraml">Range select</label>
										<div class="input-daterange input-group" id="datepicker">
											<input type="text" class="input-sm form-control" name="start" value="05/14/2014">
											<span class="input-group-addon">to</span>
											<input type="text" class="input-sm form-control" name="end" value="05/22/2014">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Chosen select <small>http://harvesthq.github.io/chosen/</small></h5>
								</div>
								<div class="ibox-content">
									<p>With chosen select uesr can fase chose from large in select input.</p>
									<div class="form-group">
										<label class="font-noraml">Basic example</label>
										<div class="input-group">
											<select data-placeholder="Choose a Country..." class="chosen-select" style="width:350px;">
												<option value="">Select</option>
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Afghanistan">Afghanistan</option>
												<option value="Aland Islands">Aland Islands</option>
												<option value="Albania">Albania</option>
												<option value="Algeria">Algeria</option>
												<option value="American Samoa">American Samoa</option>
												<option value="Andorra">Andorra</option>
												<option value="Angola">Angola</option>
												<option value="Anguilla">Anguilla</option>
												<option value="Antarctica">Antarctica</option>
												<option value="Antigua and Barbuda">Antigua and Barbuda
												<option value="Zimbabwe">Zimbabwe</option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="font-noraml">Multi select</label>
										<div class="input-group">
											<select data-placeholder="Choose a Country..." class="chosen-select" multiple style="width:350px;">
												<option value="">Select</option>
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Afghanistan">Afghanistan</option>
												<option value="Aland Islands">Aland Islands</option>
												<option value="Albania">Albania</option>
												<option value="Algeria">Algeria</option>
												<option value="American Samoa">American Samoa</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Input Mask <small>http://jasny.github.io/bootstrap/</small></h5>
								</div>
								<div class="ibox-content">
									<h3>
										Input Mask
									</h3>
									<p>
										Input masks allows a user to more easily enter data where you would like them to enter the data in a certain format.
									</p>
									<form class="form-horizontal m-t-md" action="#">
										<div class="form-group">
											<label class="col-sm-2 col-sm-2 control-label">ISBN 1</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" data-mask="999-99-999-9999-9" placeholder="">
												<span class="help-block">999-99-999-9999-9</span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">ISBN 2</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" data-mask="999 99 999 9999 9" placeholder="">
												<span class="help-block">999 99 999 9999 9</span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">ISBN 3</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" data-mask="999/99/999/9999/9" placeholder="">
												<span class="help-block">999/99/999/9999/9</span>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label">IPV4</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" data-mask="999.999.999.9999" placeholder="">
												<span class="help-block">192.168.100.200</span>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Tax ID</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" data-mask="99-9999999" placeholder="">
												<span class="help-block">99-9999999</span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Phone</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" data-mask="(999) 999-9999" placeholder="">
												<span class="help-block">(999) 999-9999</span>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Currency</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" data-mask="$ 999,999,999.99" placeholder="">
												<span class="help-block">$ 999,999,999.99</span>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Date</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" data-mask="99/99/9999" placeholder="">
												<span class="help-block">dd/mm/yyyy</span>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>All form elements <small>With custom checbox and radion elements.</small></h5>
								</div>
								<div class="ibox-content">
									<form method="get" class="form-horizontal">
										<div class="form-group"><label class="col-sm-2 control-label">Normal</label>

											<div class="col-sm-10"><input type="text" class="form-control"></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Help text</label>
											<div class="col-sm-10"><input type="text" class="form-control"> <span class="help-block m-b-none">A block of help text that breaks onto a new line and may extend beyond one line.</span>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Password</label>

											<div class="col-sm-10"><input type="password" class="form-control" name="password"></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Placeholder</label>

											<div class="col-sm-10"><input type="text" placeholder="placeholder" class="form-control"></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-lg-2 control-label">Disabled</label>

											<div class="col-lg-10"><input type="text" disabled="" placeholder="Disabled input here..." class="form-control"></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-lg-2 control-label">Static control</label>

											<div class="col-lg-10"><p class="form-control-static">email@example.com</p></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Checkboxes and radios <br/>
											<small class="text-navy">Normal Bootstrap elements</small></label>

											<div class="col-sm-10">
												<div><label> <input type="checkbox" value=""> Option one is this and that&mdash;be sure to include why it's great </label></div>
												<div><label> <input type="radio" checked="" value="option1" id="optionsRadios1" name="optionsRadios"> Option one is this and that&mdash;be sure to
													include why it's great </label></div>
												<div><label> <input type="radio" value="option2" id="optionsRadios2" name="optionsRadios"> Option two can be something else and selecting it will
													deselect option one </label></div>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Inline checkboxes</label>

											<div class="col-sm-10"><label class="checkbox-inline"> <input type="checkbox" value="option1" id="inlineCheckbox1"> a </label> <label class="checkbox-inline">
												<input type="checkbox" value="option2" id="inlineCheckbox2"> b </label> <label class="checkbox-inline">
												<input type="checkbox" value="option3" id="inlineCheckbox3"> c </label></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Checkboxes &amp; radios <br/><small class="text-navy">Custom elements</small></label>

											<div class="col-sm-10">
												<div class="i-checks"><label> <input type="checkbox" value=""> <i></i> Option one </label></div>
												<div class="i-checks"><label> <input type="checkbox" value="" checked=""> <i></i> Option two checked </label></div>
												<div class="i-checks"><label> <input type="checkbox" value="" disabled="" checked=""> <i></i> Option three checked and disabled </label></div>
												<div class="i-checks"><label> <input type="checkbox" value="" disabled=""> <i></i> Option four disabled </label></div>
												<div class="i-checks"><label> <input type="radio" value="option1" name="a"> <i></i> Option one </label></div>
												<div class="i-checks"><label> <input type="radio" checked="" value="option2" name="a"> <i></i> Option two checked </label></div>
												<div class="i-checks"><label> <input type="radio" disabled="" checked="" value="option2"> <i></i> Option three checked and disabled </label></div>
												<div class="i-checks"><label> <input type="radio" disabled="" name="a"> <i></i> Option four disabled </label></div>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Inline checkboxes</label>

											<div class="col-sm-10"><label class="checkbox-inline i-checks"> <input type="checkbox" value="option1">a </label>
												<label class="checkbox-inline i-checks"> <input type="checkbox" value="option2"> b </label>
												<label class="checkbox-inline i-checks"> <input type="checkbox" value="option3"> c </label></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Select</label>

											<div class="col-sm-10"><select class="form-control m-b" name="account">
												<option>option 1</option>
												<option>option 2</option>
												<option>option 3</option>
												<option>option 4</option>
											</select>

												<div class="col-lg-4 m-l-n"><select class="form-control" multiple="">
													<option>option 1</option>
													<option>option 2</option>
													<option>option 3</option>
													<option>option 4</option>
												</select></div>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group has-success"><label class="col-sm-2 control-label">Input with success</label>

											<div class="col-sm-10"><input type="text" class="form-control"></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group has-warning"><label class="col-sm-2 control-label">Input with warning</label>

											<div class="col-sm-10"><input type="text" class="form-control"></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group has-error"><label class="col-sm-2 control-label">Input with error</label>

											<div class="col-sm-10"><input type="text" class="form-control"></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Control sizing</label>

											<div class="col-sm-10"><input type="text" placeholder=".input-lg" class="form-control input-lg m-b">
												<input type="text" placeholder="Default input" class="form-control m-b"> <input type="text" placeholder=".input-sm" class="form-control input-sm">
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Column sizing</label>

											<div class="col-sm-10">
												<div class="row">
													<div class="col-md-2"><input type="text" placeholder=".col-md-2" class="form-control"></div>
													<div class="col-md-3"><input type="text" placeholder=".col-md-3" class="form-control"></div>
													<div class="col-md-4"><input type="text" placeholder=".col-md-4" class="form-control"></div>
												</div>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Input groups</label>

											<div class="col-sm-10">
												<div class="input-group m-b"><span class="input-group-addon">@</span> <input type="text" placeholder="Username" class="form-control"></div>
												<div class="input-group m-b"><input type="text" class="form-control"> <span class="input-group-addon">.00</span></div>
												<div class="input-group m-b"><span class="input-group-addon">$</span> <input type="text" class="form-control"> <span class="input-group-addon">.00</span></div>
												<div class="input-group m-b"><span class="input-group-addon"> <input type="checkbox"> </span> <input type="text" class="form-control"></div>
												<div class="input-group"><span class="input-group-addon"> <input type="radio"> </span> <input type="text" class="form-control"></div>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Button addons</label>

											<div class="col-sm-10">
												<div class="input-group m-b"><span class="input-group-btn">
													<button type="button" class="btn btn-primary">Go!</button> </span> <input type="text" class="form-control">
												</div>
												<div class="input-group"><input type="text" class="form-control"> <span class="input-group-btn"> <button type="button" class="btn btn-primary">Go!
												</button> </span></div>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">With dropdowns</label>

											<div class="col-sm-10">
												<div class="input-group m-b">
													<div class="input-group-btn">
														<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">Action <span class="caret"></span></button>
														<ul class="dropdown-menu">
															<li><a href="#">Action</a></li>
															<li><a href="#">Another action</a></li>
															<li><a href="#">Something else here</a></li>
															<li class="divider"></li>
															<li><a href="#">Separated link</a></li>
														</ul>
													</div>
													 <input type="text" class="form-control"></div>
												<div class="input-group"><input type="text" class="form-control">

													<div class="input-group-btn">
														<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">Action <span class="caret"></span></button>
														<ul class="dropdown-menu pull-right">
															<li><a href="#">Action</a></li>
															<li><a href="#">Another action</a></li>
															<li><a href="#">Something else here</a></li>
															<li class="divider"></li>
															<li><a href="#">Separated link</a></li>
														</ul>
													</div>
													</div>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">Segmented</label>

											<div class="col-sm-10">
												<div class="input-group m-b">
													<div class="input-group-btn">
														<button tabindex="-1" class="btn btn-white" type="button">Action</button>
														<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button"><span class="caret"></span></button>
														<ul class="dropdown-menu">
															<li><a href="#">Action</a></li>
															<li><a href="#">Another action</a></li>
															<li><a href="#">Something else here</a></li>
															<li class="divider"></li>
															<li><a href="#">Separated link</a></li>
														</ul>
													</div>
													<input type="text" class="form-control"></div>
												<div class="input-group"><input type="text" class="form-control">

													<div class="input-group-btn">
														<button tabindex="-1" class="btn btn-white" type="button">Action</button>
														<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button"><span class="caret"></span></button>
														<ul class="dropdown-menu pull-right">
															<li><a href="#">Action</a></li>
															<li><a href="#">Another action</a></li>
															<li><a href="#">Something else here</a></li>
															<li class="divider"></li>
															<li><a href="#">Separated link</a></li>
														</ul>
													</div>
													</div>
											</div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group">
											<div class="col-sm-4 col-sm-offset-2">
												<button class="btn btn-white" type="submit">Cancel</button>
												<button class="btn btn-primary" type="submit">Save changes</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'           : {},
				'.chosen-select-deselect'  : {allow_single_deselect:true},
				'.chosen-select-no-single' : {disable_search_threshold:10},
				'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}
		</script>
<?php $this->load->view("basic/end");?>
