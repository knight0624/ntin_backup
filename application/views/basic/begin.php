<!DOCTYPE html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<!-- Chrome, Firefox OS, Opera and Vivaldi -->
		<meta name="theme-color" content="#1689ce">
		<!-- Windows Phone -->
		<meta name="msapplication-navbutton-color" content="#1689ce">
		<!-- iOS Safari -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#1689ce">

		<title><?php echo $title?></title>

		<!-- 系統icon -->
		<link href="<?php echo base_url(); ?>dist/img/favicon.png" rel="shortcut icon" type="image/x-icon">

		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/font-awesome/css/font-awesome.min.css" rel="stylesheet">

		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/animate.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/style.css" rel="stylesheet">
