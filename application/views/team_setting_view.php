<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<style>
			.ibox{
				height: 880px;
				overflow-y: scroll;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-sm-12">
							<div class="ibox-title">
								<h3 style="float: left"><?php echo $course_name; ?></h3>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="ibox">
								<div class="ibox-content">
									<h2>未分組名單</h2>
									<select id="year">
										<option value="">請選擇入學年度</option>
										<option value="all">全部</option>
										<?php echo $entry_opt; ?>
									</select>
									<select id="cls">
										<option value="">請選擇班級</option>
										<option value="all">全部</option>
										<?php echo $class_opt; ?>
									</select>
									<div class="table-responsive">
										<table class="table table-striped table-hover table-bordered">
											<thead>
												<tr>
													<td>加入</td>
													<td>學號</td>
													<td>姓名</td>
													<td>入學年</td>
													<td>班級</td>
												</tr>
											</thead>
											<tbody id="NotHwStudent">
												<?php echo $NotHwStudent; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-2" style="text-align: center;">
							<div style="margin-top: 100px">
								<p><input type="text" id="team_name" style="width: 100%; height: 50px" placeholder="請輸入隊名"></p>
								<p><button class="btn btn-primary btn-lg" id="insert" style="width: 100%;">新增</button></p>
								<p><button class="btn btn-info btn-lg" id="update" style="width: 100%;">修改</button></p>
							</div>
						</div>

						<div class="col-sm-5">
							<div class="ibox">
								<div class="ibox-content">
									<h2>已分組名單</h2>
									<select id="team">
										<?php echo $team_opt; ?>
									</select>
									<div class="table-responsive">
										<table class="table table-striped table-hover table-bordered">
											<thead>
												<tr>
													<td>刪除</td>
													<td>學號</td>
													<td>姓名</td>
													<td>入學年</td>
													<td>班級</td>
												</tr>
											</thead>
											<tbody id="HwStudent">
												<?php echo $HwStudent; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>
		<script>
			$("#insert").click(function(){
				var i_arr = [];
				var i_temp = document.querySelectorAll(".i_student");
				var team_name = document.querySelector("#team_name").value;
				for (var i = 0; i < i_temp.length; i++) {
					if(i_temp[i].checked){
						i_arr.push(i_temp[i].value);
					}
				}
				if(checkArguments(team_name) && i_arr.length > 0){
					$.ajax({
						url: "<?php echo base_url(); ?>team/setTeaminfo",
						type: "POST",
						data: {
							acc : JSON.stringify(i_arr),
							team_name : team_name
						},
						dataType: "text",
						success: function(){
							refresh();
						}
					});
				} else {
					swal({
						title: "警告",
						text: "請檢查資料不可為空",
						type: "error",
						confirmButtonText: "確定",
						animation: "slide-from-top"
					});
				}
			});
			$("#update").click(function(){
				var u_arr = [];
				var u_temp = document.querySelectorAll(".d_student");
				var team_name = document.querySelector("#team_name").value;
				for (var i = 0; i < u_temp.length; i++) {
					if(u_temp[i].checked){
						u_arr.push(u_temp[i].value);
					}
				}
				var i_arr = [];
				var i_temp = document.querySelectorAll(".i_student");
				for (var i = 0; i < i_temp.length; i++) {
					if(i_temp[i].checked){
						i_arr.push(i_temp[i].value);
					}
				}
				$.ajax({
					url: "<?php echo base_url(); ?>team/deleteTeaminfo",
					type: "POST",
					data: {
						u_acc : JSON.stringify(u_arr),
						i_acc : JSON.stringify(i_arr),
						team : document.querySelector("#team").value,
						team_name : team_name
					},
					dataType: "text",
					success: function(data){
						refresh();
					}
				});
			});
			var year = "", cls = "" , team = "";

			$("#year").change(function(){
				year = $(this)[0].value
				$.ajax({
					url: "<?php echo base_url(); ?>team/getNotHwStudent",
					type: "POST",
					data: {
						year : year,
						cls : cls,
					},
					dataType: "text",
					success: function(data){
						$("#NotHwStudent").html(data);
					}
				});
				$.ajax({
					url: "<?php echo base_url(); ?>team/getHwStudent",
					type: "POST",
					data: {
						team_id : team
					},
					dataType: "text",
					success: function(data){
						$("#HwStudent").html(data);
					}
				});
			})

			$("#cls").change(function(){
				cls = $(this)[0].value
				$.ajax({
					url: "<?php echo base_url(); ?>team/getNotHwStudent",
					type: "POST",
					data: {
						year : year,
						cls : cls,
					},
					dataType: "text",
					success: function(data){
						$("#NotHwStudent").html(data);
					}
				});
				$.ajax({
					url: "<?php echo base_url(); ?>team/getHwStudent",
					type: "POST",
					data: {
						team_id : team
					},
					dataType: "text",
					success: function(data){
						$("#HwStudent").html(data);
					}
				});
			})

			$("#team").change(function(){
				team = $(this)[0].value
				$("#team_name").val($(this).find("option:selected").text())
				$.ajax({
					url: "<?php echo base_url(); ?>team/getNotHwStudent",
					type: "POST",
					data: {
						year : year,
						cls : cls,
					},
					dataType: "text",
					success: function(data){
						$("#NotHwStudent").html(data);
					}
				});
				$.ajax({
					url: "<?php echo base_url(); ?>team/getHwStudent",
					type: "POST",
					data: {
						team_id : team
					},
					dataType: "text",
					success: function(data){
						$("#HwStudent").html(data);
					}
				});
			})

			function refresh(){
				$.ajax({
					url: "<?php echo base_url(); ?>team/getTeamOpt",
					type: "POST",
					data: {
						ajax : true
					},
					dataType: "text",
					success: function(data){
						$("#team").html(data);
						location.reload();
					}
				});
			}
		</script>
<?php $this->load->view("basic/end");?>
