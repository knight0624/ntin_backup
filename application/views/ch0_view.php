<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 訪視紀錄</h2>
								</div>
								<div class="ibox-content">
									<label class="col-md-8">新增受訪者</label>
									<div class="form-horizontal">
										<form class="form-horizontal" action="<?php echo base_url(); ?>ch0/insertRespondent" method="POST">
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">帳號</label>
											<div class="col-sm-10"><input type="text" class="form-control" name="account" value="<?php echo set_value("account"); ?>" placeholder="輸入格式 如：510201100" required><div class="text-danger err-user_name"><?php echo form_error("account"); ?></div></div>
										</div>
										<div class="form-group"><label class="col-sm-2 control-label">姓名</label>
											<div class="col-sm-10"><input type="text" class="form-control" name="user_name" value="<?php echo set_value("user_name"); ?>" placeholder="輸入格式 如：王小明" required><div class="text-danger err-user_name"><?php echo form_error("user_name"); ?></div></div>
										</div>
										<div class="form-group"><label class="col-sm-2 control-label">信箱</label>
											<div class="col-sm-10"><input type="text" class="form-control" name="email" value="<?php echo set_value("email"); ?>" pattern="[a-zA-Z0-9!#$%&amp;'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" placeholder="輸入格式 如：XXX@gmail.com" title="需符合規定格式"><div class="text-danger err-email"><?php echo form_error("email"); ?></div></div>
										</div>
										<div class="text-center">
											<input type="submit"  class="btn btn-primary" value="新增">
											<input type="hidden" name="token" value="<?php echo $_SESSION["insert_token"]?>">
										</div>
									</form>
										<div class="form-group">
										<label class="col-md-8">記錄筆數：<?php echo count($record); ?></label>
										</div>
										<table class="table">
										  <thead>
										    <tr>
										      <th>id</th>
										      <th>紀錄者</th>
										      <th>訪視狀況</th>
										      <th>訪視日期</th>
										    </tr>
										  </thead>
										  <tbody>

										  	<?php

												for ($i=0; $i<count($record) ; $i++) { 
													echo 
													'<tr>
										      			<th scope="row">'.$record[$i]["record_id"].'</th>
											      		<td>'.$record[$i]["account"].'</td>';
											      		
											      		if($permission == '1' || $permission == '3'){
											      			echo 
															'<td><input class="status"type="text" name=""  value="'.$record[$i]["invite_note"].'">
											      			<button class="btn btn-primary update-status" data-recordid="'.$record[$i]["record_id"].'"value="'.$record[$i]["record_id"].'">編輯<button class="btn btn-danger delete-status" data-recordid="'.$record[$i]["record_id"].'"value="'.$record[$i]["record_id"].'">刪除</td>';
											      		}
											      		else{
											      			echo 
															'<td><p class="status"type="text" name=""  >'.$record[$i]["invite_note"].'</p>';
											      		}
											      		
											      	echo 	
											      		'<td>'.$record[$i]["date"].'</td>
										    		</tr>';

												}
											?>
										  
										    
										    
										  </tbody>
										</table>
										
										<?php 
											switch ($permission) {
												case '1':
												case '3':
													echo 
														'<div id="data_1">
															<label class="">訪視狀況</label>
															<input style="width:100%" type="text" id="status">
															<label class="">訪視日期</label>
															<div style="width:100%" class="input-group date">
																<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="date">
															</div>
															<button class="btn btn-primary" id="save_record">新增</button>
														</div>';
												break;

												
											}
										?>
										
										
									</div>
								</div>
							</div>

						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>

		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>

		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			$(".update-status").click(function(){
				var recordid = $(this).data("recordid");
				var status = $(this).prev().val();
				$.ajax({
					url: "<?php echo base_url(); ?>ch0/editstatus" ,
					type: "POST" ,
					data: {
						record_id : recordid,
						status : status
						} ,
					dataType: "text" ,
					success: function(data){
						swal({
							title: "成功",
							text: "更改成功",
							type: "success",
							confirmButtonText: "確定",
						});

					}
				});
			})

			$(".delete-status").click(function(){
				var recordid = $(this).data("recordid");
				var status = $(this).prev().val();
				swal({	title: "刪除文件?",   
						text: "按下刪除後文件不可復原",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "刪除!",
						cancelButtonText: "取消",
						closeOnConfirm: false,
						closeOnCancel: false },
						function(isConfirm){
							if (isConfirm) {
								$.ajax({
									url: "<?php echo base_url(); ?>ch0/deletestatus" ,
									type: "POST" ,
									data: {
										record_id : recordid,
										status : status
										} ,
									dataType: "text" ,
									success: function(data){
										swal({
											title: "已刪除",
											title: "已刪除",
											text: "刪除成功",
											type: "success",
											confirmButtonText: "確定",
										},
										function(){
											location.reload();
				  						});

									}
								});
							} 
							else {
								swal("取消!", "沒有被刪除", "error");   
							}
						}
				);



				
			})
			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'           : {},
				'.chosen-select-deselect'  : {allow_single_deselect:true},
				'.chosen-select-no-single' : {disable_search_threshold:10},
				'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}

			/**
			 * [onclick Foreword AJAX]
			 * @return {[type]} [description]
			 */
			if(document.querySelector("#btn_foreword")){
				document.querySelector("#btn_foreword").onclick = function(){
					var foreword_text = document.querySelector("#foreword_text").value;
					if(foreword_text != null){
						$.ajax({
							url: "<?php echo base_url(); ?>ch1/editForeword" ,
							type: "POST" ,
							data: {
								foreword_text : foreword_text
							} ,
							dataType: "text" ,
							success: function(data){
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							}
						});
					 } else {
						swal({
							title: "警告",
							text: "標題或內文不能為空",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				}
			}

			if(document.querySelector("#save_comment")){
				document.querySelector("#save_comment").onclick = function(){
				var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch1/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data){
							swal({
								title: "成功",
								text: "已儲存",
								type: "success",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					});
				}
			}

			if(document.querySelector("#save_record")){
				document.querySelector("#save_record").onclick = function(){
					var status = document.querySelector("#status").value;
					var date = document.querySelector("#date").value;
					if(status!=""  && date!=""){

						$.ajax({
							url: "<?php echo base_url(); ?>ch0/Addrecord" ,
							type: "POST" ,
							data: {
								status : status,
								 date : date
							} ,
							dataType: "text" ,
							success: function(data){
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								},
								function(){
									location.reload();
								});
							}
						});

					 } else {
						swal({
							title: "警告",
							text: "標題或內文不能為空",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				}
			}
			
		</script>
<?php $this->load->view("basic/end");?>
