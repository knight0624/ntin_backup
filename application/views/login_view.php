	<?php $this->load->view("basic/begin");?>
		<style>
			body {
				background-color: initial;
				background: url(dist/img/background.png) no-repeat center center fixed;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;
			}

			.login-container{
				width: 380px;
				margin: 0 auto;
				background-color: rgba(234, 234, 234, 0.9);
				border-radius: 10px;
			}

			@media screen and (max-width: 380px) {
				.login-container{
					width: 100%;
				}
				.loginscreen.middle-box{
					width: 100%;
				}
			}

			.middle-box{
				padding-top: 20px;
				padding-bottom: 20px;
			}
		</style>
	</head>

	<body>
		<div style="position: relative;top:50%; transform:translateY(-50%);">
			<div class="login-container animated fadeInDown">
				<div class="middle-box text-center loginscreen">
					<h2>歡迎使用智慧型家庭護理個案管理平台</h2>
					<form class="m-t" role="form" action="<?php echo base_url(); ?>login" method="POST">
						<div class="form-group">
							<input type="text" class="form-control" name="username" placeholder="帳號" required>
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="password" placeholder="密碼" required>
						</div>
						<button type="submit" class="btn btn-primary block full-width m-b">登入</button>
					</form>
					<p class="m-t">
						<small>樹德科技大學 &copy; 2016</small>
					</p>
				</div>
			</div>
		</div>

		<!-- Mainly scripts -->
		<script src="<?php echo base_url(); ?>dist/js/jquery-2.1.1.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/bootstrap.min.js"></script>
	</body>
</html>
