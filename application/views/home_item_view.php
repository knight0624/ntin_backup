<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/dataTables/datatables.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<style>
			.modal {
				text-align: center;
				padding: 0!important;
			}

			.modal:before {
				content: '';
				display: inline-block;
				height: 100%;
				vertical-align: middle;
				margin-right: -4px;
			}

			.modal-dialog {
				display: inline-block;
				text-align: left;
				vertical-align: middle;
				width: 40%;
			}

			.modal-body {
				padding: 15px;
			}

			.showSweetAlert[data-animation=slide-from-top]{
				animation: slideFromTop 0.5s;
			}

			.file-zoom-fullscreen .modal-dialog{
				position: static;
			}

			.btn-grp select {
				width: 170px;
				height: 34px;
				text-align-last: center;
			}

			.vertical-align {
				display: flex;
				align-items: center;
			}

			.btn-grp {
				margin-bottom: 13px;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="ibox">
						<div class="ibox-title">
							<h3>作業列表 - <?php echo $_SESSION["course_name"]; ?></h3>
						</div>
						<div class="ibox-content">
							<div style="height:48px;">
								<?php
									if ($writable) {
										echo "<button class='btn btn-primary' id='setModal'>新增作業</button>";
									}
								?>
								<button type="button" class="btn btn-warning" onclick="javascript:history.back()">回上一頁</button>
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover datatable" style="width:100%;">
									<thead>
										<tr>
											<th>操作項目</th>
											<th>作業名稱</th>
											<th>作業截止時間</th>
											<th>建立時間</th>
										</tr>
									</thead>

									<tfoot>
										<tr>
											<th>操作項目</th>
											<th>作業名稱</th>
											<th>作業截止時間</th>
											<th>建立時間</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>

					<div class="modal fade" id="modal_page" role="dialog" tabindex='-1'>
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h3 class="modal-title">新增作業</h3>
								</div>

								<div class="form-horizontal">
									<div class="modal-body">
										<div class="form-group">
											<label class="col-sm-3 col-sm-offset-1 control-label">作業名稱</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="hw_name" placeholder="請輸入作業名稱">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 col-sm-offset-1 control-label">作業截止時間</label>
											<div class="col-sm-8">
												<input type="date" class="form-control" id="hw_end_time" placeholder="請輸入截止時間">
											</div>
										</div>
										<hr>
										<div class="modal-footer">
											<input type="button" id="setItem" class="btn btn-primary" value="新增">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal fade" id="team_modal" role="dialog" tabindex='-1'>
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h3 class="modal-title">分組列表</h3>
								</div>

								<div class="form-horizontal">
									<div class="modal-body">
										<table class="table table-bordered table-hover">
											<thead>
												<tr>
													<th>詳細資訊</th>
													<th>分組名稱</th>
													<th>隊長姓名</th>
												</tr>
											</thead>
											<tbody id="team_item">
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/dataTables/datatables.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>
		<script>
			var datatable = $(".datatable").DataTable({
				"language":{
					"sEmptyTable":        "沒有搜尋到結果",
					"sInfo":              "顯示第 _START_ 至 _END_ 項結果 , 共 _TOTAL_ 項",
					"sInfoEmpty":         "顯示第 0 至 0 項結果 , 共 0 項",
					"sInfoFiltered":      "（由 _MAX_ 項結果過濾）",
					"sInfoPostFix":       "",
					"sInfoThousands":     ",",
					"sLengthMenu":        "顯示 _MENU_ 項結果",
					"sLoadingRecords":   "載入中...",
					"sProcessing":       "<img src='<?php echo base_url(); ?>dist/img/loader.gif'>",
					"sSearch":            "搜尋:",
					"sZeroRecords":       "沒有搜尋結果",
					"oPaginate": {
						"sFirst":           "首頁",
						"sPrevious":        "上頁",
						"sNext":            "下頁",
						"sLast":            "末頁"
					},
					"oAria": {
						"sSortAscending":  ":以遞增排列此列",
						"sSortDescending": ":以遞減排列此列"
					}
				},
				"processing": true,
				"serverSide": true,
				"scrollX": true,
				"ajax": "<?php echo base_url(); ?>home/gethomework",
				"serverMethod": "POST",
				"sorting": [[3, "desc"]],
				"columnDefs": [
					{"width": "120px", "targets": [0] },
					{"orderable": false, "targets": [0] },
					{"bSearchable": false, "targets": [0] }
				],
				"columns": [
					{ "data": "edit_btn" },
					{ "data": "hw_name" },
					{ "data": "hw_end_time" },
					{ "data": "create_time" }
				]
			});

			var fun , edit;

			$("#setModal").click(function(){
				$("#setItem").val("新增");
				fun = "set";
				$("#modal_page").modal();
			});

			$(".datatable").on("click", ".editbtn", function(){
				$("#setItem").val("更新");
				fun = "update";
				edit = $(this).data("edit");
				$.ajax({
					url: "<?php echo base_url(); ?>home/getHomeworkDetial",
					type: "POST",
					data: {
						edit : edit
					},
					dataType: "text",
					success: function(data){
						var temp_item = JSON.parse(data);
						document.querySelector("#hw_name").value = temp_item.hw_name;
						document.querySelector("#hw_end_time").value = temp_item.hw_end_time;
						$("#modal_page").modal();
					}
				});
			});

			$("#modal_page").on("hidden.bs.modal", function () {
				initArguments(
					document.querySelector("#hw_name"),
					document.querySelector("#hw_end_time")
				);
			})

			$("#setItem").click(function(){
				var hw_name = document.querySelector("#hw_name").value;
				var hw_end_time = document.querySelector("#hw_end_time").value;

				var check_value = checkArguments(hw_name , hw_end_time);
				if( check_value ){
					swal({
						title: "注意!",
						text: "課程資料將永久保留在資料庫",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#ec4758",
						confirmButtonText: "確定",
						cancelButtonText: "取消",
						closeOnConfirm: false,
						closeOnCancel: false,
						animation: "slide-from-top"
					},function(isConfirm) {
						if (isConfirm) {
							if(fun=="set"){
								$.ajax({
									url: "<?php echo base_url(); ?>home/setHomework" ,
									type: "POST" ,
									data: {
										hw_name: hw_name,
										hw_end_time : hw_end_time
									} ,
									dataType: "text" ,
									success: function(){
										datatable.draw();
										swal({
											title: "成功",
											text: "操作成功!",
											type: "success",
											confirmButtonText: "確定",
											animation: "slide-from-top"
										});
										$("#modal_page").modal("hide");
									}
								});
							} else if (fun=="update"){
								$.ajax({
									url: "<?php echo base_url(); ?>home/updateHomework" ,
									type: "POST" ,
									data: {
										hw_id: edit,
										hw_name: hw_name,
										hw_end_time : hw_end_time
									} ,
									dataType: "text" ,
									success: function(){
										datatable.draw();
										swal({
											title: "成功",
											text: "操作成功!",
											type: "success",
											confirmButtonText: "確定",
											animation: "slide-from-top"
										});
										$("#modal_page").modal("hide");
									}
								});
							}
						} else {
							swal({
								title: "取消",
								text: "您已取消刪除!",
								type: "error",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					}
					);
				} else {
					swal({
						title: "警告",
						text: "請檢查資料不可為空",
						type: "error",
						confirmButtonText: "確定",
						animation: "slide-from-top"
					});
				}
			});


			// $(".datatable").on("click", ".hwbtn", function(){
			// 	var hw = $(this).data("hw");
			// 	$.ajax({
			// 		url: "<?php echo base_url(); ?>team/getTeam",
			// 		type: "POST",
			// 		data: {
			// 			hw : hw
			// 		},
			// 		dataType: "text",
			// 		success: function(data){
			// 			$("#team_item").html(data);
			// 			$("#team_modal").modal();
			// 		}
			// 	});
			// });
		</script>
<?php $this->load->view("basic/end");?>
