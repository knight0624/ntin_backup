<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="">
		<style>
			.ibox{
				height: 880px;
				overflow-y: scroll;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-6 col-lg-offset-3">
							<div class="ibox">
								<div class="ibox-title">
									<h3 style="float: left"><?php echo $course_name; ?></h3>
									<div class="ibox-tools">
									<div style="display: inline;">修課人數：<span id="num"><?php echo $num; ?></span></div>
										<select id="year">
											<option value="">請選擇入學年度</option>
											<?php echo $entry_opt; ?>
										</select>
										<select id="cls">
											<option value="">請選擇班級</option>
											<option value="all">全部</option>
											<?php echo $class_opt; ?>
										</select>
									</div>

								</div>
								<div class="ibox-content">
									<table class="table table-bordered table-hover">
										<thead>
											<tr>
												<td><input type="checkbox" id="check_all">加入課程</td>
												<td>學號</td>
												<td>姓名</td>
												<td>入學年</td>
												<td>班級</td>
											</tr>
										</thead>
										<tbody id="student">
											<?php echo $student; ?>
										</tbody>
									</table>
									<div class="text-center">
										<button type="button" class="btn btn-warning" onclick="javascript:history.back()">回上一頁</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script>
			var year = "", cls = "";
			init();
			function init(){
				$(".student").click(function(){
					if($(this).is(':checked')){
						$.ajax({
							url: "<?php echo base_url(); ?>course/setStudent",
							type: "POST",
							data: {
								acc : $(this)[0].value
							},
							dataType: "text",
							success: function(){
								refresh();
							}
						});
					} else {
						$.ajax({
							url: "<?php echo base_url(); ?>course/deleteStudent",
							type: "POST",
							data: {
								acc : $(this)[0].value
							},
							dataType: "text",
							success: function(){
								refresh();
							}
						});
					}
				})
			}

			$("#year").change(function(){
				year = $(this)[0].value
				$.ajax({
					url: "<?php echo base_url(); ?>course/getCourseStudent",
					type: "POST",
					data: {
						year : year,
						cls : cls
					},
					dataType: "text",
					success: function(data){
						$("#student").html(data);
						init();
					}
				});
			})

			$("#cls").change(function(){
				cls = $(this)[0].value
				$.ajax({
					url: "<?php echo base_url(); ?>course/getCourseStudent",
					type: "POST",
					data: {
						year : year,
						cls : cls
					},
					dataType: "text",
					success: function(data){
						$("#student").html(data);
						init();
					}
				});
			})

			$("#check_all").click(function(){
				var item = [];
				var data = document.querySelectorAll(".student");
				var fun = "";
				if($(this).is(':checked')){
					for (var i = 0; i < data.length; i++) {
						data[i].checked = true;
						item.push(data[i].value);
						fun = "insert";
					}
				} else {
					for (var i = 0; i < data.length; i++) {
						data[i].checked = false;
						item.push(data[i].value);
						fun = "delete";
					}
				}
				$.ajax({
					url: "<?php echo base_url(); ?>course/batch",
					type: "POST",
					data: {
						data : JSON.stringify(item),
						fun : fun
					},
					dataType: "text",
					success: function(){
						refresh();
					}
				});
			})

			function refresh(){
				$.ajax({
					url: "<?php echo base_url(); ?>course/getNum",
					type: "POST",
					data: {
						ajax : true
					},
					dataType: "text",
					success: function(data){
						$("#num").text(data);
					}
				});
			}
		</script>
<?php $this->load->view("basic/end");?>
