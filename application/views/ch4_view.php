<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
			.btn-group{
				margin-top: 20px;
				margin-left: 5px;
			}
			.upload-group{
				margin: 0px auto;
				text-align: center;
			}
			.upload-group div{
				display: inline-block;
				vertical-align: top;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 肆、個案家庭成員</h2>
								</div>
								<div class="ibox-content" style="overflow-x: scroll;">
									<h3>一、家庭成員基本資料及健康狀況</h3>
									<br>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<div class="form-group">'.
														'<table class="table table-hover">'.
														'<thead>'.
															'<tr>'.
																'<th style="width: 3%">操作</th>'.
																'<th style="width: 7%">暱稱</th>'.
																'<th style="width: 6.5%">姓名</th>'.
																'<th style="width: 4%">年齡</th>'.
																'<th style="width: 6%">正式角色</th>'.
																'<th style="width: 6%">非正式角色</th>'.
																'<th style="width: 6%">教育程度</th>'.
																'<th style="width: 1%">宗教</th>'.
																'<th style="width: 2%">職業</th>'.
																'<th style="width: 1%">語言</th>'.
																'<th style="width: 16%">不健康行為或疾病</th>'.
															'</tr>'.
														'</thead>'.
														'<tbody id="home_member">';
															for ($id=0; $id < $member[0]; $id++){
																$value = $member[1]->row_array($id);
																echo
																	'<tr>'.
																	'<td>' . "<button class='btn btn-danger btn-xs del_member' name="."\""."del"."\" ". "data-a =" . "\"". $value['fm_id'] . "\"><i class='fa fa-trash'></i></button>" . '</td>'.
																	'<td><div contenteditable="true" class="fm_relatives" data-a ="'.$value['fm_id'].'">' . $value['fm_relatives'] . '</div></td>'.
																	'<td><div contenteditable="true" class="fm_name" data-a ="'.$value['fm_id'].'">' . $value['fm_name'] . '</div></td>'.
																	'<td><div contenteditable="true" class="fm_age" data-a ="'.$value['fm_id'].'">' . $value['fm_age'] . '</div></td>'.
																	'<td><div contenteditable="true" class="fm_formal_role" data-a ="'.$value['fm_id'].'">' . $value['fm_formal_role'] . '</div></td>'.
																	'<td><div contenteditable="true" class="fm_informal_role" data-a ="'.$value['fm_id'].'">' . $value['fm_informal_role'] . '</div></td>'.
																	'<td><div contenteditable="true" class="fm_education" data-a ="'.$value['fm_id'].'">' . $value['fm_education'] . '</div></td>'.
																	'<td><div contenteditable="true" class="fm_religious_faith" data-a ="'.$value['fm_id'].'">' . $value['fm_religious_faith'] . '</div></td>'.
																	'<td><div contenteditable="true" class="fm_occpational" data-a ="'.$value['fm_id'].'">' . $value['fm_occpational'] . '</div></td>'.
																	'<td><div contenteditable="true" class="fm_language" data-a ="'.$value['fm_id'].'">' . $value['fm_language'] . '</div></td>'.
																	'<td><div contenteditable="true" class="fm_disease" data-a ="'.$value['fm_id'].'">' . $value['fm_disease'] . '</div></td>'.
																'</tr>';
															}
															echo
																'<tr>'.
																	'<form method="post" name="form_home_member">'.
																		'<td><button class="btn btn-primary btn-xs" type="submit" ><i class="fa fa-plus"></i></button></td>'.
																		'<td><input type="text" class="form-control" id="fm_relatives" name="fm_relatives" required></td>'.
																		'<td><input type="text" class="form-control" id="fm_name" name="fm_name" required></td>'.
																		'<td><input type="number" class="form-control" id="fm_age" name="fm_age" style="width: 80px" required></td>'.
																		'<td><input type="text" class="form-control" id="fm_formal_role" name="fm_formal_role" required></td>'.
																		'<td><input type="text" class="form-control" id="fm_informal_role" name="fm_informal_role" required></td>'.
																		'<td><select class="form-control" name="fm_education" id="fm_education" required>'.
																			'<option>未就學</option>'.
																			'<option>國小畢業</option>'.
																			'<option>國中畢業</option>'.
																			'<option>高中畢業</option>'.
																			'<option>大學畢業</option>'.
																			'<option>其他</option>'.
																		'</select></td>'.
																			'<td><select class="form-control" name="fm_religious_faith" id="fm_religious_faith" style="width:80px;" required>'.
																			'<option>佛教</option>'.
																			'<option>道教</option>'.
																			'<option>天主教</option>'.
																			'<option>基督教</option>'.
																			'<option>回教</option>'.
																			'<option>一貫道</option>'.
																			'<option>巴哈伊教</option>'.
																			'<option>天理教</option>'.
																			'<option>其他</option>'.
																		'</select></td>'.
																			'<td><input type="text" class="form-control" id="fm_occpational" name="fm_occpational" style="width:80px;" required></td>'.
																			'<td><select class="form-control" name="fm_language" id = "fm_language" style="width:80px;" required>'.
																			'<option>國語</option>'.
																			'<option>台語</option>'.
																			'<option>客語</option>'.
																			'<option>英語</option>'.
																			'<option>粵語</option>'.
																			'<option>日語</option>'.
																			'<option>泰雅語群</option>'.
																			'<option>鄒語群</option>'.
																			'<option>排灣語群</option>'.
																			'<option>巴丹語群</option>'.
																			'<option>其他</option>'.
																		'</select></td>'.
																			'<td><input type="text" class="form-control" id="fm_disease" name="fm_disease" required></td>'.
																	'</tr>'.
																'</tbody>'.
															'</table>'.
														'<p>補充說明：</p>'.
														'<textarea class="form-control" rows="20" placeholder="補充說明" name="fm_other" id="fm_other">' .  $show_fm_other["fm_other"] . '</textarea><br>' .
														'</form>'.
														'<button class="btn btn-primary" id="btn_fm_other">儲存</button>'.
													'</div>'.
													'<div class="ibox-content">'.
														'<h3>二、家系圖</h3>'.
														'<img src="' . ((!empty($img_file_name)) ? (base_url() . 'uploads/img/' . $img_file_name) : (base_url(). 'dist/img/家系圖.jpg')) .'" alt="" style="display:block; margin:auto; height:auto; max-width:100%;">' .
														'<P class="text-center">圖1家系圖</P>'.
														'<div class="upload-group">'.
															'<div>'.
																'<a  href="' . base_url() . 'download/家系圖.pptx" download>'.
																	'<img src="' . base_url().'dist/img/download_example.png"' . 'style="width:150px;">'.
																'</a>'.
															'</div>'.
															'<div>'.
																form_open_multipart('Ch4/do_upload_ppt').
																	'<input type="file" name="userfile" id="ppt_file" style="display:none;">'.
																	'<input type="submit" id="ppt_upload" style="display:none;">'.
																	'<img src="' . base_url() . 'dist/img/select_ppt.png"  id="btn_ppt_file" value="選擇檔案(PPT)" style="margin:auto; width:150px;">'.
																	'<img src="' . base_url() . 'dist/img/upload_ppt.png"  id="btn_ppt_upload" value="上傳(PPT)" style="margin:auto; width:150px;">'.
																'</form>'.
																'<div class="btn-group">'.
																		((!empty($ppt_file_name))
																			? ('<a class="btn btn-primary btn-xs" href="' . base_url() . 'uploads/ppt/' . $ppt_file_name . '" download style="margin:auto;">下載：' . $ppt_file_name . '</a>' . '<a class="btn btn-danger btn-xs" name="del_file" id="del_ppt"'.'data-a="' .$ppt_file_name. '"><i class="fa fa-trash"></i></a>')
																			: ('')
																		).
																'</div>'.
															'</div>'.
															'<div>'.
																form_open_multipart('ch4/do_upload').
																	'<input type="file" name="userfile" id="img_file" style="display:none;">'.
																	'<input type="submit" id="img_upload" style="display:none;">'.
																	'<img src="' . base_url() . 'dist/img/select_img.png" id="btn_img_file" value="選擇檔案(JPG)" style="margin:auto; width:150px;">'.
																	'<img src="' . base_url() . 'dist/img/upload_img.png" id="btn_img_upload" value="上傳(JPG)" style="argin:auto; width:150px;">'.
																'</form>'.
																'<div class="btn-group">'.

																	((!empty($img_file_name))
																		? ('<a class="btn btn-primary btn-xs" href="' . base_url() . 'uploads/img/' . $img_file_name . '" download style=" margin:auto;">下載：'.$img_file_name. '</a>' . '<a class="btn btn-danger btn-xs" name="del_file" id="del_img"' . 'data-a="' . $img_file_name . '"><i class="fa fa-trash"></i></a>')
																		: ('')
																	) .

																'</div>'.
															'</div>'.
														'</div>'.
														'<div align="center">'.
															'<p class="text-danger">檔案大小限制為8MB</p>'.
														'</div>';

												break;
											case '2':
											case '6':
												echo
													'<div class="form-group">'.
														'<table class="table table-hover">'.
														'<thead>'.
															'<tr>'.
																'<th style="width: 3%">操作</th>'.
																'<th style="width: 7%">暱稱</th>'.
																'<th style="width: 6.5%">姓名</th>'.
																'<th style="width: 4%">年齡</th>'.
																'<th style="width: 6%">正式角色</th>'.
																'<th style="width: 6%">非正式角色</th>'.
																'<th style="width: 6%">教育程度</th>'.
																'<th style="width: 1%">宗教</th>'.
																'<th style="width: 2%">職業</th>'.
																'<th style="width: 1%">語言</th>'.
																'<th style="width: 16%">不健康行為或疾病</th>'.
															'</tr>'.
														'</thead>'.
														'<tbody id="home_member">';
															for ($id=0; $id < $member[0]; $id++){
																$value = $member[1]->row_array($id);
																echo
																'<tr>'.
																	'<td>' . "" . '</td>'.
																	'<td>' . $value['fm_relatives'] . '</td>'.
																	'<td>' . $value['fm_name'] . '</td>'.
																	'<td>' . $value['fm_age'] . '</td>'.
																	'<td>' . $value['fm_formal_role'] . '</td>'.
																	'<td>' . $value['fm_informal_role'] . '</td>'.
																	'<td>' . $value['fm_education'] . '</td>'.
																	'<td>' . $value['fm_religious_faith'] . '</td>'.
																	'<td>' . $value['fm_occpational'] . '</td>'.
																	'<td>' . $value['fm_language'] . '</td>'.
																	'<td>' . $value['fm_disease'] . '</td>'.
																'</tr>';
															}
															echo
																'</tbody>'.
																	'</table>'.
																'<p>補充說明：</p>'.
																'<p class="form-control" rows="3" placeholder="補充說明" name="fm_other" id="fm_other">' .  $show_fm_other["fm_other"] . '</p><br>' .
																'</form>'.
															'</div>'.
															'<div class="ibox-content">'.
																'<h3>二、家系圖</h3>'.
																'<img src="' .  ((!empty($img_file_name)) ? (base_url() . 'uploads/img/' . $img_file_name) : (base_url(). 'dist/img/家系圖.jpg')) .'" alt="" style="display:block; margin:auto; height:auto; max-width:100%;">' .
																'<P class="text-center">圖1家系圖</P>'.
																'<div class="upload-group">'.
																	'<div>'.
																	'</div>'.
																	'<div>'.
																		'<div class="btn-group">'.
																				((!empty($ppt_file_name))
																					? ('<a class="btn btn-primary btn-xs" href="' . base_url() . 'uploads/ppt/' . $ppt_file_name . '" download style="margin:auto;">下載：' . $ppt_file_name . '</a>')
																					: ('')
																				).
																		'</div>'.
																	'</div>'.
																	'<div>'.
																		'<div class="btn-group">'.
																			((!empty($img_file_name))
																				? ('<a class="btn btn-primary btn-xs" href="' . base_url() . 'uploads/img/' . $img_file_name . '" download style=" margin:auto;">下載：'.$img_file_name. '</a>')
																				: ('')
																			) .

																		'</div>'.
																	'</div>'.
																'</div>';
												break;
										}
									?>
									<div class="ibox-content text-center">
										<a href="<?php echo base_url("ch3");?>" class="btn btn-default">上一步</a>
										<a href="<?php echo base_url("ch5");?>" class="btn btn-default">下一步</a>
									</div>
								</div>
							</div>
							<?php
								// 1 學生
								// 2 老師
								// 3 admin
								switch ($permission) {
									case '1':
									case '6':
										echo
											'<div class="ibox float-e-margins">' .
												'<div class="ibox-title">' .
													'<h2>老師評語</h2>' .
											 	'</div>' .
											 	'<div class="ibox-content">' .
											 		'<div class="form-horizontal">' .
											 			'<p class="text-danger form-control">'.$comment_data['comment'].'</p>' .
											 		'</div>' .
											 	'</div>' .
											'</div>' ;
										break;
									case '2':
									case '3':
										echo
											'<div class="ibox float-e-margins">' .
												'<div class="ibox-title">' .
													'<h2>老師評語</h2>' .
											 	'</div>' .
											 	'<div class="ibox-content">' .
											 		'<div class="form-horizontal">' .
											 			'<textarea class="form-control" name="comment" rows="10" placeholder="老師評語">'.$comment_data['comment'].'</textarea>' .
											 			'<div class="form-group">' .
											 				'<div class="col-sm-10"><br>' .
											 					'<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>' .
											 				'</div>' .
											 			'</div>' .
											 		'</div>' .
											 	'</div>' .
											'</div>' ;
										break;
								}
							?>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>

		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>
		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			if(document.getElementById("btn_img_file")){
				document.getElementById("btn_img_file").onclick = function(){
					document.getElementById("img_file").click(true);
				}
			}

			if(document.getElementById("btn_img_upload")){
				document.getElementById("btn_img_upload").onclick = function(){
					document.getElementById("img_upload").click(true);
				}
			}

			if(document.getElementById("btn_ppt_file")){
				document.getElementById("btn_ppt_file").onclick = function(){
					document.getElementById("ppt_file").click(true);
				}
			}

			if(document.getElementById("btn_ppt_upload")){
				document.getElementById("btn_ppt_upload").onclick = function(){
					document.getElementById("ppt_upload").click(true);
				}
			}


			var edit_fm_relatives = document.querySelectorAll(".fm_relatives");
			for(var i = 0;i<edit_fm_relatives.length;i++){
				edit_fm_relatives[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_relatives" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var edit_fm_name = document.querySelectorAll(".fm_name");
			for(var i = 0;i<edit_fm_name.length;i++){
				edit_fm_name[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_name" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var edit_fm_age = document.querySelectorAll(".fm_age");
			for(var i = 0;i<edit_fm_age.length;i++){
				edit_fm_age[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_age" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var edit_fm_formal_role = document.querySelectorAll(".fm_formal_role");
			for(var i = 0;i<edit_fm_formal_role.length;i++){
				edit_fm_formal_role[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_formal_role" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var edit_fm_informal_role = document.querySelectorAll(".fm_informal_role");
			for(var i = 0;i<edit_fm_informal_role.length;i++){
				edit_fm_informal_role[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_informal_role" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var edit_fm_education = document.querySelectorAll(".fm_education");
			for(var i = 0;i<edit_fm_education.length;i++){
				edit_fm_education[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_education" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var edit_fm_religious_faith = document.querySelectorAll(".fm_religious_faith");
			for(var i = 0;i<edit_fm_religious_faith.length;i++){
				edit_fm_religious_faith[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_religious_faith" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var edit_fm_occpational = document.querySelectorAll(".fm_occpational");
			for(var i = 0;i<edit_fm_occpational.length;i++){
				edit_fm_occpational[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_occpational" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var edit_fm_language = document.querySelectorAll(".fm_language");
			for(var i = 0;i<edit_fm_language.length;i++){
				edit_fm_language[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_language" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var edit_fm_disease = document.querySelectorAll(".fm_disease");
			for(var i = 0;i<edit_fm_disease.length;i++){
				edit_fm_disease[i].onblur = function(){
					var edit_temp = this.innerHTML;
					var edit_id = this.dataset.a;
					console.log(edit_temp);
					swal({	title: "修改這筆資料?",
							text: "按下修改後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "修改!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(edit){
								if (edit) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/editMember/fm_disease" ,
										type: "POST",
										data: {
											edit_data : edit_temp,
											fm_id : edit_id
											},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "修改時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

			var del_member_data = document.querySelectorAll(".del_member");
			for(var i = 0;i<del_member_data.length;i++){
				del_member_data[i].onclick = function(){
					var del_temp = this.dataset.a;
					swal({	title: "刪除這筆資料?",
							text: "按下刪除後資料不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "刪除!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(del){
								if (del) {
									$.ajax({
										url: "<?php echo base_url();?>Ch4/delete" ,
										type: "POST",
										data: {del : del_temp},
										dataType: "text" ,
										success: function(data){
											if (data) {
												location.reload();
											}
											else{
												swal("錯誤!", "刪除時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}

	  		if(document.getElementById("del_ppt")){
				document.getElementById("del_ppt").onclick = function(){
					var del_file_data = this.dataset.a;

					swal({	title: "刪除文件?",
							text: "按下刪除後文件不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "刪除!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(del){
								if (del) {
									$.ajax({
										url: <?php echo '"' . base_url() . "Ch4/deleteFile_ppt" . '"';?> ,
										type: "POST",
										data: {del_file : del_file_data},
										dataType: "text" ,
										success: function(data){
											if (data) {
												//swal("已刪除!", "您的文件已經刪除。", "success");
												location.reload();
											}
											else{
												swal("錯誤!", "刪除時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
	  		}

			if(document.getElementById("del_img")){

				document.getElementById("del_img").onclick = function(){
					var del_file_data = this.dataset.a;

					swal({	title: "刪除PPT文件?",
							text: "按下刪除後文件不可復原",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "刪除!",
							cancelButtonText: "取消",
							closeOnConfirm: false,
							closeOnCancel: false },
							function(del){
								if (del) {
									$.ajax({
										url: <?php echo '"' . base_url() . "Ch4/deleteFile_img" . '"';?> ,
										type: "POST",
										data: {del_file : del_file_data},
										dataType: "text" ,
										success: function(data){
											if (data) {
												//swal("已刪除!", "您的文件已經刪除。", "success");
												location.reload();
											}
											else{
												swal("錯誤!", "刪除時發生錯誤!", "error");
											}
										}
									});
								}
								else {
									swal("已取消!", "您的文件是安全的", "error");
								}
							}
					);
				}
			}



			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'			: {},
				'.chosen-select-deselect'	: {allow_single_deselect:true},
				'.chosen-select-no-single'	: {disable_search_threshold:10},
				'.chosen-select-no-results'	: {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'		: {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}

			if(document.querySelector("#btn_fm_other")){
				document.querySelector("#btn_fm_other").onclick = function(){
					var fm_other_data = document.querySelector("#fm_other").value;
					if(fm_other_data){
						$.ajax({
							url: "<?php echo base_url(); ?>Ch4/addFm_other" ,
							type: "POST" ,
							data: {
								fm_other : fm_other_data
							} ,
							dataType: "text" ,
							success: function(data){
								if(data){
									swal({
										title: "成功",
										text: "儲存成功",
										type: "success",
										confirmButtonText: "確定",
									});
								} else {
									swal({
										title: "警告",
										text: "標題或內文不能為空",
										type: "error",
										confirmButtonText: "確定",
										animation: "slide-from-top"
									});
								}
							}
						});
					 } else {
						swal({
							title: "警告",
							text: "標題或內文不能為空",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				}
			}

			if(document.querySelector("#save_comment")){
				document.querySelector("#save_comment").onclick = function(){
					var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch4/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data){
							swal({
								title: "成功",
								text: "已儲存",
								type: "success",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
