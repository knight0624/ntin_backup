<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 貳、文獻查證</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
										<div class="form-group">
											<?php echo $ch2_text; ?>
										</div>
									</div>
								</div>
								
								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch1");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch3");?>" class="btn btn-default">下一步</a>
								</div>

							</div>
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2>老師評語</h2>
								</div>
								<?php echo $comment ?>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>

		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>

		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>

			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'			: {},
				'.chosen-select-deselect'	: {allow_single_deselect:true},
				'.chosen-select-no-single'	: {disable_search_threshold:10},
				'.chosen-select-no-results'	: {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'		: {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}
			if(document.querySelector("#btn_lit")){
				document.querySelector("#btn_lit").onclick = function(){
					var literature_text = document.querySelector("#Literature").value;
					if(literature_text != null){
						$.ajax({
							url: "<?php echo base_url(); ?>Ch2/editLiterature" ,
							type: "POST" ,
							data: {
								literature_text : literature_text
							} ,
							dataType: "text" ,
							success: function(data){
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							}
						});
					 } else {
						swal({
							title: "警告",
							text: "標題或內文不能為空",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				}
			}

			if(document.querySelector("#save_comment")){
				document.querySelector("#save_comment").onclick = function(){
					var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch2/editComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data){
							swal({
								title: "成功",
								text: "已儲存",
								type: "success",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
