<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
			.btn-group{
				margin-top: 20px;
			}
			.upload-group{
				margin: 0px auto;
				text-align: center;
			}
			.upload-group div{
				display: inline-block;
				vertical-align: top;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 拾、家庭資源</h2>
								</div>
								<div class="ibox-content">
									<h3>一、家庭內在資源　(FAMLIS)</h3>
									<br>
									<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '<div class="form-group">'.
														'<div class="col-sm-12">'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="financial_sup" onclick="show_textarea(\'financial_sup\')" '.(!empty($DB_all['fr_financial_sup']) ? 'checked' : '').'>　財力支持　(F；Financial Support)：　指金錢、健康保險或投資獲利等。需評估誰是家中主要經濟提供者？家庭成員之醫療費用由誰支付？</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="financial_sup" name="financial_sup" '.(!empty($DB_all['fr_financial_sup']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_financial_sup'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="advocacy_sup" onclick="show_textarea(\'advocacy_sup\')" '.(!empty($DB_all['fr_advocacy_sup']) ? 'checked' : '').'>　精神支持　(A；Advocacy)：　家庭發生壓力事件或困難時，成員間是否能互相提供心理上的支持或鼓勵等。對於獨居老人，家中的寵物往往是重要的精神支持來源。</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="advocacy_sup" name="advocacy_sup" '.(!empty($DB_all['fr_advocacy_sup']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_advocacy_sup'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="medical_management" onclick="show_textarea(\'medical_management\')" '.(!empty($DB_all['fr_medical_management']) ? 'checked' : '').'>　醫療處置　(M；Medical Management)：　家中有人生病時，如何因應此問題？是否有照護人力？是否能尋求合適的就醫管道？</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="medical_management" name="medical_management" '.(!empty($DB_all['fr_medical_management']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_medical_management'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="love" onclick="show_textarea(\'love\')" '.(!empty($DB_all['fr_love']) ? 'checked' : '').'>　愛　(L；Love)：　愛的最重要泉源來自家庭，家人的互相關愛與扶持是人類成長與心靈滋養不可或缺的。</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="love" name="love" '.(!empty($DB_all['fr_love']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_love'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="IOE" onclick="show_textarea(\'IOE\')" '.(!empty($DB_all['fr_ioe']) ? 'checked' : '').'>　資訊或教育　(I；Information or Education)：　通常愈是懂得獲取資訊來源及家庭成員教育程度愈高者，其面對壓力與解決問題的問題則愈好。</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="IOE" name="IOE" '.(!empty($DB_all['fr_ioe']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_ioe'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="structure_sup" onclick="show_textarea(\'structure_sup\')" '.(!empty($DB_all['fr_structure_sup']) ? 'checked' : '').'>　結構支持　(S；Structure Support)：　包括硬體與軟體結構，硬體者指住家環境的安排與設計是否能滿足家庭成員的需求，如家有失能者，是否設置扶手或改變馬桶高度等。軟體者指角色調整或溝通方式等改變，例如家中的太太住院時，為人先生者是否能兼俱母職？</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="structure_sup" name="structure_sup" '.(!empty($DB_all['fr_structure_sup']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_structure_sup'].'</textarea>'.
																'</div>'.
															'</div>'.
														'</div>'.
													'</div>'.
													'<div class="form-group">'.
														'<div class="col-sm-2">'.
															'<button class="btn btn-primary" id="save_FAMLIS">儲存</button>'.
														'</div>'.
													'</div>';
											break;

											case "2" :
											case "6" :
												echo '<div class="form-group">'.
														'<div class="col-sm-12">'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="financial_sup" onclick="show_textarea(\'financial_sup\')" '.(!empty($DB_all['fr_financial_sup']) ? 'checked' : '').' disabled>　財力支持　(F；Financial Support)：　指金錢、健康保險或投資獲利等。需評估誰是家中主要經濟提供者？家庭成員之醫療費用由誰支付？</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="financial_sup" name="financial_sup" '.(!empty($DB_all['fr_financial_sup']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_financial_sup'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="advocacy_sup" onclick="show_textarea(\'advocacy_sup\')" '.(!empty($DB_all['fr_advocacy_sup']) ? 'checked' : '').' disabled>　精神支持　(A；Advocacy)：　家庭發生壓力事件或困難時，成員間是否能互相提供心理上的支持或鼓勵等。對於獨居老人，家中的寵物往往是重要的精神支持來源。</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="advocacy_sup" name="advocacy_sup" '.(!empty($DB_all['fr_advocacy_sup']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_advocacy_sup'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="medical_management" onclick="show_textarea(\'medical_management\')" '.(!empty($DB_all['fr_medical_management']) ? 'checked' : '').' disabled>　醫療處置　(M；Medical Management)：　家中有人生病時，如何因應此問題？是否有照護人力？是否能尋求合適的就醫管道？</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="medical_management" name="medical_management" '.(!empty($DB_all['fr_medical_management']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_medical_management'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="love" onclick="show_textarea(\'love\')" '.(!empty($DB_all['fr_love']) ? 'checked' : '').' disabled>　愛　(L；Love)：　愛的最重要泉源來自家庭，家人的互相關愛與扶持是人類成長與心靈滋養不可或缺的。</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="love" name="love" '.(!empty($DB_all['fr_love']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_love'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="IOE" onclick="show_textarea(\'IOE\')" '.(!empty($DB_all['fr_ioe']) ? 'checked' : '').' disabled>　資訊或教育　(I；Information or Education)：　通常愈是懂得獲取資訊來源及家庭成員教育程度愈高者，其面對壓力與解決問題的問題則愈好。</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="IOE" name="IOE" '.(!empty($DB_all['fr_ioe']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_ioe'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="structure_sup" onclick="show_textarea(\'structure_sup\')" '.(!empty($DB_all['fr_structure_sup']) ? 'checked' : '').' disabled>　結構支持　(S；Structure Support)：　包括硬體與軟體結構，硬體者指住家環境的安排與設計是否能滿足家庭成員的需求，如家有失能者，是否設置扶手或改變馬桶高度等。軟體者指角色調整或溝通方式等改變，例如家中的太太住院時，為人先生者是否能兼俱母職？</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="structure_sup" name="structure_sup" '.(!empty($DB_all['fr_structure_sup']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_structure_sup'].'</p>'.
																'</div>'.
															'</div>'.
														'</div>'.
													'</div>';
											break;
										}
									?>
									</div>
								</div>
								<div class="ibox-content">
									<h3>二、家庭外在資源　(SCREEEM)</h3>
									<br>
									<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '<div class="form-group">'.
														'<div class="col-sm-12">'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="community" onclick="show_textarea(\'community\')" '.(!empty($DB_all['fr_community']) ? 'checked' : '' ).'>　社會資源　(S；Social Resources)：　家庭以外的親戚朋友或是相關福利機構是否能適時提供必要的協助等？</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="community" name="community" '.(!empty($DB_all['fr_community']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_community'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="culture" onclick="show_textarea(\'culture\')" '.(!empty($DB_all['fr_culture']) ? 'checked' : '' ).'>　文化資源　(C；Culture Resources)：　包含圖書館，文化中心或相關的畫展、戲劇、音樂會等活動？</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="culture" name="culture" '.(!empty($DB_all['fr_culture']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_culture'].'</textarea>'.
															'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="religion" onclick="show_textarea(\'religion\')" '.(!empty($DB_all['fr_religion']) ? 'checked' : '' ).'>　宗教資源　(R；Religious Resources)：　是否有宗教團體或場所，能提供家庭心靈的需要？</label>'.
															'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="religion" name="religion" '.(!empty($DB_all['fr_religion']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_religion'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="income" onclick="show_textarea(\'income\')" '.(!empty($DB_all['fr_income']) ? 'checked' : '' ).'>　經濟資源　(E；Economic Resources)：　家庭有經濟困難時，是否有相關的人或機構，能提供資助以因應最基本的生活需要？</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="income" name="income" '.(!empty($DB_all['fr_income']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_income'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="education" onclick="show_textarea(\'education\')" '.(!empty($DB_all['fr_education']) ? 'checked' : '' ).'>　教育資源　(E；Education Resources)：　是否有正式與非正式的學習管道，以獲取新知？</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="education" name="education" '.(!empty($DB_all['fr_education']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_education'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="surrounding" onclick="show_textarea(\'surrounding\')" '.(!empty($DB_all['fr_surrounding']) ? 'checked' : '' ).'>　環境資源　(E；Environmental Resources)：　日常生活的活動空間是否符合安全衛生的條件？</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="surrounding" name="surrounding" '.(!empty($DB_all['fr_surrounding']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_surrounding'].'</textarea>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="medical" onclick="show_textarea(\'medical\')" '.(!empty($DB_all['fr_medical']) ? 'checked' : '' ).'>　醫療資源　(M；Medical Resources)：　住家附近是否有符合人類三級預防健康需要的醫療設施？是否方便利用等？</label>'.
																'<div class="col-sm-12">'.
																	'<textarea rows ="20" class="form-control" id="medical" name="medical" '.(!empty($DB_all['fr_medical']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_medical'].'</textarea>'.
																'</div>'.
															'</div>'.
														'</div>'.
													'</div>'.
													'<div class="form-group">'.
														'<div class="col-sm-2">'.
															'<button class="btn btn-primary" id="save_SCREEEM">儲存</button>'.
														'</div>'.
													'</div>';
											break;

											case "2" :
											case "6" :
												echo '<div class="form-group">'.
														'<div class="col-sm-12">'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="community" onclick="show_textarea(\'community\')" '.(!empty($DB_all['fr_community']) ? 'checked' : '' ).' disabled>　社會資源　(S；Social Resources)：　家庭以外的親戚朋友或是相關福利機構是否能適時提供必要的協助等？</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="community" name="community" '.(!empty($DB_all['fr_community']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_community'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="culture" onclick="show_textarea(\'culture\')" '.(!empty($DB_all['fr_culture']) ? 'checked' : '' ).' disabled>　文化資源　(C；Culture Resources)：　包含圖書館，文化中心或相關的畫展、戲劇、音樂會等活動？</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="culture" name="culture" '.(!empty($DB_all['fr_culture']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_culture'].'</p>'.
															'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="religion" onclick="show_textarea(\'religion\')" '.(!empty($DB_all['fr_religion']) ? 'checked' : '' ).' disabled>　宗教資源　(R；Religious Resources)：　是否有宗教團體或場所，能提供家庭心靈的需要？</label>'.
															'<div class="col-sm-12">'.
																	'<p class="form-control" id="religion" name="religion" '.(!empty($DB_all['fr_religion']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_religion'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="income" onclick="show_textarea(\'income\')" '.(!empty($DB_all['fr_income']) ? 'checked' : '' ).' disabled>　經濟資源　(E；Economic Resources)：　家庭有經濟困難時，是否有相關的人或機構，能提供資助以因應最基本的生活需要？</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="income" name="income" '.(!empty($DB_all['fr_income']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_income'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="education" onclick="show_textarea(\'education\')" '.(!empty($DB_all['fr_education']) ? 'checked' : '' ).' disabled>　教育資源　(E；Education Resources)：　是否有正式與非正式的學習管道，以獲取新知？</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="education" name="education" '.(!empty($DB_all['fr_education']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_education'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="surrounding" onclick="show_textarea(\'surrounding\')" '.(!empty($DB_all['fr_surrounding']) ? 'checked' : '' ).' disabled>　環境資源　(E；Environmental Resources)：　日常生活的活動空間是否符合安全衛生的條件？</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="surrounding" name="surrounding" '.(!empty($DB_all['fr_surrounding']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_surrounding'].'</p>'.
																'</div>'.
															'</div>'.
															'<div class="col-sm-12">'.
																'<label class="checkbox-inline"><input type="checkbox" class="medical" onclick="show_textarea(\'medical\')" '.(!empty($DB_all['fr_medical']) ? 'checked' : '' ).' disabled>　醫療資源　(M；Medical Resources)：　住家附近是否有符合人類三級預防健康需要的醫療設施？是否方便利用等？</label>'.
																'<div class="col-sm-12">'.
																	'<p class="form-control" id="medical" name="medical" '.(!empty($DB_all['fr_medical']) ? 'enabled' : 'disabled' ).'>'.$DB_all['fr_medical'].'</p>'.
																'</div>'.
															'</div>'.
														'</div>'.
													'</div>';
											break;
										}
									?>
									</div>
								</div>
								<div class="ibox-content">
									<h3>三、Eco-Map</h3>
									<p>　　家庭外在資源除了上述的評估方向外，另一常被用來評估的工具為 Hartman 所提出的 Eco-Map ，他是評估家庭外在資源及其互動關係，又稱家庭生態圖，藉此可了解家庭與外界環境及資源間的相關性，並顯示其社會支持或資源利用的多寡，是一種家庭的外在結構。</p>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '
													<div style="text-align: center;">'.
															(!empty($DB_all['fr_eco_map_img']) ? '<img src="'.base_url().'uploads/img/'.$DB_all['fr_eco_map_img'].'" style="display:block; margin:auto; height:auto; max-width:50%;">' : '<img src="'.base_url().'dist/img/Eco_Map.png" style="display:block; margin:auto; height:auto; max-width:50%;">' ).
														'<P class="text-center">Eco-Map範例</P>'.
													'</div>'.
													'<div class="upload-group">'.
														'<div>'.
															'<a href="'.base_url().'download/Eco-Map範例.pptx" download>'.
																 '<img src="'.base_url().'dist/img/download_example.png" style="width:150px;">'.
															'</a>'.
														'</div>'.
														'<div>'.
															form_open_multipart('Ch10/Eco_Map_ppt_upload').
															'<input type="file" name="userfile_ppt" id="Eco_Map_ppt" style="display:none;">'.
															'<img src="'.base_url().'dist/img/select_ppt.png" id="Eco_Map_ppt_new" value="選擇 PPT 檔" style="width:150px;">'.
															'<input type="submit" id="updata_Eco_Map_ppt" style="display:none;">'.
															'<img src="'.base_url().'dist/img/upload_ppt.png" id="updata_Eco_Map_ppt_new" value="上傳" style="width:150px;">'.
															'</form>'.
															'<div class="btn-group">'.
																( ! empty($DB_all['fr_eco_map_ppt']) ?
																	'<a class="btn btn-primary btn-xs" href="'.base_url().'uploads/ppt/'.$DB_all['fr_eco_map_ppt'].'" download style="margin:auto;">下載：'.$DB_all['fr_eco_map_ppt'].'</a>'.
																	'<a class="btn btn-danger btn-xs delete_file" id="delete_file" data-file="'.$DB_all['fr_eco_map_ppt'].'"><i class="fa fa-trash"></i></a>'
																	: ''
																).
															'</div>'.
														'</div>'.
														'<div>'.
															form_open_multipart('Ch10/Eco_Map_img_upload').
															'<input type="file" name="userfile_img" id="Eco_Map_img" style="display:none;">'.
															'<img src="'.base_url().'dist/img/select_img.png" id="Eco_Map_img_new" value="選擇圖片檔" style="width:150px;">'.
															'<input type="submit" id="updata_Eco_Map_img" style="display:none;">'.
															'<img src="'.base_url().'dist/img/upload_img.png" id="updata_Eco_Map_img_new" value="上傳" style="width:150px;">'.
															'</form>'.
															'<div class="btn-group">'.
																( ! empty($DB_all['fr_eco_map_img']) ?
																	'<a class="btn btn-primary btn-xs" href="'.base_url().'uploads/img/'.$DB_all['fr_eco_map_img'].'" download style="margin:auto;">下載：'.$DB_all['fr_eco_map_img'].'</a>'.
																	'<a class="btn btn-danger btn-xs delete_file" id="delete_file" data-file="'.$DB_all['fr_eco_map_img'].'"><i class="fa fa-trash"></i></a>'
																	: ''
																).
															'</div>'.
														'</div>'.
													'</div>'.
													'<div align="center">'.
														'<p class="text-danger">檔案大小限制為8MB</p>'.
													'</div>';
											break;

											case "2" :
											case "6" :
												echo '<div style="text-align: center;">'.
														(!empty($DB_all['fr_eco_map_img']) ? '<img src="'.base_url().'uploads/img/'.$DB_all['fr_eco_map_img'].'" style="display:block; margin:auto; height:auto; max-width:50%;">' : '<img src="'.base_url().'dist/img/Eco_Map.png" style="display:block; margin:auto; height:auto; max-width:50%;">' ).
														'<P class="text-center">Eco-Map範例</P>'.
													'</div>'.
													'<div class="upload-group">'.
														'<div>'.
															'<div class="btn-group">'.
																( ! empty($DB_all['fr_eco_map_ppt']) ?
																	'<a class="btn btn-primary btn-xs" href="'.base_url().'uploads/ppt/'.$DB_all['fr_eco_map_ppt'].'" download style="margin:auto;">下載：'.$DB_all['fr_eco_map_ppt'].'</a>'
																: ''
																).
															'</div>'.
														'</div>'.
														'<div>'.
															'<div class="btn-group">'.
																( ! empty($DB_all['fr_eco_map_img']) ?
																	'<a class="btn btn-primary btn-xs" href="'.base_url().'uploads/ppt/'.$DB_all['fr_eco_map_img'].'" download style="margin:auto;">下載：'.$DB_all['fr_eco_map_img'].'</a>'
																	: ''
																).
															'</div>'.
														'</div>'.
													'</div>';
											break;
										}
									?>
								</div>
								<div class="ibox-content">
									<h3>四、補充</h3>
									<div class="form-horizontal">
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case "1" :
											case "3" :
												echo '<div class="form-group">'.
														'<div class="col-sm-12">'.
															'<textarea rows ="20" class="form-control" id="supplement">'.$DB_all['fr_supplement'].'</textarea>'.
														'</div>'.
													'</div>'.
													'<div class="form-group">'.
														'<div class="col-sm-2">'.
															'<button class="btn btn-primary" id="save_supplement">儲存</button>'.
														'</div>'.
													'</div>';
											break;

											case "2" :
											case "6" :
												echo '<div class="form-group">'.
														'<div class="col-sm-12">'.
															'<p class="form-control" id="supplement">'.$DB_all['fr_supplement'].'</p>'.
														'</div>'.
													'</div>';
											break;
										}
									?>
									</div>
 								</div>
								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch9");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch11");?>" class="btn btn-default">下一步</a>
								</div>
							</div>

							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2>老師評語</h2>
							 	</div>
							 	<div class="ibox-content">
							 	<?php
							 		// 1 學生
									// 2 老師
									// 3 admin
									switch ($permission) {
										case "1" :
										case "6" :
											echo '
												<div class="form-horizontal">
													<p class="text-danger form-control">'.$DB_all["comment"].'</p>
												</div>';
										break;
										case "2" :
										case "3" :
											echo '
												<div class="form-horizontal">
													<textarea class="form-control" name="comment" rows ="10" placeholder="老師評語">'.$DB_all['comment'].'</textarea>
													<div class="form-group">
														<div class="col-sm-10"><br>
															<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>
														</div>
													</div>
												</div>';
										break;
									}
								?>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'           : {},
				'.chosen-select-deselect'  : {allow_single_deselect:true},
				'.chosen-select-no-single' : {disable_search_threshold:10},
				'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}

			/**
			 * Eco_Map.img檔案上傳按鈕
			 */
			if (document.getElementById("Eco_Map_img_new")) {
				document.getElementById("Eco_Map_img_new").onclick = function() {
					document.getElementById("Eco_Map_img").click(true);
				}
				document.getElementById("updata_Eco_Map_img_new").onclick = function() {
					document.getElementById("updata_Eco_Map_img").click(true);
				}
			}

			/**
			 * Eco_Map.ppt/pptx檔案上傳按鈕
			 */
			if (document.getElementById("Eco_Map_ppt_new")) {
				document.getElementById("Eco_Map_ppt_new").onclick = function() {
					document.getElementById("Eco_Map_ppt").click(true);
				}
				document.getElementById("updata_Eco_Map_ppt_new").onclick = function() {
					document.getElementById("updata_Eco_Map_ppt").click(true);
				}
			}

			/**
			 * 刪除文件
			 */
			var file_array = document.querySelectorAll(".delete_file")
			for (var i = 0; i < file_array.length; i++) {
				file_array[i].onclick = function() {
					// console.log(this.dataset.file);
					var file_name = this.dataset.file
					swal({	title: "刪除文件?",
						text: "按下刪除後文件不可復原",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "刪除!",
						cancelButtonText: "取消",
						closeOnConfirm: false,
						closeOnCancel: false },
						function(isConfirm) {
							if (isConfirm) {
								$.ajax({
									url: "<?php echo base_url(); ?>Ch10/Delete_File" ,
									type: "POST" ,
									data: {
										file_name : file_name
									} ,
									dataType: "text" ,
									success: function(data){
										if (data) {
											location.reload();
										}
										else{
											swal("錯誤!", "刪除時發生錯誤!", "error");
										}
									}
								});
							}
							else {
								swal("已取消!", "您的文件是安全的", "error");
							}
						}
					);
				}
			}

			/**
			 * 有勾勾才能輸入
			 */
			function show_textarea(text_id) {
				if(document.querySelector('.'+(text_id)).checked != true) {
					document.querySelector('#'+(text_id)).disabled = true;
				} else if(document.querySelector('.'+(text_id)).checked != false) {
					document.querySelector('#'+(text_id)).disabled = false;
				}
			}

			/**
			 * FAMLIS
			 */
			if(document.querySelector("#save_FAMLIS")) {
				document.querySelector("#save_FAMLIS").onclick = function() {
					var FAMLIS_result = {};
					FAMLIS_result.financial_sup      = document.querySelector("#financial_sup").value;
					FAMLIS_result.advocacy_sup       = document.querySelector("#advocacy_sup").value;
					FAMLIS_result.medical_management = document.querySelector("#medical_management").value;
					FAMLIS_result.love               = document.querySelector("#love").value;
					FAMLIS_result.IOE                = document.querySelector("#IOE").value;
					FAMLIS_result.structure_sup      = document.querySelector("#structure_sup").value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch10/FAMLIS" ,
						type: "POST" ,
						data: {
							FAMLIS_result : FAMLIS_result
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}

			/**
			 * SCREEEM
			 */
			if(document.querySelector("#save_SCREEEM")) {
				document.querySelector("#save_SCREEEM").onclick = function(){
					var SCREEEM_result = {};
					SCREEEM_result.community   = document.querySelector("#community").value;
					SCREEEM_result.culture     = document.querySelector("#culture").value;
					SCREEEM_result.religion    = document.querySelector("#religion").value;
					SCREEEM_result.income      = document.querySelector("#income").value;
					SCREEEM_result.education   = document.querySelector("#education").value;
					SCREEEM_result.surrounding = document.querySelector("#surrounding").value;
					SCREEEM_result.medical     = document.querySelector("#medical").value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch10/SCREEEM" ,
						type: "POST" ,
						data: {
							SCREEEM_result : SCREEEM_result
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}

			/**
			 * 補充
			 */
			if(document.querySelector("#save_supplement")) {
				document.querySelector("#save_supplement").onclick = function() {
					var supplement = document.querySelector("#supplement").value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch10/Save_Supplement" ,
						type: "POST" ,
						data: {
							supplement : supplement
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}

			/**
			 * 儲存 老師評語
			 */
			if(document.querySelector("#save_comment")) {
				document.querySelector("#save_comment").onclick = function() {
					var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch10/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data) {
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
