<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 拾壹、護理問題與計畫</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
											<?php
												switch ($permission) {
													case '1':
													case '3':
														echo '
															<div class="form-group">
																<label class="col-sm-12">一、護理問題：</label>
																<div class="col-sm-12">
																	<textarea class="form-control" rows="20" placeholder="例：一、健康問題　二、衛生問題　三、情緒問題" id="care_problem">'.$care_problem.'</textarea>
																</div>
															</div>
															<hr>
															<div class="form-group">
																<label class="col-sm-12">二、護理計畫：</label>
																<div class="col-sm-12">
																	<textarea class="form-control" rows="20" placeholder="護理計畫" id="care_plan">'.$care_plan.'</textarea>
																</div>
															</div>
															<hr>
															<div class="form-group">
																<label class="col-sm-12">三、補充：</label>
																<div class="col-sm-12">
																	<textarea class="form-control" rows="20" placeholder="補充" id="care_supplement">'.$care_supplement.'</textarea>
																</div>
															</div>
															<hr>
															<div class="form-group">
																<div class="col-sm-12">
																	<button class="btn btn-primary" id="btn_care">儲存</button>
																</div>
															</div>';
													break;

													case '2':
													case '6':
														echo '
															<div class="form-group">
																<label class="col-sm-12">一、護理問題：</label>
																<div class="col-sm-12">
																	<p class="form-control" id="care_problem">'.$care_problem.'</p>
																</div>
															</div>
															<hr>
															<div class="form-group">
																<label class="col-sm-12">二、護理計畫：</label>
																<div class="col-sm-12">
																	<p class="form-control" id="care_plan">'.$care_plan.'</p>
																</div>
															</div>
															<hr>
															<div class="form-group">
																<label class="col-sm-12">三、補充：</label>
																<div class="col-sm-12">
																	<p class="form-control" id="care_supplement">'.$care_supplement.'</p>
																</div>
															</div>';
													break;
												}
											?>
									</div>
								</div>

								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch10");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch12");?>" class="btn btn-default">下一步</a>
								</div>
							</div>
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2>老師評語</h2>
							</div>
							<div class="ibox-content">
								<div class="form-horizontal">
										<?php
											switch ($permission) {
												case '1':
												case '6':
													echo '<p class="text-danger form-control">'.$comment.'</p>';
												break;

												case '2':
												case '3':
													echo '
														<textarea class="form-control" name="comment" rows="10" placeholder="老師評語">'.$comment.'</textarea>
														<div class="form-group">
															<div class="col-sm-12"><br>
																<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>
															</div>
														</div>';
												break;
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>

		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>

		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			/**
			 * [onclick Foreword AJAX]
			 * @return {[type]} [description]
			 */
			 if(document.querySelector("#btn_care")){
				document.querySelector("#btn_care").onclick = function(){
					var care_problem = document.querySelector("#care_problem").value;
					var care_plan = document.querySelector("#care_plan").value;
					var care_supplement = document.querySelector("#care_supplement").value;
					if(care_supplement == null){
						care_supplement = "";
					}
					if(care_problem != null && care_plan != null){
						$.ajax({
							url: "<?php echo base_url(); ?>Ch11/editCare" ,
							type: "POST" ,
							data: {
								care_problem : care_problem ,
								care_plan : care_plan ,
								care_supplement : care_supplement ,

							} ,
							dataType: "text" ,
							success: function(data){
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							}
						});
					 } else {
						swal({
							title: "警告",
							text: "有內容尚未輸入!!",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}

				}
			}

			/**
			 * 儲存 老師評語
			 */
			if(document.querySelector("#save_comment")){
				document.querySelector("#save_comment").onclick = function(){
					var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch11/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							} else {
								swal({
									title: "失敗",
									text: "再次檢查",
									type: "error",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
