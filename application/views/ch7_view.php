<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
			.modal {
				text-align: center;
				padding: 0!important;
			}

			.modal:before {
				content: '';
				display: inline-block;
				height: 100%;
				vertical-align: middle;
				margin-right: -4px;
			}

			.modal-dialog {
				display: inline-block;
				text-align: left;
				vertical-align: middle;
				width: 70%;
			}
			.modal-body {
				padding: 15px;
			}
			.showSweetAlert[data-animation=slide-from-top]{
				animation: slideFromTop 0.5s;
			}
			.file-zoom-fullscreen .modal-dialog{
				position: static;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h2><?php echo $hw_name ?> - 柒、家庭發展 </h2>
								</div>
								<div class="ibox-content">

									<h3>一、Duvall & Miller家庭發展實務現況</h3>
									<!-- 把fd_tasks欄位拆開，加入$tasks -->
									<?php
										$tasks_All = explode(",",$DB_all['fd_tasks']);
										for ($i=0; $i < 29; $i++)
											$tasks[$i] = 0;
										for ($i=0; $i < count($tasks_All); $i++)
											$tasks[$tasks_All[$i]-1] = $tasks_All[$i];
									?>
									<table class="table table-bordered" border="1" >
										<tr>
											<td></td>
											<td>家庭發展階段</td>
											<td>家庭發展任務</td>
											<td>重要健康事件</td>
										</tr>
										<tr>
											<td>
												<input type="radio" id="periods_1" name="periods" value="1" class="periods_class" onclick="checkRadio(0)" <?php if ($DB_all["fd_periods"] == 1) echo "checked"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>
												<div style="width: 10px; word-wrap:break-word;">新婚期</div>
											</td>
											<td>
												<input type="radio" id="stages_1" name="stages" value="1" class="stages_class" onclick="checkRadio(1)" <?php if ($DB_all["fd_stages"] == 1) echo "checked"; else echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>新婚家庭
											</td>
											<td>
												<input type="checkbox" id="tasks_1" value="1" class="tasks_class" <?php if ($tasks[0] == 1) echo "checked"; else if ($DB_all["fd_stages"] != 1) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 建立彼此滿意的新婚生活。<br>
												<input type="checkbox" id="tasks_2" value="2" class="tasks_class" <?php if ($tasks[1] == 2) echo "checked"; else if ($DB_all["fd_stages"] != 1) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 與親戚家族建立和諧關係。<br>
												<input type="checkbox" id="tasks_3" value="3" class="tasks_class" <?php if ($tasks[2] == 3) echo "checked"; else if ($DB_all["fd_stages"] != 1) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 懷孕生子之家庭計畫。<br>
											</td>
											<td>
												<ul>
  													<li>性與婚姻角色的調適與溝通。</li>
  													<li>家庭計畫教育與諮商。孕前教育。</li>
  													<li>性病防治。</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td rowspan="2">
												<input type="radio" id="periods_2" name="periods" value="2" class="periods_class" onclick="checkRadio(0)" <?php if ($DB_all["fd_periods"] == 2) echo "checked"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>
												<div style="width: 10px; word-wrap:break-word;">成員增加期</div>
											</td>
											<td>
												<input type="radio" id="stages_2" name="stages" value="2" class="stages_class" onclick="checkRadio(1)" <?php if ($DB_all["fd_stages"] == 2) echo "checked"; else if ($DB_all["fd_stages"] != 3) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>生育期家庭
											</td>
											<td>
												<input type="checkbox" id="tasks_4" value="4" class="tasks_class" <?php if ($tasks[3] == 4) echo "checked"; else if ($DB_all["fd_stages"] != 2) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 建立新寶寶到來的穩定家庭。<br>
												<input type="checkbox" id="tasks_5" value="5" class="tasks_class" <?php if ($tasks[4] == 5) echo "checked"; else if ($DB_all["fd_stages"] != 2) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 調節家中因新成員加入所產生的衝突。<br>
												<input type="checkbox" id="tasks_6" value="6" class="tasks_class" <?php if ($tasks[5] == 6) echo "checked"; else if ($DB_all["fd_stages"] != 2) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 維持婚姻關係及協調二代之間對育兒方式的衝突。<br>
											</td>
											<td>
												<ul>
													<li>生產教育。</li>
													<li>產兒保健(預防接種、兒童發展、早期健康問題發現與處理)。</li>
													<li>親子關係(虐待或忽略的預防)。</li>
													<li>家庭計畫。</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												<input type="radio" id="stages_3" name="stages" value="3" class="stages_class" onclick="checkRadio(1)" <?php if ($DB_all["fd_stages"] == 3) echo "checked"; else if ($DB_all["fd_stages"] != 2) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>學齡前期家庭
											</td>
											<td>
												<input type="checkbox" id="tasks_7" value="7" class="tasks_class" <?php if ($tasks[6] == 7) echo "checked"; else if ($DB_all["fd_stages"] != 3) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 滿足家庭成員足夠的居住、空間、隱私及安全。<br>
												<input type="checkbox" id="tasks_8" value="8" class="tasks_class" <?php if ($tasks[7] == 8) echo "checked"; else if ($DB_all["fd_stages"] != 3) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 提供子女社會化。<br>
												<input type="checkbox" id="tasks_9" value="9" class="tasks_class" <?php if ($tasks[8] == 9) echo "checked"; else if ($DB_all["fd_stages"] != 3) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 能調和另一新寶寶及較大子女們的需求。<br>
												<input type="checkbox" id="tasks_10" value="10" class="tasks_class" <?php if ($tasks[9] == 10) echo "checked"; else if ($DB_all["fd_stages"] != 3) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 維持健康的婚姻、親子、延展家庭及社區的關係。<br>
											</td>
											<td>
												<ul>
													<li>傳染病預防。</li>
													<li>意外事件預防。</li>
													<li>婚姻關係。</li>
													<li>子女間的競爭。</li>
													<li>親職教育。</li>
													<li>雙親良好生活型態的建立以影響下一代。</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td rowspan="2">
												<input type="radio" id="periods_3" name="periods" value="3" class="periods_class" onclick="checkRadio(0)" <?php if ($DB_all["fd_periods"] == 3) echo "checked"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>
												<div style="width: 10px; word-wrap:break-word;">成員擴散期</div>
											</td>
											<td>
												<input type="radio" id="stages_4" name="stages" value="4" class="stages_class" onclick="checkRadio(1)" <?php if ($DB_all["fd_stages"] == 4) echo "checked"; else if ($DB_all["fd_stages"] != 5) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>學齡期家庭
											</td>
											<td>
												<input type="checkbox" id="tasks_11" value="11" class="tasks_class" <?php if ($tasks[10] == 11) echo "checked"; else if ($DB_all["fd_stages"] != 4) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 子女的社會化，包括促進學校學習、與同儕建立群體關係。<br>
												<input type="checkbox" id="tasks_12" value="12" class="tasks_class" <?php if ($tasks[11] == 12) echo "checked"; else if ($DB_all["fd_stages"] != 4) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 維持滿意的婚姻關係。<br>
												<input type="checkbox" id="tasks_13" value="13" class="tasks_class" <?php if ($tasks[12] == 13) echo "checked"; else if ($DB_all["fd_stages"] != 4) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 滿足家庭成員的健康需要。<br>
											</td>
											<td>
												<ul>
													<li>傳染病、意外事件預防。</li>
													<li>婚姻關係。</li>
													<li>子女學校的學習問題。</li>
													<li>親職教育。</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												<input type="radio" id="stages_5" name="stages" value="5" class="stages_class" onclick="checkRadio(1)" <?php if ($DB_all["fd_stages"] == 5) echo "checked"; else if ($DB_all["fd_stages"] != 4) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>青少年期家庭
											</td>
											<td>
												<input type="checkbox" id="tasks_14" value="14" class="tasks_class" <?php if ($tasks[13] == 14) echo "checked"; else if ($DB_all["fd_stages"] != 5) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 子女發展自由、責任間的平衡，且學得圓融、自主。<br>
												<input type="checkbox" id="tasks_15" value="15" class="tasks_class" <?php if ($tasks[14] == 15) echo "checked"; else if ($DB_all["fd_stages"] != 5) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 開放式的親子溝通。<br>
												<input type="checkbox" id="tasks_16" value="16" class="tasks_class" <?php if ($tasks[15] == 16) echo "checked"; else if ($DB_all["fd_stages"] != 5) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 維持穩定的婚姻關係。<br>
											</td>
											<td>
												<ul>
													<li>健康促進與健康事件危險因子的探討。</li>
													<li>意外事件預防(車禍、運動傷害)。</li>
													<li>子女藥物、喝酒問題。</li>
													<li>子女性教育問題。</li>
													<li>夫妻婚姻關係。</li>
													<li>父母親的健康。</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td rowspan="2">
												<input type="radio" id="periods_4" name="periods" value="4" class="periods_class" onclick="checkRadio(0)" <?php if ($DB_all["fd_periods"] == 4) echo "checked"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>
												<div style="width: 10px; word-wrap:break-word;">獨立期</div>
											</td>
											<td>
												<input type="radio" id="stages_6" name="stages" value="6" class="stages_class" onclick="checkRadio(1)" <?php if ($DB_all["fd_stages"] == 6) echo "checked"; else if ($DB_all["fd_stages"] != 7) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>獨立期家庭
											</td>
											<td>
												<input type="checkbox" id="tasks_17" value="17" class="tasks_class" <?php if ($tasks[16] == 17) echo "checked"; else if ($DB_all["fd_stages"] != 6) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 協助小孩獨立，並迎接下一代的新家庭與成員。<br>
												<input type="checkbox" id="tasks_18" value="18" class="tasks_class" <?php if ($tasks[17] == 18) echo "checked"; else if ($DB_all["fd_stages"] != 6) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 重新整頓與調整夫婦婚姻關係。<br>
												<input type="checkbox" id="tasks_19" value="19" class="tasks_class" <?php if ($tasks[18] == 19) echo "checked"; else if ($DB_all["fd_stages"] != 6) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 協助年老或生病的雙親。<br>
											</td>
											<td>
												<ul>
													<li>親子溝通。</li>
													<li>親職角色的調適。</li>
													<li>雙親的健康。</li>
													<li>夫妻健康問題(慢性病、更年期)。</li>
													<li>健康生活型態的促進。</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												<input type="radio" id="stages_7" name="stages" value="7" class="stages_class" onclick="checkRadio(1)" <?php if ($DB_all["fd_stages"] == 7) echo "checked"; else if ($DB_all["fd_stages"] != 6) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>中年期家庭
											</td>
											<td>
												<input type="checkbox" id="tasks_20" value="20" class="tasks_class" <?php if ($tasks[19] == 20) echo "checked"; else if ($DB_all["fd_stages"] != 7) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 面對空巢期的適應。<br>
												<input type="checkbox" id="tasks_21" value="21" class="tasks_class" <?php if ($tasks[20] == 21) echo "checked"; else if ($DB_all["fd_stages"] != 7) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 面對慢性病，發展健康促進的生活型態。<br>
												<input type="checkbox" id="tasks_22" value="22" class="tasks_class" <?php if ($tasks[21] == 22) echo "checked"; else if ($DB_all["fd_stages"] != 7) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 夫婦婚姻關係的強化。<br>
												<input type="checkbox" id="tasks_23" value="23" class="tasks_class" <?php if ($tasks[22] == 23) echo "checked"; else if ($DB_all["fd_stages"] != 7) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 與上下兩代維持良好且有意義的關係。<br>
											</td>
											<td>
												<ul>
													<li>婚姻關係。</li>
													<li>家庭成員的溝通與關係(與小孩、媳婦、孫子、年老雙親)。</li>
													<li>照顧年老的雙親。</li>
													<li>健康促進。</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												<input type="radio" id="periods_5" name="periods" value="5" class="periods_class" onclick="checkRadio(0)" <?php if ($DB_all["fd_periods"] == 5) echo "checked"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>
												<div style="width: 10px; word-wrap:break-word;">退休死亡期</div>
											</td>
											<td>
												<input type="radio" id="stages_8" name="stages" value="8" class="stages_class" onclick="checkRadio(1)" <?php if ($DB_all["fd_stages"] == 8) echo "checked"; else echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>>老年期家庭
											</td>
											<td>
												<input type="checkbox" id="tasks_24" value="24" class="tasks_class" <?php if ($tasks[23] == 24) echo "checked"; else if ($DB_all["fd_stages"] != 8) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 維持滿意的老年生活方式。<br>
												<input type="checkbox" id="tasks_25" value="25" class="tasks_class" <?php if ($tasks[24] == 25) echo "checked"; else if ($DB_all["fd_stages"] != 8) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 適應退休、收入減少的生活。<br>
												<input type="checkbox" id="tasks_26" value="26" class="tasks_class" <?php if ($tasks[25] == 26) echo "checked"; else if ($DB_all["fd_stages"] != 8) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 維持良好婚姻關係。<br>
												<input type="checkbox" id="tasks_27" value="27" class="tasks_class" <?php if ($tasks[26] == 27) echo "checked"; else if ($DB_all["fd_stages"] != 8) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 面對疾病、死亡、適應喪偶。<br>
												<input type="checkbox" id="tasks_28" value="28" class="tasks_class" <?php if ($tasks[27] == 28) echo "checked"; else if ($DB_all["fd_stages"] != 8) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 維持代代間的家庭聯絡網。<br>
												<input type="checkbox" id="tasks_29" value="29" class="tasks_class" <?php if ($tasks[28] == 29) echo "checked"; else if ($DB_all["fd_stages"] != 8) echo "disabled"; if ($permission == 2 || $permission == 6) echo " disabled"; ?>> 保有個人存在感、包括生命回顧與人生整合。<br>
											</td>
											<td>
												<ul>
													<li>身體功能下降與慢性病。</li>
													<li>經濟壓力。</li>
													<li>社會支持系統與社交隔離。</li>
													<li>退休與老年生活調適。</li>
													<li>面對死亡與喪偶。</li>
													<li>健康促進。</li>
												</ul>
											</td>
										</tr>
									</table>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<div>' .
														'<button type="submit" class="btn btn-primary" id="save_Table">儲存</button>' .
													'</div>';
												break;
										}
									?>
								</div>
								<div class="ibox-content">
									<h3>二、補充</h3>
									<?php
										// 1 學生
										// 2 老師
										// 3 admin
										switch ($permission) {
											case '1':
											case '3':
												echo
													'<textarea class="form-control" name="supple_ment" rows="20">'.$DB_all['fd_supplement'].'</textarea>' .
													'<div>' .
														'<button type="submit" class="btn btn-primary" id="save_Supple_Ment">儲存</button>' .
													'</div>';
												break;
											case '2':
											case '6':
												echo
													'<p class="form-control" name="supple_ment" rows="4">'.$DB_all['fd_supplement'].'</p>';
												break;
										}
									?>
								</div>
								<div class="ibox-content text-center">
									<a href="<?php echo base_url("ch6");?>" class="btn btn-default">上一步</a>
									<a href="<?php echo base_url("ch8");?>" class="btn btn-default">下一步</a>
								</div>
							</div>
							<?php
								// 1 學生
								// 2 老師
								// 3 admin
								switch ($permission) {
									case '1':
									case '6':
										echo
											'<div class="ibox float-e-margins">' .
												'<div class="ibox-title">' .
													'<h2>老師評語</h2>' .
												'</div>' .
												'<div class="ibox-content">' .
													'<div class="form-horizontal">' .
														'<p class="text-danger form-control">'.$DB_all["comment"].'</p>' .
													'</div>' .
												'</div>' .
											'</div>' ;
										break;
									case '2':
									case '3':
										echo
											'<div class="ibox float-e-margins">' .
												'<div class="ibox-title">' .
													'<h2>老師評語</h2>' .
												'</div>' .
												'<div class="ibox-content">' .
													'<div class="form-horizontal">' .
														'<textarea class="form-control" name="comment" rows="10" placeholder="老師評語">'.$DB_all["comment"].'</textarea>' .
														'<div class="form-group">' .
															'<div class="col-sm-10"><br>' .
																'<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>' .
															'</div>' .
														'</div>' .
													'</div>' .
												'</div>' .
											'</div>';
										break;
								}
							?>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			// datepicker
			$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
				startView: 1,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
				startView: 2,
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
				minViewMode: 1,
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true,
				todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
				keyboardNavigation: false,
				forceParse: false,
				autoclose: true
			});

			// chosen
			var config = {
				'.chosen-select'           : {},
				'.chosen-select-deselect'  : {allow_single_deselect:true},
				'.chosen-select-no-single' : {disable_search_threshold:10},
				'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				'.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}

			function checkRadio(f) {
				if (f == 0) {
					var check_stages = [0, 1, 1, 2, 2, 3, 3, 4];
					var check_1 = document.querySelectorAll(".periods_class");
					var check_2 = document.querySelectorAll(".stages_class");
					var check_3 = document.querySelectorAll(".tasks_class");
					for (var i = 2; i <= 3; i++) {
						for (var n = 0; n < eval('check_'+i+'.length'); n++) {
							eval('check_'+i)[n].checked = false;
							eval('check_'+i)[n].disabled = true;
							if (i == 2)
								check_2[n].disabled = !check_1[check_stages[n]].checked;
						}
					}
				}else if (f == 1) {
					var check_tasks = [0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7];
					var check_2 = document.querySelectorAll(".stages_class");
					var check_3 = document.querySelectorAll(".tasks_class");
					for (var i = 0; i < check_3.length; i++) {
						check_3[i].checked = false;
						check_3[i].disabled = !check_2[check_tasks[i]].checked;
					}
				}
			}

			if (document.querySelector("#save_Table")) {
				document.querySelector("#save_Table").onclick = function(){
					var class_1 = document.querySelectorAll(".periods_class");
					var class_2 = document.querySelectorAll(".stages_class");
					var class_3 = document.querySelectorAll(".tasks_class");
					var result = [];
					for (var i = 1; i <= 3; i++){
						for (var n = 0; n < eval('class_' + i + '.length'); n++){
							if (eval('class_'+i)[n].checked){
								if (i == 3)
									result[result.length] = eval('class_'+i)[n].value;
								else
									result[i-1] = eval('class_'+i)[n].value;
							}
						}
					}
					if(result.length >=3){
						$.ajax({
						url: "<?php echo base_url(); ?>Ch7/fdTable" ,
						type: "POST" ,
						data: {
							table_value : JSON.stringify(result)
						} ,
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
					}
					else{
						swal({
							title: "警告",
							text: "有選項尚未勾選",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				}
			}
			if (document.querySelector("#save_Supple_Ment")) {
				document.querySelector("#save_Supple_Ment").onclick = function(){
					var supple_ment = document.getElementsByName("supple_ment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch7/fdSuppleMent" ,
						type: "POST" ,
						data: {
							supple_ment_value : supple_ment
						} ,
						dataType: "text" ,
						success: function(data){
							if (data) {
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						}
					});
				}
			}
			if (document.querySelector("#save_comment")) {
				document.querySelector("#save_comment").onclick = function(){
					var comment = document.getElementsByName("comment")[0].value;
					$.ajax({
						url: "<?php echo base_url(); ?>Ch7/addComment" ,
						type: "POST" ,
						data: {
							comment_value : comment
						} ,
						dataType: "text" ,
						success: function(data){
							swal({
								title: "成功",
								text: "已儲存",
								type: "success",
								confirmButtonText: "確定",
								animation: "slide-from-top"
							});
						}
					});
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
