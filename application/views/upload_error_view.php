	<?php $this->load->view("basic/begin");?>
		<style>
		</style>
	</head>

	<body class="gray-bg">
		<div class="middle-box text-center animated fadeInDown">
			<h1>404</h1>
			<h3 class="font-bold">上傳失敗</h3>
			<div class="error-desc">
				請確認上傳格式、容量是否符合，如有疑問請於上班時間內聯絡平台管理人。
                <div>
                    <hr>
                    <button type="submit" class="btn btn-primary" onclick="javascript:history.back()">回上一頁</button>
                </div>
			</div>
		</div>

		<!-- Mainly scripts -->
		<script src="<?php echo base_url(); ?>dist/js/jquery-2.1.1.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/bootstrap.min.js"></script>
	</body>
</html>