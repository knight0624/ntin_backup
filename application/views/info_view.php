<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="">
		<style>
			td{
				font-size: 17px;
				line-height: 40px!important;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-6 col-lg-offset-3">
							<div class="ibox">
								<div class="ibox-title">
									<h5>基本資料</h5>
								</div>
								<div class="ibox-content">
									<table class="table table-bordered table-hover">
										<tbody>
											<?php echo $info; ?>
										</tbody>
									</table>
									<div class="text-center">
										<a href="<?php echo base_url(); ?>info/setting" class="btn btn-primary">修改資料</a>
										<button type="button" class="btn btn-warning" onclick="javascript:history.back()">回上一頁</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src=""></script>
		<script>
		</script>
<?php $this->load->view("basic/end");?>
