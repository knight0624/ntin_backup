<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/ch.css">
		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

		<!-- Data picker -->
		<link href="<?php echo base_url(); ?>dist/css/plugins/chosen/chosen.css" rel="stylesheet">

		<!-- Input Mask-->
		<link href="<?php echo base_url(); ?>dist/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<style>
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox">
								<div class="ibox-title">
									<h2><?php echo $hw_name; ?> - 拾参、引用書籍、期刊、論文等</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
									<?php
										switch ($permission) {
											case '1':
											case '3':
												echo
												'<div class="form-group">
													<label class="col-sm-12">引用書籍、期刊、論文等：</label>
													<div class="col-sm-12">
														<textarea class="form-control" rows="30" placeholder="引用書籍、期刊、論文等" id="reference">'.$reference_content.'</textarea>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-12">
														<button class="btn btn-primary" id="btn_reference" >儲存</button>
													</div>
												</div>
												<hr>
												<div class="form-group">
													<label class="col-sm-12">上傳附件：活動剪影-家訪記錄</label>
													<div class="col-sm-12">'.
														form_open_multipart("Ch13/do_upload_file").'
															<input type="file" name="userfile[]" id="select_file" style="display:none;" multiple>
															<img src="'.base_url().'dist/img/select_file.png" id="file_select" value="選擇檔案" style="width:150px;">
															<input type="submit" id="upload_file" style="display:none;" value="1" name="number">
															<img src="'.base_url().'dist/img/upload_file.png" id="file_upload" value="上傳檔案" style="width:150px;">
														</form>
														<hr>
														<p class="text-danger">上傳多個檔案請以ctrl或shift進行多個檔案的選取</p>
														<p class="text-danger">或是以滑鼠圈選多個檔案進行上傳</p>
														<p class="text-danger">檔案大小限制為8MB</p>
													</div>
												</div>';
											break;

											case '2':
											case '6':
												echo
												'<div class="form-group">
													<label class="col-sm-12">引用書籍、期刊、論文等：</label>
													<div class="col-sm-12">
														<p class="form-control" id="reference">'.$reference_content.'</p>
													</div>
													<div class="col-sm-2"></div>
												</div>';
											break;
										}
									?>
										<div class="btn-group" style="margin-left:16.66666667%;">
											<?php
												for ($i=0; $i <count($upload) ; $i++) {
													echo
														"<a class='btn btn-primary btn-xs' href='".base_url()."uploads/reference/".$upload[$i]["file_name"]."' download style='margin:auto;width:90%'>下載：".$upload[$i]["file_name"]."</a>";
													switch ($permission) {
														case "1":
														case "3":
															echo '<a class="btn btn-danger btn-xs delete_file" data-file="'.$upload[$i]["file_name"].'"><i class="fa fa-trash"></i></a>';
														break;
													}
													echo "<br>";
												}
											?>
										</div>

										<div class="text-center">
											<a href="<?php echo base_url("ch12");?>" class="btn btn-default">上一步</a>
										</div>
									</div>
								</div>
							</div>

							<div class="ibox">
								<div class="ibox-title">
									<h2>老師評語</h2>
								</div>
								<div class="ibox-content">
									<div class="form-horizontal">
										<div class="form-group">
											<?php
												switch ($permission) {
													case '1':
													case '6':
														echo '
															<div class="col-sm-12">
																<p class="text-danger form-control">'.$comment.'</p>
															</div>
															<div class="col-sm-12">整體評分：<span class="text-danger">'.$score.'<span></div>';
													break;

													case '2':
													case '3':
														echo '
															<div class="col-sm-12">
																<textarea class="form-control" name="comment" rows="10" placeholder="老師評語">'.$comment.'</textarea>
																<label class="control-label">整體評分：</label>
																<input type="number" class="form-control" name="score" id="score" min="1" max="100" value="'.$score.'">
															</div>
															<div class="hr-line-dashed"></div>
															<div class="col-sm-12">
																<button type="submit" class="btn btn-primary" id="save_comment">儲存</button>
															</div>' ;
													break;
												}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>

		<script src="<?php echo base_url(); ?>dist/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>

		<!-- Data picker -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/datapicker/bootstrap-datepicker.js"></script>

		<!-- Chosen -->
		<script src="<?php echo base_url(); ?>dist/js/plugins/chosen/chosen.jquery.js"></script>

		<!-- Input Mask-->
		<script src="<?php echo base_url(); ?>dist/js/plugins/jasny/jasny-bootstrap.min.js"></script>

		<script>
			var file_name = null;
			if(document.querySelector("#file_select")){
				document.querySelector("#file_select").onclick = function(){
					document.querySelector("#select_file").click(true);
				}
			}
			if(document.querySelector("#file_select")){
				document.querySelector("#file_upload").onclick = function(){
					document.querySelector("#upload_file").click(true);
				}
			}

			$(".delete_file").click(function(){
				file_name = $(this).data("file");
				swal({
						title: "刪除文件?",
						text: "按下刪除後文件不可復原",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "刪除!",
						cancelButtonText: "取消",
						closeOnConfirm: false,
						closeOnCancel: false
					},
					function(isConfirm){
						if (isConfirm) {
							$.ajax({
								url: "<?php echo base_url(); ?>ch13/delete_file",
								type: "POST",
								data: {
									file_name : file_name
								},
								dataType: "text" ,
								success: function(data){
									console.log(data);
									if (data) {
										location.reload();
									}
									else{
										swal("錯誤!", "刪除時發生錯誤!", "error");
									}
								}
							});
						} else {
							swal("已取消!", "您的文件是安全的", "error");
						}
					}
				);
			})

			if(document.querySelector("#btn_reference")){
				document.querySelector("#btn_reference").onclick = function(){
					var reference = document.querySelector("#reference").value;
					if(reference != null){
						$.ajax({
							url: "<?php echo base_url(); ?>ch13/editReference" ,
							type: "POST" ,
							data: {
								reference : reference
							} ,
							dataType: "text" ,
							success: function(data){
								swal({
									title: "成功",
									text: "儲存成功",
									type: "success",
									confirmButtonText: "確定",
								});
							}
						});
					} else {
						swal({
							title: "警告",
							text: "標題或內文不能為空",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}
				}
			}

			if(document.querySelector("#save_comment")){
				document.querySelector("#save_comment").onclick = function(){
					var comment = document.getElementsByName("comment")[0].value;
					var score = document.getElementsByName("score")[0].value;
					if(score<=100 && score>=0){
						$.ajax({
							url: "<?php echo base_url(); ?>ch13/addComment" ,
							type: "POST" ,
							data: {
								comment : comment, score : score
							} ,
							dataType: "text" ,
							success: function(data){
								swal({
									title: "成功",
									text: "已儲存",
									type: "success",
									confirmButtonText: "確定",
									animation: "slide-from-top"
								});
							}
						});
					}
					else{
						swal({
							title: "警告",
							text: "整體評分只能介於0到100之間",
							type: "error",
							confirmButtonText: "確定",
							animation: "slide-from-top"
						});
					}

				}
			}


		</script>
<?php $this->load->view("basic/end");?>
