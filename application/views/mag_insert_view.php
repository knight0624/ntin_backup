<?php $this->load->view("basic/begin");?>
		<link rel="stylesheet" href="">
		<style>
			table{
				font-size: 17px;
			}
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox">
								<div class="ibox-title">
									<h5>新增使用者</h5>
								</div>
								<div class="ibox-content">
									<form class="form-horizontal" action="<?php echo base_url(); ?>manage/insertUser" method="POST">
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">學號(帳號)</label>
											<div class="col-sm-10"><input type="text" class="form-control" name="account" value="<?php echo set_value("account"); ?>" placeholder="輸入格式 如：510201100" required><div class="text-danger err-user_name"><?php echo form_error("account"); ?></div></div>
										</div>
										<div class="form-group"><label class="col-sm-2 control-label">姓名</label>
											<div class="col-sm-10"><input type="text" class="form-control" name="user_name" value="<?php echo set_value("user_name"); ?>" placeholder="輸入格式 如：王小明" required><div class="text-danger err-user_name"><?php echo form_error("user_name"); ?></div></div>
										</div>
										<div class="form-group"><label class="col-sm-2 control-label">信箱</label>
											<div class="col-sm-10"><input type="text" class="form-control" name="email" value="<?php echo set_value("email"); ?>" pattern="[a-zA-Z0-9!#$%&amp;'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" placeholder="輸入格式 如：XXX@gmail.com" required title="需符合規定格式"><div class="text-danger err-email"><?php echo form_error("email"); ?></div></div>
										</div>
										<div class="form-group"><label class="col-sm-2 control-label">入學年</label>
											<div class="col-sm-10"><input type="number" class="form-control" name="entry" value="<?php echo set_value("entry"); ?>" placeholder="格式為半形數字3碼 如：105" maxlength="10" pattern=".{9,10}" required title="需要9碼或10碼數字"><div class="text-danger err-entry"><?php echo form_error("entry"); ?></div></div>
										</div>
										<div class="form-group"><label class="col-sm-2 control-label">班級</label>
											<div class="col-sm-10"><input type="number" class="form-control" name="class" value="<?php echo set_value("class"); ?>" placeholder="格式為半形數字3碼 如：401" maxlength="10"><div class="text-danger err-class"><?php echo form_error("class"); ?></div></div>
										</div>
										<div class="hr-line-dashed"></div>
										<div class="form-group"><label class="col-sm-2 control-label">身份</label>
											<div class="col-sm-10">
												<select class="form-control m-b" name="permission">
													<?php echo $permission_opt; ?>
												</select>
											</div>
										</div>
										<div class="text-center">
											<input type="submit"  class="btn btn-primary" value="新增">
											<input type="hidden" name="token" value="<?php echo $_SESSION["insert_token"]?>">
											<a href="<?php echo base_url(); ?>manage" class="btn btn-warning">回上一頁</a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src=""></script>
		<script>
		</script>
<?php $this->load->view("basic/end");?>
