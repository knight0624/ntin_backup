<?php $this->load->view("basic/begin");?>
		<style>
		</style>
		<?php $this->load->view("basic/top")?>
				<div class="wrapper wrapper-content animated fadeInRight">
					<!-- 頁面內容開始 -->
					<div class="row">
						<div class="col-lg-6 col-lg-offset-3">
							<div class="ibox">
								<div class="ibox-title">
									<h2>選擇課程</h2>
								</div>
								<div class="ibox-content">
									<select class="form-control" id="course">
										<?php echo $opt;?>
									</select>
									<hr>
									<select class="form-control" id="hw">
									</select>
								</div>
								<button class="btn btn-primary" id="set">選擇</button>
							</div>
						</div>
					</div>
					<!-- 頁面內容結束 -->
				</div>
		<?php $this->load->view("basic/bottom")?>
		<script src="<?php echo base_url(); ?>dist/js/common.js"></script>
		<script>
			document.querySelector("#course").onchange = function(){
				if(this.value != ""){
					$.ajax({
						url: "<?php echo base_url(); ?>team/getHw",
						type: "POST",
						data: {
							course_id : this.value
						},
						dataType: "text",
						success: function(data){
							$("#hw").html(data);
						}
					});
				}
			};
			document.querySelector("#set").onclick = function(){
				var course = document.querySelector("#course").value
				var hw = document.querySelector("#hw").value
				if(course != "" && hw != ""){
					location.assign("<?php echo base_url(); ?>team/setting/"+course+"/"+hw);
				}
			}
		</script>
<?php $this->load->view("basic/end");?>
