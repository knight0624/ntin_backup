<?php
date_default_timezone_set("Asia/Taipei");

class MY_Authentication extends CI_Controller {

	private $name;
	private $login_success = 0;
	private $permission_level = 0;
	private $MenuHTML = "";

	public function __construct() {
		parent::__construct();
		session_start();
		$this->load->model("authentication_model" , "" , TRUE);

		// 判斷session是否有帳密輸入
		if(isset($_SESSION["username"]) && isset($_SESSION["password"]) && $_SESSION["username"] && $_SESSION["password"]) {

			// 判斷是否為合法帳密，$user取得"帳號"及"姓名"
			$user = $this->authentication_model->verify_User($_SESSION["username"] , $_SESSION["password"]);
				 
			if( $user->num_rows() ) {
				if( $user->row()->status == 1) {
					$this->name = $user->row()->name;
					$this->permission_level = $user->row()->permission_id;

					// ==============================動態功能清單==============================
					$MenuList = $this->authentication_model->getMenuList($this->permission_level)->result_array();
					$MenuGroup = $this->authentication_model->getMenuGroup($this->permission_level)->result_array();

					$icon = array("fa-comment-o", "fa-users", "fa-list", "fa-pencil-square-o", "fa-money");

					for ($i = 0; $i < count($MenuGroup); $i++) {
						$this->MenuHTML .= "<li class='active'><a href='#'><i class='fa ".$icon[$i]."'></i><span class='nav-label'>".$MenuGroup[$i]["menu_group_name"]."</span><span class='fa arrow'></span></a><ul class='nav nav-second-level collapse'>";

						for ($j = 0; $j < count($MenuList); $j++) {
							if($MenuGroup[$i]["menu_group_id"] != $MenuList[$j]["menu_group_id"]){
								continue;
							}
							$this->MenuHTML .= "<li><a href='".base_url().$MenuList[$j]["menu_path"]."'>".$MenuList[$j]["menu_name"]."</a></li>";
						}
						$this->MenuHTML .= "</ul></li>";
					}
					// ==============================動態功能清單==============================



					// ================================確認權限================================
					$valid = $this->authentication_model->verify_Permission($this->router->fetch_class());
					$valid == TRUE ? $this->login_success = 1 : $this->login_success = 2;
					// ================================確認權限================================
				} else {
					$this->login_success = 2;
				}
			} else {
				$this->login_success = 0;
			}
		} else {
			$this->login_success = 0;
		}
	}

	/**
	 * [取得使用者姓名]
	 * @return [type] [description]
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * [取得登入狀態]
	 * @return [type] [description]
	 */
	public function getLoginStatus() {
		return $this->login_success;
	}

	/**
	 * [取得權限等級]
	 * @return [type] [description]
	 */
	public function getPermission(){
		return $this->permission_level;
	}

	/**
	 * [取得功能名稱]
	 * @return [type] [description]
	 */
	public function getMenuName() {
		return $this->authentication_model->getMenuInfo($this->router->fetch_class())->row()->menu_name;
	}

	/**
	 * [取得功能說明]
	 * @return [type] [description]
	 */
	public function getMenuDesb() {
		return $this->authentication_model->getMenuInfo($this->router->fetch_class())->row()->menu_describe;
	}

	/**
	 * [麵包屑]
	 * @return [type] [description]
	 */
	public function getMenuBadm() {
		$breadcrumb = "<li class='active'>";
		$breadcrumb .= "<strong>".$this->authentication_model->getMenuInfo($this->router->fetch_class())->row()->menu_name."</strong>";
		$breadcrumb .= "</li>";

		return $breadcrumb;
	}

	/**
	 * [頁面初始化]
	 * @param  [type] $title [description]
	 * @return [type]        [description]
	 */
	protected function pageInit($title) {
		$data["title"] = $title;                                //頁面標題名稱
		$data["name"] = $this->getName();                       //取得使用者姓名
		$data["system_menu"] = $this->MenuHTML;                 //取得當前menu
		$data["menu_name"] = $this->getMenuName();              //取得當前功能名稱
		$data["breadcrumb"] = $this->getMenuBadm();             //麵包屑

		return $data;
	}

	/**
	 * [登出並清除session]
	 * @return [type] [description]
	 */
	public function logout() {
		session_destroy();
		header("Location: " . base_url() . "login");
	}
}

?>